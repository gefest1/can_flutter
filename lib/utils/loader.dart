import 'package:flutter/material.dart';
import 'package:tube/main.dart';

OverlayEntry? _gOverlay;

class _LoaderBlock extends StatefulWidget {
  final BuildContext context;

  const _LoaderBlock({required this.context, Key? key}) : super(key: key);

  Future<bool> onWillPop() async {
    return false;
  }

  @override
  State<_LoaderBlock> createState() => _LoaderBlockState();
}

VoidCallback? routeClean;

class _LoaderBlockState extends State<_LoaderBlock> {
  bool _cleaned = false;
  ModalRoute<dynamic>? _route;

  @override
  void initState() {
    routeClean = () {
      if (_cleaned) return;
      _cleaned = true;
      _route?.removeScopedWillPopCallback(widget.onWillPop);
    };
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _route?.removeScopedWillPopCallback(widget.onWillPop);
    _route = ModalRoute.of(widget.context);
    _route?.addScopedWillPopCallback(widget.onWillPop);
  }

  @override
  void didUpdateWidget(_LoaderBlock oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.onWillPop != oldWidget.onWillPop && _route != null) {
      _route!.removeScopedWillPopCallback(oldWidget.onWillPop);
      _route!.addScopedWillPopCallback(widget.onWillPop);
    }
  }

  @override
  void dispose() {
    if (!_cleaned) {
      _cleaned = true;
      _route?.removeScopedWillPopCallback(widget.onWillPop);
    }

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: const AbsorbPointer(
        absorbing: true,
        child: DecoratedBox(
          decoration: BoxDecoration(color: Colors.black26),
          child: Center(child: CircularProgressIndicator()),
        ),
      ),
    );
  }
}

void loaderActivate(BuildContext context) {
  // WillPopScope
  _gOverlay?.remove();
  _gOverlay = OverlayEntry(
    builder: (_) => _LoaderBlock(
      context: context,
    ),
    maintainState: true,
  );
  navigatorKeyGlobal.currentState?.overlay?.insert(_gOverlay!);
}

void killLoaderOverlay() {
  _gOverlay?.remove();
  _gOverlay = null;
}
