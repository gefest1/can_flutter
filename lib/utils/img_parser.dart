import 'dart:isolate';
import 'dart:typed_data';

import 'package:image/image.dart' as img;

Future<void> _byeImageExif(List<dynamic> args) async {
  // final commandPort = ReceivePort();
  // p.send(commandPort.sendPort);
  final SendPort p = args[0];

  final Uint8List data = args[1];
  final img.Image capturedImage = img.decodeImage(data)!;
  final img.Image orientedImage = img.bakeOrientation(capturedImage);

  Isolate.exit(p, img.encodeJpg(orientedImage));
}

Future<List<int>> byeImageExif(Uint8List data) async {
  final p = ReceivePort();
  await Isolate.spawn(_byeImageExif, [p.sendPort, data]);

  return await p.first as List<int>;
}
