import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tube/utils/routes.observer.dart';

class NestedLogicRoute extends StatefulWidget {
  final Widget child;

  const NestedLogicRoute({
    required this.child,
    Key? key,
  }) : super(key: key);

  @override
  State<NestedLogicRoute> createState() => _NestedLogicRouteState();
}

class _NestedLogicRouteState extends State<NestedLogicRoute> {
  MyNavigatorObserver observer = MyNavigatorObserver();
  GlobalKey<NavigatorState> navKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (observer.routeStack.length == 1) return true;
        navKey.currentState?.pop();
        return false;
      },
      child: Navigator(
        key: navKey,
        onPopPage: (route, result) {
          if (observer.routeStack.length != 1) return true;
          Navigator.pop(context, result);
          return false;
        },
        observers: [observer],
        pages: [
          Platform.isIOS
              ? CupertinoPage(child: widget.child)
              : MaterialPage(child: widget.child),
        ],
      ),
    );
  }
}
