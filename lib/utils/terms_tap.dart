import 'dart:developer';

import 'package:url_launcher/url_launcher.dart';

void termsTap() async {
  try {
    await launchUrl(Uri.parse('https://www.checkallnow.net/public'));
  } catch (e, s) {
    log(e.toString(), stackTrace: s);
  }
}

void politicTap() async {
  try {
    await launchUrl(Uri.parse('https://www.checkallnow.net/policy '));
  } catch (e, s) {
    log(e.toString(), stackTrace: s);
  }
}
