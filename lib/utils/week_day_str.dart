import 'package:intl/intl.dart';

String? getWeekStr(List<DateTime> val) {
  String? str;
  final value = [...val]..sort((a, b) => a.compareTo(b));
  if (value.isNotEmpty) {
    for (int ind = 0; ind < value.length * 2 - 1; ++ind) {
      if (ind.isOdd) {
        (str ??= '');
        str += ' | ';
        continue;
      }
      str ??= '';
      str += DateFormat('HH:mm').format(value[ind ~/ 2]);
    }
  }
  return str;
}
