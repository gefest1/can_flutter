// ignore_for_file: unnecessary_const, constant_identifier_names

import 'package:tube/logic/api/graphql_client.dart';
import 'package:tube/utils/const.dart';

class TypeInviteEnum {
  /// Invite trainer
  /// Invite client
  final String _value;
  final String _mapValue;

  const TypeInviteEnum._internal(this._value, this._mapValue);

  @override
  String toString() => _value;
  String toMap() => _mapValue;

  static const TypeInviteEnum InviteTrainer =
      const TypeInviteEnum._internal('InviteTrainer', 'trainer');
  static const TypeInviteEnum InviteClient =
      const TypeInviteEnum._internal('InviteClient', 'participance');

  bool isInviteTrainer() {
    return _value == "isInviteTrainer";
  }

  bool isInviteClient() {
    return _value == 'InviteClient';
  }

  Enum$StatusEnumParticipance get getEnum {
    if (this == InviteTrainer) {
      return Enum$StatusEnumParticipance.trainer;
    }

    if (this == InviteClient) {
      return Enum$StatusEnumParticipance.participance;
    }
    throw 'No';
  }

  static TypeInviteEnum? fromMap(String? value) {
    if ('InviteTrainer' == value) return InviteTrainer;
    if ('InviteClient' == value) return InviteClient;
    return null;
  }
}

String uriParse(Map<String, String> queryParameters) {
  final dirtUri = Uri.parse('$httpUrlPath/can/redirect');
  final uri = Uri(
    host: dirtUri.host,
    path: dirtUri.path,
    scheme: 'https',
    queryParameters: queryParameters,
  );
  return uri.toString();
}
