import 'package:flutter/material.dart';
import 'package:graphql/client.dart';
import 'package:tube/logic/api/graphql_client.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/utils/parse_time.dart';

class GetSpecialBooking extends ChangeNotifier {
  final Map<String, Booking> bookings = {};

  /// key: trainerId + parsed ISO date
  final Map<String, WorkTimeObj> workTimeMap = {};

  GetSpecialBooking();

  Future<Booking> getBooking(String bookingId) async {
    try {
      final queryResult = await graphqlClient.query$getWorkJob(
        Options$Query$getWorkJob(
          variables: Variables$Query$getWorkJob(bookingId: bookingId),
          fetchPolicy: FetchPolicy.networkOnly,
        ),
      );
      // final queryResult = await client.query(
      //   QueryOptions(
      //     document: getBookingId,
      //     variables: {
      //       "bookingId": bookingId,
      //     },
      //     fetchPolicy: FetchPolicy.networkOnly,
      //   ),
      // );
      if (queryResult.hasException) throw queryResult.exception!;

      final Booking newValue =
          Booking.fromMap(queryResult.parsedData!.getWorkJob.toJson());

      if (bookings[bookingId] == newValue) return newValue;
      bookings[bookingId] = newValue;
      // ignore: avoid_function_literals_in_foreach_calls
      newValue.workTimes?.entries.forEach((element) {
        final trainerId = element.key;
        final list = element.value;
        // ignore: avoid_function_literals_in_foreach_calls
        list.forEach((workTimeObj) {
          workTimeMap[trainerId +
              parseTime(workTimeObj.date!).toIso8601String()] = workTimeObj;
        });
      });
      notifyListeners();
      return newValue;
    } catch (e) {
      rethrow;
    }
  }

  int? getNumberOfUser(
      {required String bookingId,
      required String trainerId,
      required DateTime dateTime}) {
    if (bookings[bookingId] == null) return null;

    final WorkTimeObj? work =
        workTimeMap[trainerId + parseTime(dateTime).toIso8601String()];
    if (work == null) return null;
    return work.now?.toInt();
  }

  void superLazyGetBooking(String bookingId) async {
    if (bookings[bookingId] != null) return;
    getBooking(bookingId);
  }

  @override
  void dispose() {
    bookings.clear();
    super.dispose();
  }
}
