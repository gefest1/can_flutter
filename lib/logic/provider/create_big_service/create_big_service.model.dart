class CreateBigServiceModel {
  const CreateBigServiceModel({
    this.name,
    this.description,
    this.duration,
    this.trainerIds,
    this.price,
    this.percent,
  });

  final String? name;
  final String? description;
  final Duration? duration;
  final List<String>? trainerIds;
  final double? price;
  final double? percent;

  CreateBigServiceModel copyWith({
    String? name,
    String? description,
    Duration? duration,
    List<String>? trainerIds,
    double? price,
    double? percent,
  }) {
    return CreateBigServiceModel(
      name: name ?? this.name,
      description: description ?? this.description,
      duration: duration ?? this.duration,
      trainerIds: trainerIds ?? this.trainerIds,
      price: price ?? this.price,
      percent: percent ?? this.percent,
    );
  }
}
