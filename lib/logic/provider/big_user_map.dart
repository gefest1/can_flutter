import 'package:flutter/foundation.dart';

import 'package:graphql/client.dart';
import 'package:tube/logic/api/graphql_client.dart';

import 'package:tube/logic/blocs/user/user.model.dart';

class BigUserMap extends ChangeNotifier {
  //
  Map<String, User> userMap = {};

  BigUserMap();

  Future<void> getUsers(List<String> userIds) async {
    final queryRes = await graphqlClient.query$getUsersByIds(
      Options$Query$getUsersByIds(
        variables: Variables$Query$getUsersByIds(ids: userIds),
        fetchPolicy: FetchPolicy.networkOnly,
      ),
    );

    if (queryRes.hasException) throw queryRes.exception!;

    final users = queryRes.parsedData!.getUsersByIds
        .map((e) => User.fromMap(e.toJson()))
        .toList();
    final newMap = {...userMap};
    for (final user in users) {
      newMap[user.id!] = user;
    }
    if (mapEquals(newMap, userMap)) return;
    userMap.clear();
    userMap = newMap;
    notifyListeners();
  }
}
