import 'package:flutter/material.dart';
import 'package:tube/logic/provider/create_big_event/create_big_event.model.dart';

class CreateBigEventProvider extends ChangeNotifier {
  CreateBigEventModel createBigEvent;

  CreateBigEventProvider({
    required this.createBigEvent,
  });

  CreateBigEventModel setNewInstance(final CreateBigEventModel newVal) {
    if (createBigEvent != newVal) {
      createBigEvent = newVal;
      notifyListeners();
    }
    return createBigEvent;
  }
}
