import 'dart:async';
import 'dart:developer';

import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:graphql/client.dart';
import 'package:tube/logic/api/graphql_client.dart';
import 'package:tube/logic/blocs/gifts/model/gift.model.dart';
import 'package:tube/logic/model/create_tarriff.dart';

part 'gift_state.dart';
part 'gift_event.dart';
part 'reaction/get_gift.reaction.dart';
part 'reaction/use_gift_reaction.dart';

class GiftBloc extends Bloc<GiftEvent, GiftState> {
  final String clubId;
  GiftBloc({
    required this.clubId,
  }) : super(const LoadingGiftState()) {
    on<GetGiftEvent>(getGiftReaction);
    on<UseGiftEvent>(useGiftReaction);

    add(const GetGiftEvent());
  }
}
