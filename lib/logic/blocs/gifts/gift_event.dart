part of 'gift_bloc.dart';

abstract class GiftEvent {
  const GiftEvent();
}

class GetGiftEvent extends GiftEvent {
  const GetGiftEvent();
}

class UseGiftEvent extends GiftEvent {
  final Completer? completer;
  final List<InputTrainGraph> inputTrains;
  final String giftId;
  final DateTime? dateTime;

  const UseGiftEvent({
    this.completer,
    required this.inputTrains,
    required this.giftId,
    this.dateTime,
  });
}
