// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:equatable/equatable.dart';

import 'package:tube/logic/blocs/club/club.model.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/utils/date_time_parse.dart';

class GiftModel extends Equatable {
  final String id;
  final int count;
  final String clubId;
  final String bookingId;
  final String clientId;
  final String? boughtId;
  final DateTime createdDate;
  final String? tariffId;
  final DateTime? deadlineDate;

  final Booking? booking;
  final User? client;
  final Club? club;

  const GiftModel({
    required this.id,
    required this.count,
    required this.clubId,
    required this.bookingId,
    required this.clientId,
    this.boughtId,
    required this.createdDate,
    this.tariffId,
    this.deadlineDate,
    this.booking,
    this.client,
    this.club,
  });

  GiftModel copyWith({
    String? id,
    int? count,
    String? clubId,
    String? bookingId,
    String? clientId,
    String? boughtId,
    DateTime? createdDate,
    String? tariffId,
    DateTime? deadlineDate,
    Booking? booking,
    User? client,
    Club? club,
  }) {
    return GiftModel(
      id: id ?? this.id,
      count: count ?? this.count,
      clubId: clubId ?? this.clubId,
      bookingId: bookingId ?? this.bookingId,
      clientId: clientId ?? this.clientId,
      boughtId: boughtId ?? this.boughtId,
      createdDate: createdDate ?? this.createdDate,
      tariffId: tariffId ?? this.tariffId,
      deadlineDate: deadlineDate ?? this.deadlineDate,
      booking: booking ?? this.booking,
      client: client ?? this.client,
      club: club ?? this.club,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      '_id': id,
      'count': count,
      'clubId': clubId,
      'bookingId': bookingId,
      'clientId': clientId,
      'boughtId': boughtId,
      'createdDate': createdDate.millisecondsSinceEpoch,
      'tariffId': tariffId,
      'deadlineDate': deadlineDate?.millisecondsSinceEpoch,
      'booking': booking?.toMap(),
      'client': client?.toMap(),
      'club': club?.toMap(),
    };
  }

  factory GiftModel.fromMap(Map<String, dynamic> map) {
    return GiftModel(
      id: map['_id'] as String,
      count: map['count'] as int,
      clubId: map['clubId'] as String,
      bookingId: map['bookingId'] as String,
      clientId: map['clientId'] as String,
      boughtId: map['boughtId'] != null ? map['boughtId'] as String : null,
      createdDate: dateTimeParse(map['createdDate'])!,
      tariffId: map['tariffId'] != null ? map['tariffId'] as String : null,
      deadlineDate: dateTimeParse(map['deadlineDate']),
      booking: map['booking'] != null
          ? Booking.fromMap(map['booking'] as Map<String, dynamic>)
          : null,
      client: map['client'] != null
          ? User.fromMap(map['client'] as Map<String, dynamic>)
          : null,
      club: map['club'] != null
          ? Club.fromMap(map['club'] as Map<String, dynamic>)
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory GiftModel.fromJson(String source) =>
      GiftModel.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  bool get stringify => true;

  @override
  List<Object?> get props {
    return [
      id,
      count,
      clubId,
      bookingId,
      clientId,
      boughtId,
      createdDate,
      tariffId,
      deadlineDate,
      booking,
      client,
      club,
    ];
  }
}
