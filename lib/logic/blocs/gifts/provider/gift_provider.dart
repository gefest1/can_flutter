import 'package:flutter/material.dart';
import 'package:tube/logic/blocs/gifts/model/gift.model.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/model/booking.model.dart';

class GiftTariffProvider extends ChangeNotifier {
  Booking booking;
  GiftModel gift;
  Map<String, List<DateTime>>? dateTimesMap;
  Map<String, User>? mapUsers;
  DateTime? dateTime;

  GiftTariffProvider({
    required this.booking,
    required this.gift,
    this.dateTimesMap,
    this.mapUsers,
    this.dateTime,
  });

  void update({
    Booking? booking,
    Map<String, List<DateTime>>? dateTimesMap,
    Map<String, User>? mapUsers,
    String? owner,
    DateTime? dateTime,
  }) {
    this.booking = booking ?? this.booking;
    this.dateTimesMap = dateTimesMap ?? this.dateTimesMap;
    this.mapUsers = mapUsers ?? this.mapUsers;
    this.dateTime = dateTime ?? this.dateTime;
    notifyListeners();
  }

  void clean({
    // bool booking = false,
    bool dateTimesMap = false,
    bool mapUsers = false,
    bool owner = false,
    bool dateTime = false,
  }) {
    // if (booking) this.booking = null;
    if (dateTimesMap) this.dateTimesMap = null;
    if (mapUsers) this.mapUsers = null;
    if (dateTime) this.dateTime = null;
    notifyListeners();
  }
}
