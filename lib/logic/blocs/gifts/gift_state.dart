part of 'gift_bloc.dart';

abstract class GiftState extends Equatable {
  const GiftState();
}

class LoadingGiftState extends GiftState {
  const LoadingGiftState();
  @override
  List<Object?> get props => [];
}

class DataGiftState extends GiftState {
  final List<GiftModel> gifts;

  const DataGiftState(this.gifts);

  @override
  List<Object?> get props => [gifts];
}
