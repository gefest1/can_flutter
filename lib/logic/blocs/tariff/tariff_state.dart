part of 'tariff_bloc.dart';

abstract class TariffState extends Equatable {
  const TariffState();
}

class LoadingTariffState extends TariffState {
  const LoadingTariffState();

  @override
  List<Object?> get props => [];
}

class DataTariffState extends TariffState {
  final List<Tariff> tariffs;
  const DataTariffState(this.tariffs);

  @override
  List<Object?> get props => [tariffs];
}
