part of 'tariff_bloc.dart';

abstract class TariffEvent {
  const TariffEvent();
}

class GetDataTariffEvent extends TariffEvent {
  final int count;

  const GetDataTariffEvent({this.count = 0});
}

class AddTariffEvent extends TariffEvent {
  final List<InputTrainGraph> inputTrains;
  final Booking booking;
  final Completer completer;
  final String? owner;
  final DateTime? dateTime;
  final SubBooking? subBooking;

  const AddTariffEvent({
    required this.inputTrains,
    required this.booking,
    required this.completer,
    this.owner,
    this.dateTime,
    this.subBooking,
  });
}

class RemoveTrainTimeEvent extends TariffEvent {
  final String tariffId;
  final String trainTimeId;

  const RemoveTrainTimeEvent({
    required this.trainTimeId,
    required this.tariffId,
  });
}

class SetNewTrainTimeEvent extends TariffEvent {
  final List<TrainTime> trainTime;
  final String tariffId;

  const SetNewTrainTimeEvent({
    required this.trainTime,
    required this.tariffId,
  });
}

class UpdateTrainTimeEvent extends TariffEvent {
  final List<TrainTime> trainTime;
  final String tariffId;
  final Completer completer;

  const UpdateTrainTimeEvent({
    required this.trainTime,
    required this.tariffId,
    required this.completer,
  });
}

class InputTrain {
  final DateTime date;
  final DateTime endTime;
  final String trainerId;

  const InputTrain({
    required this.date,
    required this.endTime,
    required this.trainerId,
  });
  @override
  String toString() {
    return [
      date,
      endTime,
      trainerId,
    ].toString();
  }

  Map<String, dynamic> toMap() {
    return {
      "date": date.toUtc().toIso8601String(),
      "endTime": endTime.toUtc().toIso8601String(),
      "trainerId": trainerId,
    };
  }
}

class EditTariffEvent extends TariffEvent {
  final String tariffId;
  final List<InputTrain> inputs;
  final Completer? completer;

  const EditTariffEvent({
    required this.tariffId,
    required this.inputs,
    this.completer,
  });
}
