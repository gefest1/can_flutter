import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:tube/utils/date_time_parse.dart';

class TrainTime extends Equatable {
  final String? id; //: String!
  final bool? come; //: Boolean!
  final DateTime? date; //: DateTime!
  final DateTime? endTime; //: DateTime!
  final String? trainerId;

  const TrainTime({
    this.id,
    this.come,
    this.date,
    this.endTime,
    this.trainerId,
  });

  TrainTime copyWith({
    String? id,
    bool? come,
    DateTime? date,
    DateTime? endTime,
    String? trainerId,
  }) {
    return TrainTime(
      id: id ?? this.id,
      come: come ?? this.come,
      date: date ?? this.date,
      endTime: endTime ?? this.endTime,
      trainerId: trainerId ?? this.trainerId,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'come': come,
      'date': date?.toUtc().toIso8601String(),
      'endTime': endTime?.toUtc().toIso8601String(),
      'trainerId': trainerId
    };
  }

  factory TrainTime.fromMap(Map<String, dynamic> map) {
    return TrainTime(
      id: map['_id'],
      come: map['come'],
      date: dateTimeParse(map['date']),
      endTime: dateTimeParse(map['endTime']),
      trainerId: map['trainerId'],
    );
  }

  String toJson() => json.encode(toMap());

  factory TrainTime.fromJson(String source) =>
      TrainTime.fromMap(json.decode(source));

  @override
  String toString() {
    return 'TrainTime(id: $id,\n come: $come,,\n date: $date,,\n endTime: $endTime,\n)';
  }

  @override
  List<Object?> get props => [
        id,
        come,
        date?.toUtc().toIso8601String(),
        endTime?.toUtc().toIso8601String(),
      ];
}
