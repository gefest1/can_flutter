part of '../club_client_bloc.dart';

extension GetReactionClub on ClubClientBloc {
  getClubClient(
      GetClubClientEvent event, Emitter<ClubClientState> emitter) async {
    try {
      final queryRes = await graphqlClient.query$getClientsByClubId(
        Options$Query$getClientsByClubId(
          variables: Variables$Query$getClientsByClubId(clubId: clubId),
          fetchPolicy: FetchPolicy.networkOnly,
        ),
      );

      if (queryRes.hasException) throw queryRes.exception!;

      emitter(
        DataClubClientState(
          users: queryRes.parsedData!.getClientsByClubId
              .map(
                (e) => User.fromMap(e.toJson()),
              )
              .toList(),
        ),
      );
    } catch (e) {
      log(e.toString());
    }
  }
}
