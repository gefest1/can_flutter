import 'dart:developer';

import 'package:equatable/equatable.dart';
import 'package:graphql/client.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:tube/logic/api/graphql_client.dart';
import 'package:tube/logic/blocs/user/user.model.dart';

part './club_client_event.dart';
part './club_client_state.dart';
part 'reaction/get_club_reaction.dart';

class ClubClientBloc extends Bloc<ClubClientEvent, ClubClientState> {
  final String clubId;

  ClubClientBloc({
    required this.clubId,
  }) : super(const InitClubClientState()) {
    on<GetClubClientEvent>(getClubClient);
    add(const GetClubClientEvent());
  }
}
