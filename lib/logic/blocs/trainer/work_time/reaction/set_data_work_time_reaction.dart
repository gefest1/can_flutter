part of '../work_time_bloc.dart';

extension SetDataWorkTimeReaction on ControllerWorkTimeBloc {
  void setDataWorkTimeReaction(
    SetDataWorkTimeEvent event,
    Emitter<WorkTimeState> emit,
  ) async {
    try {
      emit(LoadingWorkTimeState());
      final mutationResult = await graphqlClient.mutate$setWorkTime(
        Options$Mutation$setWorkTime(
          variables: Variables$Mutation$setWorkTime(
            clubId: clubId,
            workTime: event.workTimes.map<Input$WorkTimeInput>((e) {
              return Input$WorkTimeInput(
                endTime: e.endTime!,
                startTime: e.startTime!,
              );
            }).toList(),
            trainerId: trainerId,
          ),
          fetchPolicy: FetchPolicy.networkOnly,
        ),
      );

      if (mutationResult.hasException) throw mutationResult.exception!;

      Iterable<MapEntry<int, WorkTimeGraph>> workTimeGraphList =
          mutationResult.parsedData!.setWorkTime.workTime.map((e) {
        final data = WorkTimeGraph.fromMap(e.toJson());
        return MapEntry((data.startTime!.weekday + 6) % 7, data);
      });

      emit(
        SuccessWorkTimeState(
          workTime: Map.fromEntries(workTimeGraphList),
        ),
      );
      event.completer?.complete(null);
    } catch (e, s) {
      event.completer?.completeError(e, s);
    }
  }
}
