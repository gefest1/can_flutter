part of './work_time_bloc.dart';

abstract class WorkTimeState extends Equatable {
  const WorkTimeState();

  @override
  List<Object?> get props => [];
}

class LoadingWorkTimeState extends WorkTimeState {}

class SuccessWorkTimeState extends WorkTimeState {
  final Map<int, WorkTimeGraph> workTime;

  const SuccessWorkTimeState({
    required this.workTime,
  });
  @override
  List<Object?> get props => [workTime];
}
