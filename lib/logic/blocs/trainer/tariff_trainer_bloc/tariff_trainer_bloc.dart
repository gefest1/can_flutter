import 'dart:async';
import 'dart:developer';

import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

import 'package:graphql/client.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:tube/logic/api/graphql_client.dart';

import 'package:tube/logic/blocs/tariff/model/tariff.model.dart';

import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/utils/date_time_parse.dart';

part 'tariff_trainer_state.dart';
part 'tariff_trainer_event.dart';
part 'reaction/get_soon_tariff_reaction.dart';

class TariffTrainerBloc
    extends HydratedBloc<TariffTrainerEvent, TariffTrainerState> {
  final String clubId;
  late StreamSubscription streamSubscription;

  void onTariff(Tariff tariff) {
    if (state is GetTariffTrainerState) {
      add(
        SetNewTrainTimeEvent(
          dirtTariffList: (state as GetTariffTrainerState).dirtTariffList,
          tariff: tariff,
        ),
      );
    }
  }

  TariffTrainerBloc({
    required this.clubId,
  }) : super(const LoadingTariffTrainerState()) {
    on<GetSoonTariffTrainerEvent>(getSoonTariffReaction);
    on<SetNewTrainTimeEvent>(setNewTrainTimeReaction);
    add(const GetSoonTariffTrainerEvent());
    streamSubscription = graphqlClient
        .subscribe(
      SubscriptionOptions(
        document: gql(r''' subscription tairffWebSocket {
          tairffWebSocket {
            _id
            trainTimes {
              _id
              come
              date
              endTime
              trainerId
            }
          }
        }'''),
      ),
    )
        .listen((event) {
      log('SUB ${event.hasException}');
      if (event.hasException) return;
      onTariff(Tariff.fromMap(event.data!['tairffWebSocket']));
    });
    // streamSubscription = graphqlClient
    //     .subscribe$tairffWebSocket(
    //   Options$Subscription$tairffWebSocket(
    //     fetchPolicy: FetchPolicy.networkOnly,
    //   ),
    // )
    //     .listen((event) {
    //   log('SUB ${event.hasException}');
    //   if (event.hasException) return;
    //   onTariff(Tariff.fromMap(event.parsedData!.toJson()));
    // });
  }

  @override
  Future<void> close() {
    streamSubscription.cancel();
    return super.close();
  }

  @override
  TariffTrainerState? fromJson(Map<String, dynamic> json) {
    return null;
  }

  @override
  Map<String, dynamic>? toJson(TariffTrainerState state) {
    return null;
  }
}
