part of 'tariff_trainer_bloc.dart';

abstract class TariffTrainerEvent {
  const TariffTrainerEvent();
}

class GetSoonTariffTrainerEvent extends TariffTrainerEvent {
  const GetSoonTariffTrainerEvent();
}

class SetNewTrainTimeEvent extends TariffTrainerEvent {
  final List<Tariff> dirtTariffList;
  final Tariff tariff;

  const SetNewTrainTimeEvent({
    required this.dirtTariffList,
    required this.tariff,
  });
}
