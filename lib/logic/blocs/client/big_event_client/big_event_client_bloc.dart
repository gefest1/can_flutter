import 'dart:developer';
import 'package:graphql/client.dart' as graphql;
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:tube/logic/api/graphql_client.dart';
import 'package:tube/logic/blocs/big_event/entities/big_event_entity.dart';

part 'big_event_client_state.dart';
part 'reaction/get_data.dart';
part 'big_event_client_event.dart';

class BigEventClientBloc
    extends HydratedBloc<BigEventClientEvent, BigEventClientState> {
  final String clubId;

  BigEventClientBloc({
    required this.clubId,
  }) : super(const LoadingBigEventClientState()) {
    on<GetBigEventClientEvent>(getDataReaction);
    add(const GetBigEventClientEvent());
  }

  @override
  BigEventClientState? fromJson(Map<String, dynamic> json) => null;

  @override
  Map<String, dynamic>? toJson(BigEventClientState state) => null;
}
