part of 'big_event_client_bloc.dart';

abstract class BigEventClientState {
  const BigEventClientState();
}

class LoadingBigEventClientState extends BigEventClientState {
  const LoadingBigEventClientState();
}

class DataBigEventClientState extends BigEventClientState {
  final List<BigEventEntity> events;

  const DataBigEventClientState({
    required this.events,
  });
}
