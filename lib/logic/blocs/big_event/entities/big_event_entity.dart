// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:equatable/equatable.dart';

import 'package:tube/logic/blocs/big_event/entities/create_big_entity.dart';
import 'package:tube/utils/date_time_parse.dart';

class BigEventEntity extends Equatable {
  final String id;
  final List<String> trainerIds;
  final List<String> clientIds;
  final int durationMin;
  final DateTime date;
  final String clubId;
  final DateTime finishDate;
  final String name;
  final String description;
  final AddressDataEntity addressData;
  final List<String> guestsEmail;

  const BigEventEntity({
    required this.id,
    required this.trainerIds,
    required this.clientIds,
    required this.durationMin,
    required this.date,
    required this.clubId,
    required this.finishDate,
    required this.name,
    required this.description,
    required this.addressData,
    required this.guestsEmail,
  });

  BigEventEntity copyWith({
    String? id,
    List<String>? trainerIds,
    List<String>? clientIds,
    int? durationMin,
    DateTime? date,
    String? clubId,
    DateTime? finishDate,
    String? name,
    String? description,
    AddressDataEntity? addressData,
    List<String>? guestsEmail,
  }) {
    return BigEventEntity(
      id: id ?? this.id,
      trainerIds: trainerIds ?? this.trainerIds,
      clientIds: clientIds ?? this.clientIds,
      durationMin: durationMin ?? this.durationMin,
      date: date ?? this.date,
      clubId: clubId ?? this.clubId,
      finishDate: finishDate ?? this.finishDate,
      name: name ?? this.name,
      description: description ?? this.description,
      addressData: addressData ?? this.addressData,
      guestsEmail: guestsEmail ?? this.guestsEmail,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      '_id': id,
      'trainerIds': trainerIds,
      'clientIds': clientIds,
      'durationMin': durationMin,
      'date': date.millisecondsSinceEpoch,
      'clubId': clubId,
      'finishDate': finishDate.millisecondsSinceEpoch,
      'name': name,
      'description': description,
      'addressData': addressData.toMap(),
      'guestsEmail': guestsEmail,
    };
  }

  factory BigEventEntity.fromMap(Map<String, dynamic> map) {
    return BigEventEntity(
      id: (map["_id"] ?? '') as String,
      trainerIds: List<String>.from(
        map['trainerIds'] ?? const <String>[],
      ),
      clientIds: List<String>.from(
        map['clientIds'] ?? const <String>[],
      ),
      durationMin: (map["durationMin"] ?? 0) as int,
      date: dateTimeParse(map["date"] ?? 0)!,
      clubId: (map["clubId"] ?? '') as String,
      finishDate: dateTimeParse(map["finishDate"] ?? 0)!,
      name: (map["name"] ?? '') as String,
      description: (map["description"] ?? '') as String,
      addressData: AddressDataEntity.fromMap(
        (map["addressData"] ?? Map<String, dynamic>.from({}))
            as Map<String, dynamic>,
      ),
      guestsEmail: List<String>.from(
        map['guestsEmail'] ?? const <String>[],
      ),
    );
  }

  String toJson() => json.encode(toMap());

  factory BigEventEntity.fromJson(String source) =>
      BigEventEntity.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  bool get stringify => true;

  @override
  List<Object> get props {
    return [
      id,
      trainerIds,
      clientIds,
      durationMin,
      date,
      clubId,
      finishDate,
      name,
      description,
      addressData,
      guestsEmail,
    ];
  }
}
