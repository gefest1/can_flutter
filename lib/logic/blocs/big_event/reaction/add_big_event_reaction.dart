part of '../big_event_bloc.dart';

extension AddBigEventReaction on AdminBigEventBloc {
  void addBigEventReaction(
    AddBigEventEvent event,
    Emitter<AdminBigEventState> emit,
  ) async {
    final state = this.state;
    try {
      final preData = <BigEventEntity>[];
      if (state is DataAdminBigEventState) {
        preData.addAll(state.bigEvents);
      }
      final mutRes = await graphqlClient.mutate$createBigEvent(
        Options$Mutation$createBigEvent(
          fetchPolicy: FetchPolicy.networkOnly,
          variables: Variables$Mutation$createBigEvent(
            trainerIds: event.entity.trainerIds,
            clientIds: event.entity.clientIds,
            durationMin: event.entity.durationMin,
            name: event.entity.name,
            description: event.entity.description,
            addressData: Input$AddressDataInput(
              city: event.entity.addressData.city,
              country: event.entity.addressData.country,
              house: event.entity.addressData.house,
              street: event.entity.addressData.street,
            ),
            date: event.entity.date,
            clubId: event.entity.clubId,
            guestsEmail: event.entity.guestsEmail,
          ),
        ),
      );

      if (mutRes.hasException) throw mutRes.exception!;

      event.completer?.complete();
      final newEvent =
          BigEventEntity.fromMap(mutRes.parsedData!.createBigEvent.toJson());
      //
      emit(
        DataAdminBigEventState(
          bigEvents: [
            ...preData,
            newEvent,
          ],
        ),
      );
      add(const GetAdminBigEventEvent());

      //
    } catch (e, s) {
      if (event.completer == null) rethrow;
      event.completer?.completeError(e, s);
    }
  }
}
