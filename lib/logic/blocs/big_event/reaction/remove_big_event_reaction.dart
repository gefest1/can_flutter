part of '../big_event_bloc.dart';

extension RemoveBigEventReaction on AdminBigEventBloc {
  void removeBigEventReaction(
    RemoveAdminBigEventEvent event,
    Emitter<AdminBigEventState> emit,
  ) async {
    final state = this.state;
    try {
      final preData = <BigEventEntity>[];
      if (state is DataAdminBigEventState) {
        preData.addAll(state.bigEvents);
      }
      final mutRes = await graphqlClient.mutate$removeBigEvent(
        Options$Mutation$removeBigEvent(
          variables:
              Variables$Mutation$removeBigEvent(bigEventId: event.bigEventId),
        ),
      );

      if (mutRes.hasException) throw mutRes.exception!;
      event.completer?.complete();

      emit(
        DataAdminBigEventState(
          bigEvents: preData
              .where((element) => element.id != event.bigEventId)
              .toList(),
        ),
      );
      add(const GetAdminBigEventEvent());
    } catch (e, s) {
      if (event.completer == null) rethrow;
      event.completer?.completeError(e, s);
    }
  }
}
