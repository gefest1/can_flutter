part of '../active_tariff_bloc.dart';

extension GetData on ActiveTariffBloc {
  void getDataActiveTariffReaction(
    GetDataActiveTariffEvent event,
    Emitter<ActiveTariffState> emit,
  ) async {
    try {
      final queryResult = await graphqlClient.query$getActive(
        Options$Query$getActive(
          variables: Variables$Query$getActive(clubId: clubId),
          fetchPolicy: FetchPolicy.networkOnly,
        ),
      );
      // final queryResult = await client.query(
      //   QueryOptions(
      //     document: getActiveDocs,
      //     variables: {
      //       "clubId": clubId,
      //     },
      //     fetchPolicy: FetchPolicy.networkOnly,
      //   ),
      // );

      if (queryResult.hasException) throw queryResult.exception!;
      final List<TariffSingle> listTariffSingle = [];
      final List<Tariff> listTariff = queryResult.parsedData!.getActive
          .map((e) => Tariff.fromMap(e.toJson()))
          .toList();

      for (final singleTariff in listTariff) {
        for (final TrainTime singleTrainTime in singleTariff.trainTimes ?? []) {
          if (singleTrainTime.come == true &&
              DateTime.now().compareTo(singleTrainTime.endTime!) == -1) {
            final toAdd =
                TariffSingle.fromMapTariff(singleTariff, singleTrainTime);
            listTariffSingle.add(toAdd);
          }
        }
      }
      emit(DataActiveTariffState(listTariffSingle));
    } on OperationException catch (_) {
      log(_.toString(), name: 'OperationException');
      add(event);
      rethrow;
    } catch (e) {
      log(e.toString());
    }
  }
}
