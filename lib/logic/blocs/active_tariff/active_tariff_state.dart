part of './active_tariff_bloc.dart';

abstract class ActiveTariffState extends Equatable {
  const ActiveTariffState();
}

class LoadingActiveTariffState extends ActiveTariffState {
  const LoadingActiveTariffState();

  @override
  List<Object?> get props => [];
}

class DataActiveTariffState extends ActiveTariffState {
  final List<TariffSingle> tariffs;

  const DataActiveTariffState(this.tariffs);

  @override
  List<Object?> get props => [tariffs];
}
