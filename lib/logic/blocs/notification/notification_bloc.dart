import 'package:equatable/equatable.dart';
import 'package:graphql/client.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:tube/logic/api/graphql_client.dart';
import 'package:tube/logic/blocs/notification/model/notification.model.dart';
import 'package:tube/logic/model/status_enum.dart';

part './notification_state.dart';
part './notification_event.dart';
part './reaction/get_notification_reaction.dart';

class NotificationBloc
    extends HydratedBloc<NotificationEvent, NotificationState> {
  final StatusEnum participance;
  final String clubId;

  @override
  String get id => "{$clubId}_${participance.toString()}";

  NotificationBloc({
    required this.participance,
    required this.clubId,
  }) : super(const InitNotificationState()) {
    on<GetNotificationEvent>(getData);
    add(const GetNotificationEvent());
  }

  @override
  Map<String, dynamic>? toJson(NotificationState state) {
    return null;
  }

  @override
  NotificationState? fromJson(Map<String, dynamic> json) {
    return null;
  }
}
