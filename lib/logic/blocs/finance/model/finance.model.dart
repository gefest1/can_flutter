import 'dart:convert';

import 'package:equatable/equatable.dart';

import 'package:tube/logic/blocs/club/club.model.dart';
import 'package:tube/logic/blocs/tariff/model/tariff.model.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/utils/date_time_parse.dart';

class TypeFinance {
  final String _value;

  const TypeFinance._internal(this._value);

  @override
  String toString() => _value;
  String toMap() => _value;

  static const TypeFinance booking = TypeFinance._internal('Booking');

  static TypeFinance? fromMap(String? value) {
    if (booking._value == value) return booking;

    if (value == null) return null;
    return TypeFinance._internal(value);
  }
}

class Finance extends Equatable {
  final String? id; //: String!
  final Booking? booking; //: BookingGraph
  final String? bookingId; //: String!
  final Club? club; //: ClubGraph
  final String? clubId; //: String!
  final DateTime? createdAt; //: DateTime!
  final String? displayValue; //: String!
  final bool? isFull; //: Boolean!
  final String? subLabel; //: String
  final Tariff? tariff; //: TariffGraph
  final String? tariffId; //: String!
  final User? trainer; //: UserGraph
  final String? trainerId; //: String
  final TypeFinance? type; //: TypeFinance!
  final User? user; //: UserGraph
  final String? userId; //: String!
  final num? value; //: Float!

  const Finance({
    this.id,
    this.booking,
    this.bookingId,
    this.club,
    this.clubId,
    this.createdAt,
    this.displayValue,
    this.isFull,
    this.subLabel,
    this.tariff,
    this.tariffId,
    this.trainer,
    this.trainerId,
    this.type,
    this.user,
    this.userId,
    this.value,
  });

  Finance copyWith({
    String? id,
    Booking? booking,
    String? bookingId,
    Club? club,
    String? clubId,
    DateTime? createdAt,
    String? displayValue,
    bool? isFull,
    String? subLabel,
    Tariff? tariff,
    String? tariffId,
    User? trainer,
    String? trainerId,
    TypeFinance? type,
    User? user,
    String? userId,
    num? value,
  }) {
    return Finance(
      id: id ?? this.id,
      booking: booking ?? this.booking,
      bookingId: bookingId ?? this.bookingId,
      club: club ?? this.club,
      clubId: clubId ?? this.clubId,
      createdAt: createdAt ?? this.createdAt,
      displayValue: displayValue ?? this.displayValue,
      isFull: isFull ?? this.isFull,
      subLabel: subLabel ?? this.subLabel,
      tariff: tariff ?? this.tariff,
      tariffId: tariffId ?? this.tariffId,
      trainer: trainer ?? this.trainer,
      trainerId: trainerId ?? this.trainerId,
      type: type ?? this.type,
      user: user ?? this.user,
      userId: userId ?? this.userId,
      value: value ?? this.value,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'booking': booking?.toMap(),
      'bookingId': bookingId,
      'club': club?.toMap(),
      'clubId': clubId,
      'createdAt': createdAt?.toIso8601String(),
      'displayValue': displayValue,
      'isFull': isFull,
      'subLabel': subLabel,
      'tariff': tariff?.toMap(),
      'tariffId': tariffId,
      'trainer': trainer?.toMap(),
      'trainerId': trainerId,
      'type': type?.toMap(),
      'user': user?.toMap(),
      'userId': userId,
      'value': value,
    };
  }

  factory Finance.fromMap(Map<String, dynamic> map) {
    return Finance(
      id: map['_id'],
      booking: map['booking'] != null ? Booking.fromMap(map['booking']) : null,
      bookingId: map['bookingId'],
      club: map['club'] != null ? Club.fromMap(map['club']) : null,
      clubId: map['clubId'],
      createdAt:
          map['createdAt'] != null ? dateTimeParse(map['createdAt']) : null,
      displayValue: map['displayValue'],
      isFull: map['isFull'],
      subLabel: map['subLabel'],
      tariff: map['tariff'] != null ? Tariff.fromMap(map['tariff']) : null,
      tariffId: map['tariffId'],
      trainer: map['trainer'] != null ? User.fromMap(map['trainer']) : null,
      trainerId: map['trainerId'],
      type: map['type'] != null ? TypeFinance.fromMap(map['type']) : null,
      user: map['user'] != null ? User.fromMap(map['user']) : null,
      userId: map['userId'],
      value: map['value'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Finance.fromJson(String source) =>
      Finance.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Finance(id: $id, booking: $booking, bookingId: $bookingId, club: $club, clubId: $clubId, createdAt: $createdAt, displayValue: $displayValue, isFull: $isFull, subLabel: $subLabel, tariff: $tariff, tariffId: $tariffId, trainer: $trainer, trainerId: $trainerId, type: $type, user: $user, userId: $userId, value: $value)';
  }

  @override
  List<Object?> get props {
    return [
      id,
      booking,
      bookingId,
      club,
      clubId,
      createdAt,
      displayValue,
      isFull,
      subLabel,
      tariff,
      tariffId,
      trainer,
      trainerId,
      type,
      user,
      userId,
      value,
    ];
  }
}
