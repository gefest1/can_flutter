part of './finance_bloc.dart';

abstract class FinanceEvent {
  const FinanceEvent();
}

class GetFinanceEvent extends FinanceEvent {
  final DateTime startTime;
  final DateTime endTime;

  const GetFinanceEvent({
    required this.startTime,
    required this.endTime,
  });
}
