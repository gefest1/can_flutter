part of '../user_bloc.dart';

extension InitUserEventExtension on UserBloc {
  void initUserEventReaction(InitUserEvent event, Emitter<UserState> emitter) {
    emitter(DataUserState(event.user));
  }
}
