part of './user_bloc.dart';

abstract class UserState extends Equatable {
  const UserState();
}

class LoadingUserState extends UserState {
  const LoadingUserState();

  @override
  List<Object?> get props => [];
}

class DataUserState extends UserState {
  final User user;

  const DataUserState(this.user);

  @override
  List<Object?> get props => [user];
}

class InitUserState extends UserState {
  const InitUserState();

  @override
  List<Object?> get props => [];
}
