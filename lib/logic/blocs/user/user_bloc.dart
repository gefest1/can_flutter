import 'dart:developer';
import 'dart:typed_data';

import 'package:equatable/equatable.dart';
import 'package:graphql/client.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:tube/logic/api/graphql_client.dart';
import 'package:tube/logic/blocs/user/user.model.dart';

import 'package:tube/utils/img_parser.dart';
import 'package:http/http.dart' as http;

part './user_state.dart';
part './user_event.dart';
part 'reaction/edit_user_data.dart';
part 'reaction/init_user.reaction.dart';
part 'reaction/get_data_reaction.dart';

class UserBloc extends HydratedBloc<UserEvent, UserState> {
  UserBloc() : super(const InitUserState()) {
    on<InitUserEvent>(initUserEventReaction);
    on<GetDataUserEvent>(getData);
    on<EditUserEvent>(editUserData);
    add(const GetDataUserEvent());
  }

  @override
  UserState? fromJson(Map<String, dynamic> json) {
    if (json['type'] == "InitUserEvent") {
      return DataUserState(
        User.fromMap(json['data']),
      );
    }
    return const InitUserState();
  }

  @override
  Map<String, dynamic>? toJson(UserState state) {
    if (state is InitUserEvent) {
      return {
        "type": "InitUserEvent",
        "data": (state as InitUserEvent).user.toMap(),
      };
    }
    return null;
  }
}
