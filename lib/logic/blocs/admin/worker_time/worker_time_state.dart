part of './worker_time_bloc.dart';

abstract class WorkTimeState extends Equatable {
  const WorkTimeState();
}

class LoadingWorkTimeState extends WorkTimeState {
  const LoadingWorkTimeState();

  @override
  List<Object?> get props => [];
}

class DataWorkTimeState extends WorkTimeState {
  final String clubId;
  final String userId;
  final Map<int, WorkTimeGraph> workTime;

  const DataWorkTimeState({
    required this.clubId,
    required this.userId,
    required this.workTime,
  });

  @override
  List<Object?> get props => [
        clubId,
        userId,
        workTime,
      ];
}
