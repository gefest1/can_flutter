part of './worker_time_bloc.dart';

abstract class WorkTimeEvent {
  const WorkTimeEvent();
}

class GetDataWorkTimeEvent extends WorkTimeEvent {
  final String clubId;
  final String userId;

  const GetDataWorkTimeEvent({
    required this.clubId,
    required this.userId,
  });
}
