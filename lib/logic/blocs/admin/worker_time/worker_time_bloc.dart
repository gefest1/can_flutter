import 'dart:developer';

import 'package:equatable/equatable.dart';
import 'package:graphql/client.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:tube/logic/api/graphql_client.dart';
import 'package:tube/logic/model/work_time_graph.dart';

part 'reaction/get_worker.dart';
part 'worker_time_event.dart';
part 'worker_time_state.dart';

class WorkerTimeBloc extends HydratedBloc<WorkTimeEvent, WorkTimeState> {
  final String clubId;
  final String userId;

  WorkerTimeBloc({
    required this.clubId,
    required this.userId,
  }) : super(const LoadingWorkTimeState()) {
    on<GetDataWorkTimeEvent>(getDataAboutUser);
    add(
      GetDataWorkTimeEvent(
        clubId: clubId,
        userId: userId,
      ),
    );
  }

  @override
  Map<String, dynamic>? toJson(WorkTimeState state) {
    return null;
  }

  @override
  WorkTimeState? fromJson(Map<String, dynamic> json) {
    return null;
  }
}
