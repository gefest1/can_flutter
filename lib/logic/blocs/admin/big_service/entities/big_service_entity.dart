// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:equatable/equatable.dart';

class BigServiceEntity extends Equatable {
  final String id;
  final String name;
  final num price;
  final String clubId;
  final num percent;
  final List<String> trainerIds;
  final String description;
  final int durationMin;
  const BigServiceEntity({
    required this.id,
    required this.name,
    required this.price,
    required this.clubId,
    required this.percent,
    required this.trainerIds,
    required this.description,
    required this.durationMin,
  });

  BigServiceEntity copyWith({
    String? id,
    String? name,
    num? price,
    String? clubId,
    num? percent,
    List<String>? trainerIds,
    String? description,
    int? durationMin,
  }) {
    return BigServiceEntity(
      id: id ?? this.id,
      name: name ?? this.name,
      price: price ?? this.price,
      clubId: clubId ?? this.clubId,
      percent: percent ?? this.percent,
      trainerIds: trainerIds ?? this.trainerIds,
      description: description ?? this.description,
      durationMin: durationMin ?? this.durationMin,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      '_id': id,
      'name': name,
      'price': price,
      'clubId': clubId,
      'percent': percent,
      'trainerIds': trainerIds,
      'description': description,
      'durationMin': durationMin,
    };
  }

  factory BigServiceEntity.fromMap(Map<String, dynamic> map) {
    return BigServiceEntity(
      id: (map["_id"] ?? '') as String,
      name: (map["name"] ?? '') as String,
      price: (map["price"] ?? 0) as num,
      clubId: (map["clubId"] ?? '') as String,
      percent: (map["percent"] ?? 0) as num,
      trainerIds: List<String>.from(map['trainerIds'] ?? const <String>[]),
      description: (map["description"] ?? '') as String,
      durationMin: (map["durationMin"] ?? 0) as int,
    );
  }

  String toJson() => json.encode(toMap());

  factory BigServiceEntity.fromJson(String source) =>
      BigServiceEntity.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  bool get stringify => true;

  @override
  List<Object> get props {
    return [
      id,
      name,
      price,
      clubId,
      percent,
      trainerIds,
      description,
      durationMin,
    ];
  }
}
