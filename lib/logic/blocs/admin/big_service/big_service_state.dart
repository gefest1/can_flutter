part of './big_service_bloc.dart';

abstract class AdminBigServiceState {
  const AdminBigServiceState();
}

class LoadingAdminBigServiceState extends AdminBigServiceState {
  const LoadingAdminBigServiceState();
}

class DataAdminBigServiceState extends AdminBigServiceState {
  final List<BigServiceEntity> data;

  const DataAdminBigServiceState({required this.data});
}
