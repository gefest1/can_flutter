import 'dart:async';
import 'dart:developer';

import 'package:graphql/client.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:tube/logic/blocs/admin/big_service/entities/big_service_entity.dart';
import 'package:tube/main.dart';

part 'big_service_state.dart';
part 'big_service_event.dart';
part 'reaction/add_big_service.dart';

part 'reaction/get_admin_service.dart';

class AdminBigServiceBloc
    extends HydratedBloc<AdminBigServiceEvent, AdminBigServiceState> {
  final String clubId;
  AdminBigServiceBloc({
    required this.clubId,
  }) : super(const LoadingAdminBigServiceState()) {
    on<AddAdminBigServiceEvent>(addBigService);
    on<GetAdminBigServiceEvent>(getAdminServiceReaction);
    add(const GetAdminBigServiceEvent());
  }

  @override
  AdminBigServiceState? fromJson(Map<String, dynamic> json) => null;

  @override
  Map<String, dynamic>? toJson(AdminBigServiceState state) => null;
}
