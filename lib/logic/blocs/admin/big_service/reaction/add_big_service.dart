part of '../big_service_bloc.dart';

class CreateBigServiceEntity {
  final String name;
  final String description;
  final int durationMin;
  final List<String> trainerIds;
  final num price;
  final num percent;
  final String clubId;
  const CreateBigServiceEntity({
    required this.name,
    required this.description,
    required this.durationMin,
    required this.trainerIds,
    required this.price,
    required this.percent,
    required this.clubId,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'name': name,
      'description': description,
      'durationMin': durationMin,
      'trainerIds': trainerIds,
      'price': price,
      'percent': percent,
      'clubId': clubId,
    };
  }
}

extension AddBigService on AdminBigServiceBloc {
  void addBigService(
    AddAdminBigServiceEvent event,
    Emitter<AdminBigServiceState> emit,
  ) async {
    try {
      final mutationRes = await graphqlClient.mutate$createService(
        Options$Mutation$createService(
          variables: Variables$Mutation$createService(
            name: event.entity.name,
            description: event.entity.description,
            durationMin: event.entity.durationMin,
            trainerIds: event.entity.trainerIds,
            price: event.entity.price.toDouble(),
            percent: event.entity.percent.toDouble(),
            clubId: event.entity.clubId,
          ),
          fetchPolicy: FetchPolicy.networkOnly,
        ),
      );
      if (mutationRes.hasException) throw mutationRes.exception!;
      event.completer?.complete();
    } catch (e, s) {
      if (event.completer == null) rethrow;
      event.completer?.completeError(e, s);
    }
  }
}
