import 'package:graphql/client.dart';

final getNumberClubId = gql('''
query (\$bookingId: String!){
  getNumberClubId(bookingId: \$bookingId){
    activeTariffs {
      clientId
      _id
      trainCount
      deadlineDate
    }
    trainers {
			_id
      iin
      date
      email
      fullName
      photoUrl {
        M
        XL
        thumbnail
      }
      phoneNumber
    }
  }
}''');
