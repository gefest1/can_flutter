class EditPrice {
  final double price;
  final double percent;
  final double? trialPrice;

  const EditPrice({
    required this.price,
    required this.percent,
    this.trialPrice,
  });
}
