class EditDescription {
  final String name;
  final String description;

  const EditDescription({
    required this.name,
    required this.description,
  });
}
