import 'package:tube/logic/model/admin/edit_booking.model.dart';

class EditSchedule {
  final WorkTimeType workTime;

  const EditSchedule({required this.workTime});
}
