part of '../booking_admin_bloc.dart';

extension GetBookingAdminReaction on BookingAdminBloc {
  void getBookingAdminReaction(
    GetBookingAdminEvent event,
    Emitter<BookingAdminState> emit,
  ) async {
    final queryResult = await graphqlClient.query$getBookingsOfClub(
      Options$Query$getBookingsOfClub(
        variables: Variables$Query$getBookingsOfClub(clubId: clubId),
        fetchPolicy: FetchPolicy.networkOnly,
      ),
    );

    if (queryResult.hasException) throw queryResult.exception!;

    final listBooking = queryResult.parsedData!.getBookingsOfClub
        .map((e) => Booking.fromMap(e.toJson()))
        .toList();
    emit(DataBookingAdminState(bookings: listBooking));
  }

  void removeBooking(
    RemoveBookingAdminEvent event,
    Emitter<BookingAdminState> emit,
  ) async {
    try {
      final mutationRes = await graphqlClient.mutate$removeBooking(
        Options$Mutation$removeBooking(
          variables:
              Variables$Mutation$removeBooking(bookingId: event.bookingId),
          fetchPolicy: FetchPolicy.networkOnly,
        ),
      );

      if (mutationRes.hasException) throw mutationRes.exception!;
      add(const GetBookingAdminEvent());
      event.completer?.complete();
    } catch (e, s) {
      event.completer?.completeError(e, s);
    }
  }
}
