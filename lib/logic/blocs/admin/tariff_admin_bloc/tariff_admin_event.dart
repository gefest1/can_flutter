part of 'tariff_admin_bloc.dart';

abstract class TariffAdminEvent {
  const TariffAdminEvent();
}

class GetSoonTariffAdminEvent extends TariffAdminEvent {
  const GetSoonTariffAdminEvent();
}
