part of '../tariff_admin_bloc.dart';

extension TariffReaction on TariffAdminBloc {
  void getSoonTariffReaction(
    GetSoonTariffAdminEvent event,
    Emitter<TariffAdminState> emit,
  ) async {
    try {
      final queryResult = await graphqlClient.query$getSoonTariffClubId(
        Options$Query$getSoonTariffClubId(
          variables: Variables$Query$getSoonTariffClubId(
            clubId: clubId,
          ),
        ),
      );

      if (queryResult.hasException) queryResult.exception!;

      final newResult = queryResult.parsedData!.getSoonTariffClubId
          .map((e) => Tariff.fromMap(e.toJson()))
          .toList();

      emit(GetTariffAdminState(dirtTariffList: newResult));
    } catch (e) {
      log(e.toString());
    }
  }
}
