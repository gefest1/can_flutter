import 'dart:async';
import 'package:equatable/equatable.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:http/http.dart' show MultipartFile;
import 'package:image_picker/image_picker.dart';
import 'package:tube/logic/api/graphql_client.dart';
import 'package:tube/logic/blocs/club/club.model.dart';

import 'package:tube/utils/img_parser.dart';

part 'club_state.dart';
part 'club_event.dart';
part 'reaction/create_club.dart';

class ClubBloc extends HydratedBloc<ClubEvent, ClubState> {
  ClubBloc() : super(InitClubState()) {
    on<CreateClubEvent>(createClubReaction);
  }

  @override
  ClubState? fromJson(Map<String, dynamic> json) {
    return null;
  }

  @override
  Map<String, dynamic>? toJson(ClubState state) {
    return null;
  }
}
