part of './club_bloc.dart';

abstract class ClubEvent {}

class CreateClubEvent extends ClubEvent {
  final String name;
  final String type;
  final String description;
  final XFile file;
  final Completer<Club>? completer;

  CreateClubEvent({
    required this.name,
    required this.type,
    required this.description,
    required this.file,
    this.completer,
  });
}
