part of '../participance_bloc.dart';

extension TagReactions on ParticipanceBloc {
  void addTagReaction(
    AddTagParticipanceEvent event,
    Emitter<ParticipanceState> emit,
  ) async {
    final state = this.state;
    List<Participance> preData;
    try {
      if (state is DataParticipanceState) {
        preData = state.participances;
      } else {
        preData = [];
      }
      final mutRes = await graphqlClient.mutate$addTag(
        Options$Mutation$addTag(
          variables: Variables$Mutation$addTag(
            clubId: event.clubId,
            tag: event.tagName,
          ),
        ),
      );

      if (mutRes.hasException) throw mutRes.exception!;
      final newClub = Club.fromMap(mutRes.parsedData!.addTag.toJson());
      final newParticipances = preData
          .map((e) => e.clubId == newClub.id ? e.copyWith(club: newClub) : e)
          .toList();
      emit(
        DataParticipanceState(
          newParticipances,
          Map.fromEntries(
            newParticipances.map(
              (e) => MapEntry(e.clubId!, e),
            ),
          ),
        ),
      );
      event.completer?.complete();
    } catch (e, s) {
      if (event.completer == null) rethrow;
      event.completer?.completeError(e, s);
    }
  }

  void editTagReaction(
    EditTagParticipanceEvent event,
    Emitter<ParticipanceState> emit,
  ) async {
    final state = this.state;
    List<Participance> preData;
    try {
      if (state is DataParticipanceState) {
        preData = state.participances;
      } else {
        preData = [];
      }
      final mutRes = await graphqlClient.mutate$editTag(
        Options$Mutation$editTag(
          variables: Variables$Mutation$editTag(
            clubId: event.clubId,
            tag: event.tagName,
            oldTag: event.oldTagName,
          ),
          fetchPolicy: FetchPolicy.networkOnly,
        ),
      );

      if (mutRes.hasException) throw mutRes.exception!;
      final newClub = Club.fromMap(mutRes.parsedData!.editTag.toJson());
      final newParticipances = preData
          .map((e) => e.clubId == newClub.id ? e.copyWith(club: newClub) : e)
          .toList();
      emit(
        DataParticipanceState(
          newParticipances,
          Map.fromEntries(
            newParticipances.map(
              (e) => MapEntry(e.clubId!, e),
            ),
          ),
        ),
      );
      event.completer?.complete();
    } catch (e, s) {
      if (event.completer == null) rethrow;
      event.completer?.completeError(e, s);
    }
  }

  void removeTagReaction(
    RemoveTagParticipanceEvent event,
    Emitter<ParticipanceState> emit,
  ) async {
    final state = this.state;
    List<Participance> preData;
    try {
      if (state is DataParticipanceState) {
        preData = state.participances;
      } else {
        preData = [];
      }
      final mutRes = await graphqlClient.mutate$removeTag(
        Options$Mutation$removeTag(
          variables: Variables$Mutation$removeTag(
            clubId: event.clubId,
            tag: event.tagName,
          ),
          fetchPolicy: FetchPolicy.networkOnly,
        ),
      );

      if (mutRes.hasException) throw mutRes.exception!;
      final newClub = Club.fromMap(mutRes.parsedData!.removeTag.toJson());
      final newParticipances = preData
          .map((e) => e.clubId == newClub.id ? e.copyWith(club: newClub) : e)
          .toList();
      emit(
        DataParticipanceState(
          newParticipances,
          Map.fromEntries(
            newParticipances.map(
              (e) => MapEntry(e.clubId!, e),
            ),
          ),
        ),
      );
      event.completer?.complete();
    } catch (e, s) {
      if (event.completer == null) rethrow;
      event.completer?.completeError(e, s);
    }
  }
}
