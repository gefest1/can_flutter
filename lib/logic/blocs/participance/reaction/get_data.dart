part of '../participance_bloc.dart';

extension GetDataParticipanceEventExtension on ParticipanceBloc {
  void getDataParticipanceEventReaction(
    GetDataParticipanceEvent event,
    Emitter<ParticipanceState> emitter,
  ) async {
    try {
      if (event.recursion == 5) return;
      final queryResult = await graphqlClient.query$getParticipanceQuery(
        Options$Query$getParticipanceQuery(
          fetchPolicy: FetchPolicy.networkOnly,
        ),
      );

      if (queryResult.hasException) throw queryResult.exception!;
      final List<Participance> parts = queryResult.parsedData!.getParticipance
          .map((e) => Participance.fromMap(e.toJson()))
          .toList();
      if (parts.isNotEmpty) {
        MainDrawerState.currentParticipance =
            parts[MainDrawerState.index ??= 0];
        emitter(DataParticipanceState(parts, {
          for (Participance part in parts) part.clubId!: part,
        }));
      }
      if (parts.isEmpty) emitter(const DataParticipanceState([], {}));
    } on OperationException catch (_) {
      log(_.toString());
      add(GetDataParticipanceEvent(recursion: event.recursion + 1));
      rethrow;
    } catch (e) {
      add(GetDataParticipanceEvent(recursion: event.recursion + 1));
      log(e.toString());
    }
  }
}
