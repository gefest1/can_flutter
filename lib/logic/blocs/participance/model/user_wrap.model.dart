import 'dart:convert';

import 'package:equatable/equatable.dart';

import 'package:tube/logic/blocs/participance/model/participance.model.dart';

class UserWrapParticipance extends Equatable {
  final String? userId;
  final Participance? participances;

  const UserWrapParticipance({
    this.userId,
    this.participances,
  });

  UserWrapParticipance copyWith({
    String? userId,
    Participance? participances,
  }) {
    return UserWrapParticipance(
      userId: userId ?? this.userId,
      participances: participances ?? this.participances,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'userId': userId,
      'participances': participances?.toMap(),
    };
  }

  factory UserWrapParticipance.fromMap(Map<String, dynamic> map) {
    return UserWrapParticipance(
      userId: map['userId'],
      participances: map['participances'] != null
          ? Participance.fromMap(map['participances'])
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory UserWrapParticipance.fromJson(String source) =>
      UserWrapParticipance.fromMap(json.decode(source));

  @override
  String toString() =>
      'UserWrapParticipance(userId: $userId, participances: $participances)';

  @override
  List<Object?> get props => [userId, participances];
}
