part of './participance_bloc.dart';

abstract class ParticipanceState extends Equatable {
  const ParticipanceState();
}

class LoadingParticipanceState extends ParticipanceState {
  final ParticipanceState? prevState;

  const LoadingParticipanceState([this.prevState]);

  @override
  List<Object?> get props => [prevState];
}

class DataParticipanceState extends ParticipanceState {
  final List<Participance> participances;
  final Map<String, Participance> mapParticipances;

  const DataParticipanceState(
    this.participances,
    this.mapParticipances,
  );

  @override
  List<Object?> get props => [participances, mapParticipances];
}
