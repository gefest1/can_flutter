import 'dart:async';
import 'package:flutter/widgets.dart' as widgets;
import 'package:gql/ast.dart';
import 'package:graphql/client.dart' as graphql;
import 'package:graphql_flutter/graphql_flutter.dart' as graphql_flutter;
import 'package:tube/utils/date_time_parse.dart';

class Subscription$tairffWebSocket {
  Subscription$tairffWebSocket({
    this.tairffWebSocket,
    this.$__typename = 'Subscription',
  });

  factory Subscription$tairffWebSocket.fromJson(Map<String, dynamic> json) {
    final l$tairffWebSocket = json['tairffWebSocket'];
    final l$$__typename = json['__typename'];
    return Subscription$tairffWebSocket(
      tairffWebSocket: l$tairffWebSocket == null
          ? null
          : Subscription$tairffWebSocket$tairffWebSocket.fromJson(
              (l$tairffWebSocket as Map<String, dynamic>)),
      $__typename: (l$$__typename as String),
    );
  }

  final Subscription$tairffWebSocket$tairffWebSocket? tairffWebSocket;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$tairffWebSocket = tairffWebSocket;
    _resultData['tairffWebSocket'] = l$tairffWebSocket?.toJson();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$tairffWebSocket = tairffWebSocket;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$tairffWebSocket,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Subscription$tairffWebSocket) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$tairffWebSocket = tairffWebSocket;
    final lOther$tairffWebSocket = other.tairffWebSocket;
    if (l$tairffWebSocket != lOther$tairffWebSocket) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Subscription$tairffWebSocket
    on Subscription$tairffWebSocket {
  CopyWith$Subscription$tairffWebSocket<Subscription$tairffWebSocket>
      get copyWith => CopyWith$Subscription$tairffWebSocket(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Subscription$tairffWebSocket<TRes> {
  factory CopyWith$Subscription$tairffWebSocket(
    Subscription$tairffWebSocket instance,
    TRes Function(Subscription$tairffWebSocket) then,
  ) = _CopyWithImpl$Subscription$tairffWebSocket;

  factory CopyWith$Subscription$tairffWebSocket.stub(TRes res) =
      _CopyWithStubImpl$Subscription$tairffWebSocket;

  TRes call({
    Subscription$tairffWebSocket$tairffWebSocket? tairffWebSocket,
    String? $__typename,
  });
  CopyWith$Subscription$tairffWebSocket$tairffWebSocket<TRes>
      get tairffWebSocket;
}

class _CopyWithImpl$Subscription$tairffWebSocket<TRes>
    implements CopyWith$Subscription$tairffWebSocket<TRes> {
  _CopyWithImpl$Subscription$tairffWebSocket(
    this._instance,
    this._then,
  );

  final Subscription$tairffWebSocket _instance;

  final TRes Function(Subscription$tairffWebSocket) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? tairffWebSocket = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Subscription$tairffWebSocket(
        tairffWebSocket: tairffWebSocket == _undefined
            ? _instance.tairffWebSocket
            : (tairffWebSocket
                as Subscription$tairffWebSocket$tairffWebSocket?),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
  CopyWith$Subscription$tairffWebSocket$tairffWebSocket<TRes>
      get tairffWebSocket {
    final local$tairffWebSocket = _instance.tairffWebSocket;
    return local$tairffWebSocket == null
        ? CopyWith$Subscription$tairffWebSocket$tairffWebSocket.stub(
            _then(_instance))
        : CopyWith$Subscription$tairffWebSocket$tairffWebSocket(
            local$tairffWebSocket, (e) => call(tairffWebSocket: e));
  }
}

class _CopyWithStubImpl$Subscription$tairffWebSocket<TRes>
    implements CopyWith$Subscription$tairffWebSocket<TRes> {
  _CopyWithStubImpl$Subscription$tairffWebSocket(this._res);

  TRes _res;

  call({
    Subscription$tairffWebSocket$tairffWebSocket? tairffWebSocket,
    String? $__typename,
  }) =>
      _res;
  CopyWith$Subscription$tairffWebSocket$tairffWebSocket<TRes>
      get tairffWebSocket =>
          CopyWith$Subscription$tairffWebSocket$tairffWebSocket.stub(_res);
}

const documentNodeSubscriptiontairffWebSocket = DocumentNode(definitions: [
  OperationDefinitionNode(
    type: OperationType.subscription,
    name: NameNode(value: 'tairffWebSocket'),
    variableDefinitions: [],
    directives: [],
    selectionSet: SelectionSetNode(selections: [
      FieldNode(
        name: NameNode(value: 'tairffWebSocket'),
        alias: null,
        arguments: [],
        directives: [],
        selectionSet: SelectionSetNode(selections: [
          FieldNode(
            name: NameNode(value: '_id'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null,
          ),
          FieldNode(
            name: NameNode(value: 'trainTimes'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: SelectionSetNode(selections: [
              FieldNode(
                name: NameNode(value: '_id'),
                alias: null,
                arguments: [],
                directives: [],
                selectionSet: null,
              ),
              FieldNode(
                name: NameNode(value: 'come'),
                alias: null,
                arguments: [],
                directives: [],
                selectionSet: null,
              ),
              FieldNode(
                name: NameNode(value: 'date'),
                alias: null,
                arguments: [],
                directives: [],
                selectionSet: null,
              ),
              FieldNode(
                name: NameNode(value: 'endTime'),
                alias: null,
                arguments: [],
                directives: [],
                selectionSet: null,
              ),
              FieldNode(
                name: NameNode(value: 'trainerId'),
                alias: null,
                arguments: [],
                directives: [],
                selectionSet: null,
              ),
              FieldNode(
                name: NameNode(value: '__typename'),
                alias: null,
                arguments: [],
                directives: [],
                selectionSet: null,
              ),
            ]),
          ),
          FieldNode(
            name: NameNode(value: '__typename'),
            alias: null,
            arguments: [],
            directives: [],
            selectionSet: null,
          ),
        ]),
      ),
      FieldNode(
        name: NameNode(value: '__typename'),
        alias: null,
        arguments: [],
        directives: [],
        selectionSet: null,
      ),
    ]),
  ),
]);
Subscription$tairffWebSocket _parserFn$Subscription$tairffWebSocket(
        Map<String, dynamic> data) =>
    Subscription$tairffWebSocket.fromJson(data);

class Options$Subscription$tairffWebSocket
    extends graphql.SubscriptionOptions<Subscription$tairffWebSocket> {
  Options$Subscription$tairffWebSocket({
    String? operationName,
    graphql.FetchPolicy? fetchPolicy,
    graphql.ErrorPolicy? errorPolicy,
    graphql.CacheRereadPolicy? cacheRereadPolicy,
    Object? optimisticResult,
    Subscription$tairffWebSocket? typedOptimisticResult,
    graphql.Context? context,
  }) : super(
          operationName: operationName,
          fetchPolicy: fetchPolicy,
          errorPolicy: errorPolicy,
          cacheRereadPolicy: cacheRereadPolicy,
          optimisticResult: optimisticResult ?? typedOptimisticResult?.toJson(),
          context: context,
          document: documentNodeSubscriptiontairffWebSocket,
          parserFn: _parserFn$Subscription$tairffWebSocket,
        );
}

class WatchOptions$Subscription$tairffWebSocket
    extends graphql.WatchQueryOptions<Subscription$tairffWebSocket> {
  WatchOptions$Subscription$tairffWebSocket({
    String? operationName,
    graphql.FetchPolicy? fetchPolicy,
    graphql.ErrorPolicy? errorPolicy,
    graphql.CacheRereadPolicy? cacheRereadPolicy,
    Object? optimisticResult,
    Subscription$tairffWebSocket? typedOptimisticResult,
    graphql.Context? context,
    Duration? pollInterval,
    bool? eagerlyFetchResults,
    bool carryForwardDataOnException = true,
    bool fetchResults = false,
  }) : super(
          operationName: operationName,
          fetchPolicy: fetchPolicy,
          errorPolicy: errorPolicy,
          cacheRereadPolicy: cacheRereadPolicy,
          optimisticResult: optimisticResult ?? typedOptimisticResult?.toJson(),
          context: context,
          document: documentNodeSubscriptiontairffWebSocket,
          pollInterval: pollInterval,
          eagerlyFetchResults: eagerlyFetchResults,
          carryForwardDataOnException: carryForwardDataOnException,
          fetchResults: fetchResults,
          parserFn: _parserFn$Subscription$tairffWebSocket,
        );
}

class FetchMoreOptions$Subscription$tairffWebSocket
    extends graphql.FetchMoreOptions {
  FetchMoreOptions$Subscription$tairffWebSocket(
      {required graphql.UpdateQuery updateQuery})
      : super(
          updateQuery: updateQuery,
          document: documentNodeSubscriptiontairffWebSocket,
        );
}

extension ClientExtension$Subscription$tairffWebSocket
    on graphql.GraphQLClient {
  Stream<graphql.QueryResult<Subscription$tairffWebSocket>>
      subscribe$tairffWebSocket(
              [Options$Subscription$tairffWebSocket? options]) =>
          this.subscribe(options ?? Options$Subscription$tairffWebSocket());
  graphql.ObservableQuery<
      Subscription$tairffWebSocket> watchSubscription$tairffWebSocket(
          [WatchOptions$Subscription$tairffWebSocket? options]) =>
      this.watchQuery(options ?? WatchOptions$Subscription$tairffWebSocket());
}

graphql.QueryResult<Subscription$tairffWebSocket>
    useSubscription$tairffWebSocket(
            Options$Subscription$tairffWebSocket options) =>
        graphql_flutter.useSubscription(options);

class Subscription$tairffWebSocket$Widget
    extends graphql_flutter.Subscription<Subscription$tairffWebSocket> {
  Subscription$tairffWebSocket$Widget({
    widgets.Key? key,
    Options$Subscription$tairffWebSocket? options,
    required graphql_flutter.SubscriptionBuilder<Subscription$tairffWebSocket>
        builder,
    graphql_flutter.OnSubscriptionResult<Subscription$tairffWebSocket>?
        onSubscriptionResult,
  }) : super(
          key: key,
          options: options ?? Options$Subscription$tairffWebSocket(),
          builder: builder,
          onSubscriptionResult: onSubscriptionResult,
        );
}

class Subscription$tairffWebSocket$tairffWebSocket {
  Subscription$tairffWebSocket$tairffWebSocket({
    required this.$_id,
    required this.trainTimes,
    this.$__typename = 'TariffGraph',
  });

  factory Subscription$tairffWebSocket$tairffWebSocket.fromJson(
      Map<String, dynamic> json) {
    final l$$_id = json['_id'];
    final l$trainTimes = json['trainTimes'];
    final l$$__typename = json['__typename'];
    return Subscription$tairffWebSocket$tairffWebSocket(
      $_id: (l$$_id as String),
      trainTimes: (l$trainTimes as List<dynamic>)
          .map((e) =>
              Subscription$tairffWebSocket$tairffWebSocket$trainTimes.fromJson(
                  (e as Map<String, dynamic>)))
          .toList(),
      $__typename: (l$$__typename as String),
    );
  }

  final String $_id;

  final List<Subscription$tairffWebSocket$tairffWebSocket$trainTimes>
      trainTimes;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$$_id = $_id;
    _resultData['_id'] = l$$_id;
    final l$trainTimes = trainTimes;
    _resultData['trainTimes'] = l$trainTimes.map((e) => e.toJson()).toList();
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$$_id = $_id;
    final l$trainTimes = trainTimes;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$$_id,
      Object.hashAll(l$trainTimes.map((v) => v)),
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Subscription$tairffWebSocket$tairffWebSocket) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$$_id = $_id;
    final lOther$$_id = other.$_id;
    if (l$$_id != lOther$$_id) {
      return false;
    }
    final l$trainTimes = trainTimes;
    final lOther$trainTimes = other.trainTimes;
    if (l$trainTimes.length != lOther$trainTimes.length) {
      return false;
    }
    for (int i = 0; i < l$trainTimes.length; i++) {
      final l$trainTimes$entry = l$trainTimes[i];
      final lOther$trainTimes$entry = lOther$trainTimes[i];
      if (l$trainTimes$entry != lOther$trainTimes$entry) {
        return false;
      }
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Subscription$tairffWebSocket$tairffWebSocket
    on Subscription$tairffWebSocket$tairffWebSocket {
  CopyWith$Subscription$tairffWebSocket$tairffWebSocket<
          Subscription$tairffWebSocket$tairffWebSocket>
      get copyWith => CopyWith$Subscription$tairffWebSocket$tairffWebSocket(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Subscription$tairffWebSocket$tairffWebSocket<TRes> {
  factory CopyWith$Subscription$tairffWebSocket$tairffWebSocket(
    Subscription$tairffWebSocket$tairffWebSocket instance,
    TRes Function(Subscription$tairffWebSocket$tairffWebSocket) then,
  ) = _CopyWithImpl$Subscription$tairffWebSocket$tairffWebSocket;

  factory CopyWith$Subscription$tairffWebSocket$tairffWebSocket.stub(TRes res) =
      _CopyWithStubImpl$Subscription$tairffWebSocket$tairffWebSocket;

  TRes call({
    String? $_id,
    List<Subscription$tairffWebSocket$tairffWebSocket$trainTimes>? trainTimes,
    String? $__typename,
  });
  TRes trainTimes(
      Iterable<Subscription$tairffWebSocket$tairffWebSocket$trainTimes> Function(
              Iterable<
                  CopyWith$Subscription$tairffWebSocket$tairffWebSocket$trainTimes<
                      Subscription$tairffWebSocket$tairffWebSocket$trainTimes>>)
          _fn);
}

class _CopyWithImpl$Subscription$tairffWebSocket$tairffWebSocket<TRes>
    implements CopyWith$Subscription$tairffWebSocket$tairffWebSocket<TRes> {
  _CopyWithImpl$Subscription$tairffWebSocket$tairffWebSocket(
    this._instance,
    this._then,
  );

  final Subscription$tairffWebSocket$tairffWebSocket _instance;

  final TRes Function(Subscription$tairffWebSocket$tairffWebSocket) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? $_id = _undefined,
    Object? trainTimes = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Subscription$tairffWebSocket$tairffWebSocket(
        $_id: $_id == _undefined || $_id == null
            ? _instance.$_id
            : ($_id as String),
        trainTimes: trainTimes == _undefined || trainTimes == null
            ? _instance.trainTimes
            : (trainTimes as List<
                Subscription$tairffWebSocket$tairffWebSocket$trainTimes>),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
  TRes trainTimes(
          Iterable<Subscription$tairffWebSocket$tairffWebSocket$trainTimes> Function(
                  Iterable<
                      CopyWith$Subscription$tairffWebSocket$tairffWebSocket$trainTimes<
                          Subscription$tairffWebSocket$tairffWebSocket$trainTimes>>)
              _fn) =>
      call(
          trainTimes: _fn(_instance.trainTimes.map((e) =>
              CopyWith$Subscription$tairffWebSocket$tairffWebSocket$trainTimes(
                e,
                (i) => i,
              ))).toList());
}

class _CopyWithStubImpl$Subscription$tairffWebSocket$tairffWebSocket<TRes>
    implements CopyWith$Subscription$tairffWebSocket$tairffWebSocket<TRes> {
  _CopyWithStubImpl$Subscription$tairffWebSocket$tairffWebSocket(this._res);

  TRes _res;

  call({
    String? $_id,
    List<Subscription$tairffWebSocket$tairffWebSocket$trainTimes>? trainTimes,
    String? $__typename,
  }) =>
      _res;
  trainTimes(_fn) => _res;
}

class Subscription$tairffWebSocket$tairffWebSocket$trainTimes {
  Subscription$tairffWebSocket$tairffWebSocket$trainTimes({
    required this.$_id,
    required this.come,
    required this.date,
    required this.endTime,
    required this.trainerId,
    this.$__typename = 'TrainTimesGraph',
  });

  factory Subscription$tairffWebSocket$tairffWebSocket$trainTimes.fromJson(
      Map<String, dynamic> json) {
    final l$$_id = json['_id'];
    final l$come = json['come'];
    final l$date = json['date'];
    final l$endTime = json['endTime'];
    final l$trainerId = json['trainerId'];
    final l$$__typename = json['__typename'];
    return Subscription$tairffWebSocket$tairffWebSocket$trainTimes(
      $_id: (l$$_id as String),
      come: (l$come as bool),
      date: dateTimeParseG(l$date),
      endTime: dateTimeParseG(l$endTime),
      trainerId: (l$trainerId as String),
      $__typename: (l$$__typename as String),
    );
  }

  final String $_id;

  final bool come;

  final DateTime date;

  final DateTime endTime;

  final String trainerId;

  final String $__typename;

  Map<String, dynamic> toJson() {
    final _resultData = <String, dynamic>{};
    final l$$_id = $_id;
    _resultData['_id'] = l$$_id;
    final l$come = come;
    _resultData['come'] = l$come;
    final l$date = date;
    _resultData['date'] = toMapDateTimeParseG(l$date);
    final l$endTime = endTime;
    _resultData['endTime'] = toMapDateTimeParseG(l$endTime);
    final l$trainerId = trainerId;
    _resultData['trainerId'] = l$trainerId;
    final l$$__typename = $__typename;
    _resultData['__typename'] = l$$__typename;
    return _resultData;
  }

  @override
  int get hashCode {
    final l$$_id = $_id;
    final l$come = come;
    final l$date = date;
    final l$endTime = endTime;
    final l$trainerId = trainerId;
    final l$$__typename = $__typename;
    return Object.hashAll([
      l$$_id,
      l$come,
      l$date,
      l$endTime,
      l$trainerId,
      l$$__typename,
    ]);
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Subscription$tairffWebSocket$tairffWebSocket$trainTimes) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$$_id = $_id;
    final lOther$$_id = other.$_id;
    if (l$$_id != lOther$$_id) {
      return false;
    }
    final l$come = come;
    final lOther$come = other.come;
    if (l$come != lOther$come) {
      return false;
    }
    final l$date = date;
    final lOther$date = other.date;
    if (l$date != lOther$date) {
      return false;
    }
    final l$endTime = endTime;
    final lOther$endTime = other.endTime;
    if (l$endTime != lOther$endTime) {
      return false;
    }
    final l$trainerId = trainerId;
    final lOther$trainerId = other.trainerId;
    if (l$trainerId != lOther$trainerId) {
      return false;
    }
    final l$$__typename = $__typename;
    final lOther$$__typename = other.$__typename;
    if (l$$__typename != lOther$$__typename) {
      return false;
    }
    return true;
  }
}

extension UtilityExtension$Subscription$tairffWebSocket$tairffWebSocket$trainTimes
    on Subscription$tairffWebSocket$tairffWebSocket$trainTimes {
  CopyWith$Subscription$tairffWebSocket$tairffWebSocket$trainTimes<
          Subscription$tairffWebSocket$tairffWebSocket$trainTimes>
      get copyWith =>
          CopyWith$Subscription$tairffWebSocket$tairffWebSocket$trainTimes(
            this,
            (i) => i,
          );
}

abstract class CopyWith$Subscription$tairffWebSocket$tairffWebSocket$trainTimes<
    TRes> {
  factory CopyWith$Subscription$tairffWebSocket$tairffWebSocket$trainTimes(
    Subscription$tairffWebSocket$tairffWebSocket$trainTimes instance,
    TRes Function(Subscription$tairffWebSocket$tairffWebSocket$trainTimes) then,
  ) = _CopyWithImpl$Subscription$tairffWebSocket$tairffWebSocket$trainTimes;

  factory CopyWith$Subscription$tairffWebSocket$tairffWebSocket$trainTimes.stub(
          TRes res) =
      _CopyWithStubImpl$Subscription$tairffWebSocket$tairffWebSocket$trainTimes;

  TRes call({
    String? $_id,
    bool? come,
    DateTime? date,
    DateTime? endTime,
    String? trainerId,
    String? $__typename,
  });
}

class _CopyWithImpl$Subscription$tairffWebSocket$tairffWebSocket$trainTimes<
        TRes>
    implements
        CopyWith$Subscription$tairffWebSocket$tairffWebSocket$trainTimes<TRes> {
  _CopyWithImpl$Subscription$tairffWebSocket$tairffWebSocket$trainTimes(
    this._instance,
    this._then,
  );

  final Subscription$tairffWebSocket$tairffWebSocket$trainTimes _instance;

  final TRes Function(Subscription$tairffWebSocket$tairffWebSocket$trainTimes)
      _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? $_id = _undefined,
    Object? come = _undefined,
    Object? date = _undefined,
    Object? endTime = _undefined,
    Object? trainerId = _undefined,
    Object? $__typename = _undefined,
  }) =>
      _then(Subscription$tairffWebSocket$tairffWebSocket$trainTimes(
        $_id: $_id == _undefined || $_id == null
            ? _instance.$_id
            : ($_id as String),
        come: come == _undefined || come == null
            ? _instance.come
            : (come as bool),
        date: date == _undefined || date == null
            ? _instance.date
            : (date as DateTime),
        endTime: endTime == _undefined || endTime == null
            ? _instance.endTime
            : (endTime as DateTime),
        trainerId: trainerId == _undefined || trainerId == null
            ? _instance.trainerId
            : (trainerId as String),
        $__typename: $__typename == _undefined || $__typename == null
            ? _instance.$__typename
            : ($__typename as String),
      ));
}

class _CopyWithStubImpl$Subscription$tairffWebSocket$tairffWebSocket$trainTimes<
        TRes>
    implements
        CopyWith$Subscription$tairffWebSocket$tairffWebSocket$trainTimes<TRes> {
  _CopyWithStubImpl$Subscription$tairffWebSocket$tairffWebSocket$trainTimes(
      this._res);

  TRes _res;

  call({
    String? $_id,
    bool? come,
    DateTime? date,
    DateTime? endTime,
    String? trainerId,
    String? $__typename,
  }) =>
      _res;
}
