import 'package:graphql/client.dart';
import 'package:tube/logic/blocs/admin/booking_admin/documents/create_booking.model.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/main.dart';

Future<List<Booking>> getTariffWithTrainer({
  // required GraphQLClient client,
  required String clubId,
}) async {
  final queryResult = await graphqlClient.query$getTariffWithTrainer(
    Options$Query$getTariffWithTrainer(
      variables: Variables$Query$getTariffWithTrainer(
        clubId: clubId,
      ),
      fetchPolicy: FetchPolicy.networkOnly,
    ),
  );
  if (queryResult.hasException) throw queryResult.exception!;

  return List<Booking>.from(
    queryResult.parsedData!.getTariffWithTrainer.map(
      (x) => Booking.fromMap(x.toJson()),
    ),
  );
}
