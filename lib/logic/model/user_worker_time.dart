import 'dart:convert';

import 'package:equatable/equatable.dart';

import 'package:tube/logic/model/work_time_graph.dart';

class UserWorkerTime extends Equatable {
  final String? id;
  final String? clubId;
  final String? userId;
  final List<WorkTimeGraph>? workTime;

  const UserWorkerTime({
    this.id,
    this.clubId,
    this.userId,
    this.workTime,
  });

  UserWorkerTime copyWith({
    String? id,
    String? clubId,
    String? userId,
    List<WorkTimeGraph>? workTime,
  }) {
    return UserWorkerTime(
      id: id ?? this.id,
      clubId: clubId ?? this.clubId,
      userId: userId ?? this.userId,
      workTime: workTime ?? this.workTime,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'clubId': clubId,
      'userId': userId,
      'workTime': workTime?.map((x) => x.toMap()).toList(),
    };
  }

  factory UserWorkerTime.fromMap(Map<String, dynamic> map) {
    return UserWorkerTime(
      id: map['_id'],
      clubId: map['clubId'],
      userId: map['userId'],
      workTime: map['workTime'] != null
          ? List<WorkTimeGraph>.from(
              map['workTime']?.map((x) => WorkTimeGraph.fromMap(x)))
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory UserWorkerTime.fromJson(String source) =>
      UserWorkerTime.fromMap(json.decode(source));

  @override
  String toString() {
    return 'UserWorkerTime(id: $id, clubId: $clubId, userId: $userId, workTime: $workTime)';
  }

  @override
  List<Object?> get props => [id, clubId, userId, workTime];
}
