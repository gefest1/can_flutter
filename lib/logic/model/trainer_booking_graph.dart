import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/utils/date_time_parse.dart';

class TrainerBookingGraph extends Equatable {
  final String? id;
  final String? bookingId;
  final String? clubId;
  final DateTime? endTime;
  final DateTime? startTime;
  final num? studentCount;
  final List<String>? studentIds;
  final String? trainerId;
  final User? trainer;

  const TrainerBookingGraph({
    this.id,
    this.bookingId,
    this.clubId,
    this.endTime,
    this.startTime,
    this.studentCount,
    this.studentIds,
    this.trainerId,
    this.trainer,
  });

  TrainerBookingGraph copyWith({
    String? id,
    String? bookingId,
    String? clubId,
    DateTime? endTime,
    DateTime? startTime,
    num? studentCount,
    List<String>? studentIds,
    String? trainerId,
    User? trainer,
  }) {
    return TrainerBookingGraph(
      id: id ?? this.id,
      bookingId: bookingId ?? this.bookingId,
      clubId: clubId ?? this.clubId,
      endTime: endTime ?? this.endTime,
      startTime: startTime ?? this.startTime,
      studentCount: studentCount ?? this.studentCount,
      studentIds: studentIds ?? this.studentIds,
      trainerId: trainerId ?? this.trainerId,
      trainer: trainer ?? this.trainer,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      '_id': id,
      'bookingId': bookingId,
      'clubId': clubId,
      'endTime': endTime?.millisecondsSinceEpoch,
      'startTime': startTime?.millisecondsSinceEpoch,
      'studentCount': studentCount,
      'studentIds': studentIds,
      'trainerId': trainerId,
      'trainer': trainer?.toMap(),
    };
  }

  factory TrainerBookingGraph.fromMap(Map<String, dynamic> map) {
    return TrainerBookingGraph(
      id: map['_id'],
      bookingId: map['bookingId'],
      clubId: map['clubId'],
      endTime: map['endTime'] != null ? dateTimeParse(map['endTime']) : null,
      startTime:
          map['startTime'] != null ? dateTimeParse(map['startTime']) : null,
      studentCount: map['studentCount'],
      studentIds: List<String>.from(map['studentIds']),
      trainerId: map['trainerId'],
      trainer: map['trainer'] == null ? null : User.fromMap(map['trainer']),
    );
  }

  String toJson() => json.encode(toMap());

  factory TrainerBookingGraph.fromJson(String source) =>
      TrainerBookingGraph.fromMap(json.decode(source));

  @override
  String toString() {
    return 'TrainerBookingGraph(id: $id, bookingId: $bookingId, clubId: $clubId, endTime: $endTime, startTime: $startTime, studentCount: $studentCount, studentIds: $studentIds, trainerId: $trainerId)';
  }

  @override
  List<Object?> get props {
    return [
      id,
      bookingId,
      clubId,
      endTime,
      startTime,
      studentCount,
      studentIds,
      trainerId,
    ];
  }
}
