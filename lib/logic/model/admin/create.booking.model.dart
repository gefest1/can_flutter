import 'package:flutter/material.dart';

import 'package:tube/logic/model/booking.model.dart';

class CreateBookingModel extends ChangeNotifier {
  Booking booking;

  Map<String, Map<int, List<DateTime>>> workTime = {};

  CreateBookingModel({
    required this.booking,
  });

  CreateBookingModel copyWith({
    Booking? booking,
  }) {
    return CreateBookingModel(
      booking: booking ?? this.booking,
    );
  }

  Booking setNewInstance(final Booking bookingNew) {
    if (booking != bookingNew) {
      booking = bookingNew;
      notifyListeners();
    }
    return booking;
  }

  void setNewWorkTime(final Map<String, Map<int, List<DateTime>>> workTimeNew) {
    if (workTimeNew != workTime) {
      workTime = workTimeNew;
      notifyListeners();
    }
  }
}
