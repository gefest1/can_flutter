// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';

class TelegramCustomPainter extends CustomPainter {
  final Color color;

  const TelegramCustomPainter({
    this.color = Colors.black,
    Listenable? repaint,
  }) : super(repaint: repaint);
  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width, size.height * 0.5000000);
    path_0.cubicTo(size.width, size.height * 0.7761417, size.width * 0.7761417,
        size.height, size.width * 0.5000000, size.height);
    path_0.cubicTo(size.width * 0.2238575, size.height, 0,
        size.height * 0.7761417, 0, size.height * 0.5000000);
    path_0.cubicTo(0, size.height * 0.2238575, size.width * 0.2238575, 0,
        size.width * 0.5000000, 0);
    path_0.cubicTo(size.width * 0.7761417, 0, size.width,
        size.height * 0.2238575, size.width, size.height * 0.5000000);
    path_0.close();
    path_0.moveTo(size.width * 0.5447958, size.height * 0.3569367);
    path_0.lineTo(size.width * 0.2080925, size.height * 0.4956667);
    path_0.cubicTo(
        size.width * 0.1488437,
        size.height * 0.5187875,
        size.width * 0.1835258,
        size.height * 0.5404625,
        size.width * 0.1835258,
        size.height * 0.5404625);
    path_0.cubicTo(
        size.width * 0.1835258,
        size.height * 0.5404625,
        size.width * 0.2341042,
        size.height * 0.5578042,
        size.width * 0.2774567,
        size.height * 0.5708083);
    path_0.cubicTo(
        size.width * 0.3208092,
        size.height * 0.5838167,
        size.width * 0.3439304,
        size.height * 0.5693625,
        size.width * 0.3439304,
        size.height * 0.5693625);
    path_0.lineTo(size.width * 0.5476875, size.height * 0.4320792);
    path_0.cubicTo(
        size.width * 0.6199417,
        size.height * 0.3829479,
        size.width * 0.6026000,
        size.height * 0.4234125,
        size.width * 0.5852583,
        size.height * 0.4407500);
    path_0.cubicTo(
        size.width * 0.5476875,
        size.height * 0.4783250,
        size.width * 0.4855500,
        size.height * 0.5375708,
        size.width * 0.4335250,
        size.height * 0.5852583);
    path_0.cubicTo(
        size.width * 0.4104046,
        size.height * 0.6054917,
        size.width * 0.4219667,
        size.height * 0.6228333,
        size.width * 0.4320792,
        size.height * 0.6315042);
    path_0.cubicTo(
        size.width * 0.4614875,
        size.height * 0.6563875,
        size.width * 0.5307292,
        size.height * 0.7016292,
        size.width * 0.5622083,
        size.height * 0.7221958);
    path_0.cubicTo(
        size.width * 0.5709500,
        size.height * 0.7279083,
        size.width * 0.5767792,
        size.height * 0.7317167,
        size.width * 0.5780333,
        size.height * 0.7326583);
    path_0.cubicTo(
        size.width * 0.5852583,
        size.height * 0.7384375,
        size.width * 0.6257208,
        size.height * 0.7644500,
        size.width * 0.6502875,
        size.height * 0.7586708);
    path_0.cubicTo(
        size.width * 0.6748542,
        size.height * 0.7528917,
        size.width * 0.6777458,
        size.height * 0.7196542,
        size.width * 0.6777458,
        size.height * 0.7196542);
    path_0.lineTo(size.width * 0.7138708, size.height * 0.4927750);
    path_0.cubicTo(
        size.width * 0.7170833,
        size.height * 0.4715042,
        size.width * 0.7202958,
        size.height * 0.4506792,
        size.width * 0.7232875,
        size.height * 0.4312625);
    path_0.cubicTo(
        size.width * 0.7310750,
        size.height * 0.3807562,
        size.width * 0.7373958,
        size.height * 0.3397571,
        size.width * 0.7384375,
        size.height * 0.3251446);
    path_0.cubicTo(
        size.width * 0.7427750,
        size.height * 0.2760117,
        size.width * 0.6907500,
        size.height * 0.2962429,
        size.width * 0.6907500,
        size.height * 0.2962429);
    path_0.cubicTo(
        size.width * 0.6907500,
        size.height * 0.2962429,
        size.width * 0.5780333,
        size.height * 0.3424858,
        size.width * 0.5447958,
        size.height * 0.3569367);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = color;
    canvas.drawPath(path_0, paint_0_fill);
  }

  @override
  bool shouldRepaint(covariant TelegramCustomPainter oldDelegate) {
    return oldDelegate.color != color;
  }
}
