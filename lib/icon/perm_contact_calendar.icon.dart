import 'package:flutter/material.dart';

class PermContactCalendarCancelCustomPainter extends CustomPainter {
  final Color color;

  const PermContactCalendarCancelCustomPainter({
    this.color = const Color(0xffFF2F2F),
    Listenable? repaint,
  }) : super(repaint: repaint);

  @override
  void paint(Canvas canvas, Size size) {
    Path path_1 = Path();
    path_1.moveTo(0, size.height);
    path_1.lineTo(size.width * 1.083333, size.height * -0.08333333);

    final Paint paintStroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.1388889;
    paintStroke.color = color;
    paintStroke.strokeCap = StrokeCap.round;
    canvas.drawPath(path_1, paintStroke);
  }

  @override
  bool shouldRepaint(
    covariant PermContactCalendarCancelCustomPainter oldDelegate,
  ) {
    return oldDelegate.color != color;
  }
}
