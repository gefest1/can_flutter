// ignore_for_file: non_constant_identifier_names
import 'package:flutter/material.dart';

class WhatsappGlyphCustomPainter extends CustomPainter {
  final Color color;

  const WhatsappGlyphCustomPainter({
    this.color = Colors.black,
    Listenable? repaint,
  }) : super(repaint: repaint);
  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.8542375, size.height * 0.1453221);
    path_0.cubicTo(
        size.width * 0.7602417,
        size.height * 0.05166000,
        size.width * 0.6352333,
        size.height * 0.00005264417,
        size.width * 0.5020500,
        0);
    path_0.cubicTo(
        size.width * 0.2276292,
        0,
        size.width * 0.004284375,
        size.height * 0.2222767,
        size.width * 0.004174542,
        size.height * 0.4954917);
    path_0.cubicTo(
        size.width * 0.004137946,
        size.height * 0.5828250,
        size.width * 0.02706475,
        size.height * 0.6680750,
        size.width * 0.07063125,
        size.height * 0.7432208);
    path_0.lineTo(0, size.height);
    path_0.lineTo(size.width * 0.2639308, size.height * 0.9310958);
    path_0.cubicTo(
        size.width * 0.3366487,
        size.height * 0.9705708,
        size.width * 0.4185250,
        size.height * 0.9913750,
        size.width * 0.5018500,
        size.height * 0.9914083);
    path_0.lineTo(size.width * 0.5020542, size.height * 0.9914083);
    path_0.cubicTo(
        size.width * 0.7764458,
        size.height * 0.9914083,
        size.width * 0.9998125,
        size.height * 0.7691042,
        size.width * 0.9999250,
        size.height * 0.4958875);
    path_0.cubicTo(
        size.width * 0.9999750,
        size.height * 0.3634829,
        size.width * 0.9482375,
        size.height * 0.2389838,
        size.width * 0.8542375,
        size.height * 0.1453221);
    path_0.close();
    path_0.moveTo(size.width * 0.5020542, size.height * 0.9077167);
    path_0.lineTo(size.width * 0.5018875, size.height * 0.9077167);
    path_0.cubicTo(
        size.width * 0.4276333,
        size.height * 0.9076875,
        size.width * 0.3548029,
        size.height * 0.8878333,
        size.width * 0.2912696,
        size.height * 0.8503083);
    path_0.lineTo(size.width * 0.2761575, size.height * 0.8413833);
    path_0.lineTo(size.width * 0.1195396, size.height * 0.8822750);
    path_0.lineTo(size.width * 0.1613446, size.height * 0.7302917);
    path_0.lineTo(size.width * 0.1515042, size.height * 0.7147083);
    path_0.cubicTo(
        size.width * 0.1100813,
        size.height * 0.6491333,
        size.width * 0.08820250,
        size.height * 0.5733417,
        size.width * 0.08823500,
        size.height * 0.4955208);
    path_0.cubicTo(
        size.width * 0.08832583,
        size.height * 0.2684375,
        size.width * 0.2739625,
        size.height * 0.08369042,
        size.width * 0.5022208,
        size.height * 0.08369042);
    path_0.cubicTo(
        size.width * 0.6127458,
        size.height * 0.08373250,
        size.width * 0.7166458,
        size.height * 0.1266292,
        size.width * 0.7947750,
        size.height * 0.2044775);
    path_0.cubicTo(
        size.width * 0.8729042,
        size.height * 0.2823263,
        size.width * 0.9159083,
        size.height * 0.3858054,
        size.width * 0.9158625,
        size.height * 0.4958542);
    path_0.cubicTo(
        size.width * 0.9157708,
        size.height * 0.7229542,
        size.width * 0.7301375,
        size.height * 0.9077167,
        size.width * 0.5020542,
        size.height * 0.9077167);
    path_0.close();
    path_0.moveTo(size.width * 0.7290375, size.height * 0.5992583);
    path_0.cubicTo(
        size.width * 0.7165958,
        size.height * 0.5930583,
        size.width * 0.6554375,
        size.height * 0.5631042,
        size.width * 0.6440333,
        size.height * 0.5589750);
    path_0.cubicTo(
        size.width * 0.6326292,
        size.height * 0.5548417,
        size.width * 0.6243375,
        size.height * 0.5527750,
        size.width * 0.6160458,
        size.height * 0.5651708);
    path_0.cubicTo(
        size.width * 0.6077500,
        size.height * 0.5775667,
        size.width * 0.5839125,
        size.height * 0.6054542,
        size.width * 0.5766542,
        size.height * 0.6137167);
    path_0.cubicTo(
        size.width * 0.5693958,
        size.height * 0.6219833,
        size.width * 0.5621417,
        size.height * 0.6230167,
        size.width * 0.5497000,
        size.height * 0.6168167);
    path_0.cubicTo(
        size.width * 0.5372583,
        size.height * 0.6106208,
        size.width * 0.4971750,
        size.height * 0.5975458,
        size.width * 0.4496583,
        size.height * 0.5553625);
    path_0.cubicTo(
        size.width * 0.4126800,
        size.height * 0.5225375,
        size.width * 0.3877113,
        size.height * 0.4819875,
        size.width * 0.3804550,
        size.height * 0.4695917);
    path_0.cubicTo(
        size.width * 0.3731988,
        size.height * 0.4571958,
        size.width * 0.3796833,
        size.height * 0.4504958,
        size.width * 0.3859108,
        size.height * 0.4443208);
    path_0.cubicTo(
        size.width * 0.3915063,
        size.height * 0.4387750,
        size.width * 0.3983504,
        size.height * 0.4298625,
        size.width * 0.4045708,
        size.height * 0.4226292);
    path_0.cubicTo(
        size.width * 0.4107900,
        size.height * 0.4154008,
        size.width * 0.4128629,
        size.height * 0.4102363,
        size.width * 0.4170083,
        size.height * 0.4019742);
    path_0.cubicTo(
        size.width * 0.4211583,
        size.height * 0.3937104,
        size.width * 0.4190833,
        size.height * 0.3864792,
        size.width * 0.4159733,
        size.height * 0.3802821);
    path_0.cubicTo(
        size.width * 0.4128629,
        size.height * 0.3740846,
        size.width * 0.3879850,
        size.height * 0.3131417,
        size.width * 0.3776171,
        size.height * 0.2883517);
    path_0.cubicTo(
        size.width * 0.3675204,
        size.height * 0.2642083,
        size.width * 0.3572637,
        size.height * 0.2674750,
        size.width * 0.3496279,
        size.height * 0.2670971);
    path_0.cubicTo(
        size.width * 0.3423796,
        size.height * 0.2667379,
        size.width * 0.3340779,
        size.height * 0.2666613,
        size.width * 0.3257858,
        size.height * 0.2666613);
    path_0.cubicTo(
        size.width * 0.3174925,
        size.height * 0.2666613,
        size.width * 0.3040154,
        size.height * 0.2697604,
        size.width * 0.2926117,
        size.height * 0.2821546);
    path_0.cubicTo(
        size.width * 0.2812096,
        size.height * 0.2945504,
        size.width * 0.2490737,
        size.height * 0.3245050,
        size.width * 0.2490737,
        size.height * 0.3854467);
    path_0.cubicTo(
        size.width * 0.2490737,
        size.height * 0.4463875,
        size.width * 0.2936492,
        size.height * 0.5052625,
        size.width * 0.2998696,
        size.height * 0.5135250);
    path_0.cubicTo(
        size.width * 0.3060887,
        size.height * 0.5217917,
        size.width * 0.3875892,
        size.height * 0.6468542,
        size.width * 0.5123792,
        size.height * 0.7004833);
    path_0.cubicTo(
        size.width * 0.5420625,
        size.height * 0.7132417,
        size.width * 0.5652333,
        size.height * 0.7208583,
        size.width * 0.5833000,
        size.height * 0.7265667);
    path_0.cubicTo(
        size.width * 0.6131000,
        size.height * 0.7359875,
        size.width * 0.6402208,
        size.height * 0.7346583,
        size.width * 0.6616542,
        size.height * 0.7314708);
    path_0.cubicTo(
        size.width * 0.6855583,
        size.height * 0.7279167,
        size.width * 0.7352583,
        size.height * 0.7015167,
        size.width * 0.7456250,
        size.height * 0.6725958);
    path_0.cubicTo(
        size.width * 0.7559917,
        size.height * 0.6436750,
        size.width * 0.7559917,
        size.height * 0.6188833,
        size.width * 0.7528792,
        size.height * 0.6137208);
    path_0.cubicTo(
        size.width * 0.7497708,
        size.height * 0.6085542,
        size.width * 0.7414792,
        size.height * 0.6054542,
        size.width * 0.7290375,
        size.height * 0.5992583);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = color;
    canvas.drawPath(path_0, paint_0_fill);
  }

  @override
  bool shouldRepaint(covariant WhatsappGlyphCustomPainter oldDelegate) {
    return oldDelegate.color != color;
  }
}
