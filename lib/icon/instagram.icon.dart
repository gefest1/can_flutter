// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';

//Copy this CustomPainter code to the Bottom of the File
class InstagramGlyphCustomPainter extends CustomPainter {
  final Color color;

  const InstagramGlyphCustomPainter({
    this.color = Colors.black,
    Listenable? repaint,
  }) : super(repaint: repaint);

  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.3333604, size.height * 0.5000000);
    path_0.cubicTo(
        size.width * 0.3333604,
        size.height * 0.4079563,
        size.width * 0.4079563,
        size.height * 0.3333200,
        size.width * 0.5000000,
        size.height * 0.3333200);
    path_0.cubicTo(
        size.width * 0.5920417,
        size.height * 0.3333200,
        size.width * 0.6666792,
        size.height * 0.4079563,
        size.width * 0.6666792,
        size.height * 0.5000000);
    path_0.cubicTo(
        size.width * 0.6666792,
        size.height * 0.5920417,
        size.width * 0.5920417,
        size.height * 0.6666792,
        size.width * 0.5000000,
        size.height * 0.6666792);
    path_0.cubicTo(
        size.width * 0.4079563,
        size.height * 0.6666792,
        size.width * 0.3333604,
        size.height * 0.5920417,
        size.width * 0.3333604,
        size.height * 0.5000000);
    path_0.close();
    path_0.moveTo(size.width * 0.2432554, size.height * 0.5000000);
    path_0.cubicTo(
        size.width * 0.2432554,
        size.height * 0.6418000,
        size.width * 0.3581987,
        size.height * 0.7567458,
        size.width * 0.5000000,
        size.height * 0.7567458);
    path_0.cubicTo(
        size.width * 0.6418000,
        size.height * 0.7567458,
        size.width * 0.7567458,
        size.height * 0.6418000,
        size.width * 0.7567458,
        size.height * 0.5000000);
    path_0.cubicTo(
        size.width * 0.7567458,
        size.height * 0.3581987,
        size.width * 0.6418000,
        size.height * 0.2432554,
        size.width * 0.5000000,
        size.height * 0.2432554);
    path_0.cubicTo(
        size.width * 0.3581987,
        size.height * 0.2432554,
        size.width * 0.2432554,
        size.height * 0.3581987,
        size.width * 0.2432554,
        size.height * 0.5000000);
    path_0.close();
    path_0.moveTo(size.width * 0.7069083, size.height * 0.2330775);
    path_0.cubicTo(
        size.width * 0.7069083,
        size.height * 0.2661954,
        size.width * 0.7337625,
        size.height * 0.2930937,
        size.width * 0.7669208,
        size.height * 0.2930937);
    path_0.cubicTo(
        size.width * 0.8000417,
        size.height * 0.2930937,
        size.width * 0.8269375,
        size.height * 0.2661954,
        size.width * 0.8269375,
        size.height * 0.2330775);
    path_0.cubicTo(
        size.width * 0.8269375,
        size.height * 0.1999596,
        size.width * 0.8000792,
        size.height * 0.1731017,
        size.width * 0.7669208,
        size.height * 0.1731017);
    path_0.cubicTo(
        size.width * 0.7337625,
        size.height * 0.1731017,
        size.width * 0.7069083,
        size.height * 0.1999596,
        size.width * 0.7069083,
        size.height * 0.2330775);
    path_0.close();
    path_0.moveTo(size.width * 0.2979804, size.height * 0.9069875);
    path_0.cubicTo(
        size.width * 0.2492325,
        size.height * 0.9047667,
        size.width * 0.2227383,
        size.height * 0.8966458,
        size.width * 0.2051292,
        size.height * 0.8897833);
    path_0.cubicTo(
        size.width * 0.1817850,
        size.height * 0.8806958,
        size.width * 0.1651454,
        size.height * 0.8698708,
        size.width * 0.1476171,
        size.height * 0.8523833);
    path_0.cubicTo(
        size.width * 0.1301292,
        size.height * 0.8348958,
        size.width * 0.1192650,
        size.height * 0.8182542,
        size.width * 0.1102179,
        size.height * 0.7949125);
    path_0.cubicTo(
        size.width * 0.1033521,
        size.height * 0.7773042,
        size.width * 0.09523417,
        size.height * 0.7508083,
        size.width * 0.09301292,
        size.height * 0.7020583);
    path_0.cubicTo(
        size.width * 0.09058958,
        size.height * 0.6493542,
        size.width * 0.09010500,
        size.height * 0.6335208,
        size.width * 0.09010500,
        size.height * 0.5000000);
    path_0.cubicTo(
        size.width * 0.09010500,
        size.height * 0.3664783,
        size.width * 0.09063000,
        size.height * 0.3506867,
        size.width * 0.09301292,
        size.height * 0.2979404);
    path_0.cubicTo(
        size.width * 0.09523417,
        size.height * 0.2491921,
        size.width * 0.1033925,
        size.height * 0.2227383,
        size.width * 0.1102179,
        size.height * 0.2050888);
    path_0.cubicTo(
        size.width * 0.1193054,
        size.height * 0.1817446,
        size.width * 0.1301292,
        size.height * 0.1651050,
        size.width * 0.1476171,
        size.height * 0.1475767);
    path_0.cubicTo(
        size.width * 0.1651050,
        size.height * 0.1300888,
        size.width * 0.1817446,
        size.height * 0.1192246,
        size.width * 0.2051292,
        size.height * 0.1101775);
    path_0.cubicTo(
        size.width * 0.2227383,
        size.height * 0.1033117,
        size.width * 0.2492325,
        size.height * 0.09519375,
        size.width * 0.2979804,
        size.height * 0.09297250);
    path_0.cubicTo(
        size.width * 0.3506867,
        size.height * 0.09054917,
        size.width * 0.3665188,
        size.height * 0.09006458,
        size.width * 0.5000000,
        size.height * 0.09006458);
    path_0.cubicTo(
        size.width * 0.6335208,
        size.height * 0.09006458,
        size.width * 0.6493125,
        size.height * 0.09058958,
        size.width * 0.7020583,
        size.height * 0.09297250);
    path_0.cubicTo(
        size.width * 0.7508083,
        size.height * 0.09519375,
        size.width * 0.7772625,
        size.height * 0.1033521,
        size.width * 0.7949125,
        size.height * 0.1101775);
    path_0.cubicTo(
        size.width * 0.8182542,
        size.height * 0.1192246,
        size.width * 0.8348958,
        size.height * 0.1300888,
        size.width * 0.8524250,
        size.height * 0.1475767);
    path_0.cubicTo(
        size.width * 0.8699125,
        size.height * 0.1650646,
        size.width * 0.8807333,
        size.height * 0.1817446,
        size.width * 0.8898208,
        size.height * 0.2050888);
    path_0.cubicTo(
        size.width * 0.8966875,
        size.height * 0.2226979,
        size.width * 0.9048042,
        size.height * 0.2491921,
        size.width * 0.9070292,
        size.height * 0.2979404);
    path_0.cubicTo(
        size.width * 0.9094500,
        size.height * 0.3506867,
        size.width * 0.9099333,
        size.height * 0.3664783,
        size.width * 0.9099333,
        size.height * 0.5000000);
    path_0.cubicTo(
        size.width * 0.9099333,
        size.height * 0.6334833,
        size.width * 0.9094500,
        size.height * 0.6493125,
        size.width * 0.9070292,
        size.height * 0.7020583);
    path_0.cubicTo(
        size.width * 0.9048042,
        size.height * 0.7508083,
        size.width * 0.8966458,
        size.height * 0.7773042,
        size.width * 0.8898208,
        size.height * 0.7949125);
    path_0.cubicTo(
        size.width * 0.8807333,
        size.height * 0.8182542,
        size.width * 0.8699125,
        size.height * 0.8348958,
        size.width * 0.8524250,
        size.height * 0.8523833);
    path_0.cubicTo(
        size.width * 0.8349375,
        size.height * 0.8698708,
        size.width * 0.8182542,
        size.height * 0.8806958,
        size.width * 0.7949125,
        size.height * 0.8897833);
    path_0.cubicTo(
        size.width * 0.7773042,
        size.height * 0.8966458,
        size.width * 0.7508083,
        size.height * 0.9047667,
        size.width * 0.7020583,
        size.height * 0.9069875);
    path_0.cubicTo(
        size.width * 0.6493542,
        size.height * 0.9094083,
        size.width * 0.6335208,
        size.height * 0.9098958,
        size.width * 0.5000000,
        size.height * 0.9098958);
    path_0.cubicTo(
        size.width * 0.3665188,
        size.height * 0.9098958,
        size.width * 0.3506867,
        size.height * 0.9094083,
        size.width * 0.2979804,
        size.height * 0.9069875);
    path_0.close();
    path_0.moveTo(size.width * 0.2938613, size.height * 0.003029079);
    path_0.cubicTo(
        size.width * 0.2406300,
        size.height * 0.005452333,
        size.width * 0.2042812,
        size.height * 0.01389337,
        size.width * 0.1724958,
        size.height * 0.02625200);
    path_0.cubicTo(
        size.width * 0.1396204,
        size.height * 0.03901454,
        size.width * 0.1117529,
        size.height * 0.05613875,
        size.width * 0.08392583,
        size.height * 0.08392583);
    path_0.cubicTo(
        size.width * 0.05613875,
        size.height * 0.1117125,
        size.width * 0.03901454,
        size.height * 0.1395800,
        size.width * 0.02625200,
        size.height * 0.1724958);
    path_0.cubicTo(
        size.width * 0.01389337,
        size.height * 0.2042812,
        size.width * 0.005452333,
        size.height * 0.2406300,
        size.width * 0.003029079,
        size.height * 0.2938613);
    path_0.cubicTo(size.width * 0.0005654292, size.height * 0.3471729, 0,
        size.height * 0.3642167, 0, size.height * 0.5000000);
    path_0.cubicTo(
        0,
        size.height * 0.6357833,
        size.width * 0.0005654292,
        size.height * 0.6528292,
        size.width * 0.003029079,
        size.height * 0.7061375);
    path_0.cubicTo(
        size.width * 0.005452333,
        size.height * 0.7593708,
        size.width * 0.01389337,
        size.height * 0.7957208,
        size.width * 0.02625200,
        size.height * 0.8275042);
    path_0.cubicTo(
        size.width * 0.03901454,
        size.height * 0.8603792,
        size.width * 0.05609875,
        size.height * 0.8882875,
        size.width * 0.08392583,
        size.height * 0.9160750);
    path_0.cubicTo(
        size.width * 0.1117125,
        size.height * 0.9438625,
        size.width * 0.1395800,
        size.height * 0.9609458,
        size.width * 0.1724958,
        size.height * 0.9737500);
    path_0.cubicTo(
        size.width * 0.2043217,
        size.height * 0.9861083,
        size.width * 0.2406300,
        size.height * 0.9945458,
        size.width * 0.2938613,
        size.height * 0.9969708);
    path_0.cubicTo(
        size.width * 0.3472133,
        size.height * 0.9993958,
        size.width * 0.3642167,
        size.height,
        size.width * 0.5000000,
        size.height);
    path_0.cubicTo(
        size.width * 0.6358250,
        size.height,
        size.width * 0.6528292,
        size.height * 0.9994333,
        size.width * 0.7061375,
        size.height * 0.9969708);
    path_0.cubicTo(
        size.width * 0.7593708,
        size.height * 0.9945458,
        size.width * 0.7957208,
        size.height * 0.9861083,
        size.width * 0.8275042,
        size.height * 0.9737500);
    path_0.cubicTo(
        size.width * 0.8603792,
        size.height * 0.9609458,
        size.width * 0.8882458,
        size.height * 0.9438625,
        size.width * 0.9160750,
        size.height * 0.9160750);
    path_0.cubicTo(
        size.width * 0.9438625,
        size.height * 0.8882875,
        size.width * 0.9609458,
        size.height * 0.8603792,
        size.width * 0.9737500,
        size.height * 0.8275042);
    path_0.cubicTo(
        size.width * 0.9861083,
        size.height * 0.7957208,
        size.width * 0.9945875,
        size.height * 0.7593708,
        size.width * 0.9969708,
        size.height * 0.7061375);
    path_0.cubicTo(
        size.width * 0.9993958,
        size.height * 0.6527875,
        size.width * 0.9999583,
        size.height * 0.6357833,
        size.width * 0.9999583,
        size.height * 0.5000000);
    path_0.cubicTo(
        size.width * 0.9999583,
        size.height * 0.3642167,
        size.width * 0.9993958,
        size.height * 0.3471729,
        size.width * 0.9969708,
        size.height * 0.2938613);
    path_0.cubicTo(
        size.width * 0.9945458,
        size.height * 0.2406300,
        size.width * 0.9861083,
        size.height * 0.2042812,
        size.width * 0.9737500,
        size.height * 0.1724958);
    path_0.cubicTo(
        size.width * 0.9609458,
        size.height * 0.1396204,
        size.width * 0.9438625,
        size.height * 0.1117529,
        size.width * 0.9160750,
        size.height * 0.08392583);
    path_0.cubicTo(
        size.width * 0.8882875,
        size.height * 0.05613875,
        size.width * 0.8603792,
        size.height * 0.03901454,
        size.width * 0.8275458,
        size.height * 0.02625200);
    path_0.cubicTo(
        size.width * 0.7957208,
        size.height * 0.01389337,
        size.width * 0.7593708,
        size.height * 0.005411958,
        size.width * 0.7061792,
        size.height * 0.003029079);
    path_0.cubicTo(size.width * 0.6528667, size.height * 0.0006058167,
        size.width * 0.6358250, 0, size.width * 0.5000417, 0);
    path_0.cubicTo(
        size.width * 0.3642167,
        0,
        size.width * 0.3472133,
        size.height * 0.0005654292,
        size.width * 0.2938613,
        size.height * 0.003029079);
    path_0.close();

    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = color;
    canvas.drawPath(path_0, paint_0_fill);
  }

  @override
  bool shouldRepaint(covariant InstagramGlyphCustomPainter oldDelegate) {
    return oldDelegate.color != color;
  }
}
