// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';

class _RPSCustomPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.7101960, size.height * 0.2749400);
    path_0.lineTo(size.width * 0.5038000, size.height * 0.07515308);
    path_0.lineTo(size.width * 0.2974020, size.height * 0.2749400);

    Paint paint_0_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.08000000;
    paint_0_stroke.color = Colors.white.withOpacity(1.0);
    paint_0_stroke.strokeCap = StrokeCap.round;
    paint_0_stroke.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_0, paint_0_stroke);

    Paint paint_1_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.08;
    paint_1_stroke.color = Colors.white.withOpacity(1.0);
    paint_1_stroke.strokeCap = StrokeCap.round;
    final path = Path()
      ..moveTo(size.width * 0.5, size.height * 0.1)
      ..lineTo(size.width * 0.5, size.height * 0.5);

    canvas.drawPath(
      path,
      paint_1_stroke,
    );

    Path path_2 = Path();
    path_2.moveTo(size.width * 0.1546092, size.height * 0.5228385);
    path_2.lineTo(size.width * 0.1546092, size.height * 0.7151462);
    path_2.cubicTo(
        size.width * 0.1546092,
        size.height * 0.8213538,
        size.width * 0.2441524,
        size.height * 0.9074538,
        size.width * 0.3546092,
        size.height * 0.9074538);
    path_2.lineTo(size.width * 0.6346080, size.height * 0.9074538);
    path_2.cubicTo(
        size.width * 0.7450680,
        size.height * 0.9074538,
        size.width * 0.8346080,
        size.height * 0.8213538,
        size.width * 0.8346080,
        size.height * 0.7151462);
    path_2.lineTo(size.width * 0.8346080, size.height * 0.5228385);

    Paint paint_2_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.08000000;
    paint_2_stroke.color = Colors.white.withOpacity(1.0);
    paint_2_stroke.strokeCap = StrokeCap.round;
    paint_2_stroke.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_2, paint_2_stroke);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}

class ExportIcon extends StatelessWidget {
  const ExportIcon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: _RPSCustomPainter(),
      size: const Size(24, 24),
    );
  }
}
