import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:tube/flexible/lib/src/flexible_bottom_sheet_route.dart';
import 'package:tube/logic/blocs/club/club.model.dart';
import 'package:tube/logic/model/status_enum.dart';
import 'package:tube/main.dart';
import 'package:tube/ui/add_compnay/pages/finish_page.dart';
import 'package:tube/ui/common/molecules/custom_button.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';
import 'package:tube/ui/common/molecules/fast_app_bar.dart';
import 'package:tube/utils/loader.dart';
import 'package:tube/logic/blocs/club/club_bloc.dart';
import 'package:tube/utils/uri_parse.dart';
import 'package:tube/logic/blocs/participance/participance_bloc.dart';

class OccupationCompanyPage extends StatefulWidget {
  final ScrollController scrollController;
  final XFile file;
  final String fullName;
  final String name;

  const OccupationCompanyPage({
    required this.scrollController,
    required this.name,
    required this.file,
    required this.fullName,
    Key? key,
  }) : super(key: key);

  @override
  State<OccupationCompanyPage> createState() => _OccupationCompanyPageState();
}

class _OccupationCompanyPageState extends State<OccupationCompanyPage> {
  late final _media = MediaQuery.of(navigatorKeyGlobal.currentContext!);
  late final TextEditingController typeController = TextEditingController()
    ..addListener(() {
      streamController.add(null);
    });
  late final TextEditingController descriptionController =
      TextEditingController()
        ..addListener(() {
          streamController.add(null);
        });
  final StreamController streamController = StreamController();

  void _finish() async {
    loaderActivate(context);
    try {
      final Completer<Club> completer = Completer<Club>();
      BlocProvider.of<ClubBloc>(context).add(
        CreateClubEvent(
          description: descriptionController.text.trim(),
          name: widget.fullName.trim(),
          type: typeController.text.trim(),
          file: widget.file,
          completer: completer,
        ),
      );
      final club = await completer.future;
      if (!mounted) return;
      Navigator.popUntil(context, (route) => route.isFirst);

      context.read<ParticipanceBloc>().add(const GetDataParticipanceEvent());

      showFlexibleBottomSheet(
        minHeight: 0,
        initHeight: 0.95,
        maxHeight: 0.95,
        context: context,
        builder: (ctx, scroll, _) {
          return Material(
            color: Colors.transparent,
            child: FinishPage(
              enumType: TypeInviteEnum.InviteTrainer,
              statusEnum: StatusEnum.trainer,
              controller: scroll,
              clubId: club.id!,
              title: 'Добавление сотрудников',
            ),
          );
        },
        anchors: [0.95],
      );
    } catch (e) {
      log(e.toString());
    }

    killLoaderOverlay();
  }

  @override
  void dispose() {
    streamController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final focus = FocusScope.of(context);

        if (focus.hasFocus || focus.hasPrimaryFocus) {
          focus.unfocus();
        }
      },
      child: Container(
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(10),
          ),
        ),
        alignment: Alignment.topCenter,
        child: CustomScrollView(
          controller: widget.scrollController,
          slivers: [
            SliverToBoxAdapter(
              child: FastAppBar(
                title: 'Род деятельности',
                suffIcon: Icons.close,
                suffOnTap: () => Navigator.pop(context),
              ),
            ),
            SliverPadding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              sliver: SliverToBoxAdapter(
                child: Column(
                  children: [
                    const SizedBox(height: 30),
                    CustomTextField(
                      controller: typeController,
                      hintText: 'Например: «Курсы английского»',
                      title: 'Род деятельности компании',
                    ),
                    const SizedBox(height: 30),
                    CustomTextField(
                      controller: descriptionController,
                      hintText: 'Описание компании',
                      title:
                          'Это поможет вашим клиентам, понять чем вы занимаетесь более подробно',
                    ),
                  ],
                ),
              ),
            ),
            SliverFillRemaining(
              hasScrollBody: false,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: EdgeInsets.only(
                    bottom: _media.padding.bottom + 20,
                    top: 20,
                    left: 20,
                    right: 20,
                  ),
                  child: StreamBuilder(
                    stream: streamController.stream,
                    builder: (_, __) {
                      return AnimatedOpacity(
                        opacity: typeController.text.isNotEmpty &&
                                descriptionController.text.isNotEmpty
                            ? 1
                            : 0,
                        duration: const Duration(milliseconds: 300),
                        child: CustomButton(
                          title: 'Продолжить',
                          onTap: _finish,
                        ),
                      );
                    },
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
