import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:tube/main.dart';
import 'package:tube/ui/common/atoms/load_photo_circle.dart';
import 'package:tube/ui/common/molecules/custom_button.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';
import 'package:tube/ui/common/molecules/fast_app_bar.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class NameCompanyPage extends StatefulWidget {
  const NameCompanyPage({Key? key}) : super(key: key);

  @override
  State<NameCompanyPage> createState() => _NameCompanyPageState();
}

class _NameCompanyPageState extends State<NameCompanyPage> {
  late final _media = MediaQuery.of(navigatorKeyGlobal.currentContext!);
  XFile? _file;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final focus = FocusScope.of(context);

        if (focus.hasFocus || focus.hasPrimaryFocus) {
          focus.unfocus();
        }
      },
      child: Container(
        height: (_media.size.height - 56 / 2 - _media.padding.top) * 0.98,
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(10),
          ),
        ),
        alignment: Alignment.topCenter,
        child: Column(
          children: [
            FastAppBar(
              title: 'Создание компании',
              suffIcon: Icons.close,
              suffOnTap: () => Navigator.pop(context),
            ),
            Expanded(
              child: CustomScrollView(
                clipBehavior: Clip.none,
                slivers: [
                  SliverPadding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    sliver: SliverToBoxAdapter(
                      child: Column(
                        children: [
                          const SizedBox(height: 40),
                          Center(
                            child: LoadPhotoCircle(
                              file: _file,
                              onTap: () async {
                                _file = await ImagePicker()
                                    .pickImage(source: ImageSource.gallery);
                                setState(() {});
                              },
                            ),
                          ),
                          const SizedBox(height: 40),
                          const CustomTextField(
                            hintText: 'Название компании',
                            title: 'Как будет называться ваша компания?',
                          )
                        ],
                      ),
                    ),
                  ),
                  SliverFillRemaining(
                    fillOverscroll: true,
                    hasScrollBody: false,
                    child: Padding(
                      padding: EdgeInsets.only(
                        bottom: _media.padding.bottom + 20,
                        top: 20,
                        left: 20,
                        right: 20,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          CustomButton(
                            title: 'Продолжить',
                            onTap: () async {
                              // showModalBottomSheet(
                              //   context: context,
                              //   backgroundColor: Colors.transparent,
                              //   isScrollControlled: true,
                              //   isDismissible: true,
                              //   builder: (_) {
                              //     return const OccupationCompanyPage();
                              //   },
                              // );
                            },
                          ),
                          const SizedBox(height: 10),
                          RichText(
                            text: const TextSpan(
                              style: TextStyle(
                                fontSize: P2TextStyle.fontSize,
                                height: P2TextStyle.height,
                                fontWeight: P2TextStyle.fontWeight,
                                color: ColorData.colortTextSecondary,
                              ),
                              children: [
                                TextSpan(
                                  text:
                                      "Продолжая создание компании, вы принимаете наши ",
                                ),
                                TextSpan(
                                  text: "Условия использования для клиентов ",
                                  style: TextStyle(
                                    color: ColorData.colorMainLink,
                                  ),
                                ),
                                TextSpan(text: "и "),
                                TextSpan(
                                  text: "Политику конфиденциальности",
                                  style: TextStyle(
                                    color: ColorData.colorMainLink,
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
