import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text.toUpperCase(),
      selection: newValue.selection,
    );
  }
}

class FullNameController extends StatefulWidget {
  final TextEditingController controller;

  const FullNameController({
    required this.controller,
    Key? key,
  }) : super(key: key);

  @override
  State<FullNameController> createState() => _FullNameControllerState();
}

class _FullNameControllerState extends State<FullNameController> {
  final upperCase = UpperCaseTextFormatter();
  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: widget.controller,
      inputFormatters: [upperCase],
      decoration: const InputDecoration(
        hintText: 'Ivan Ivanov',
        hintStyle: TextStyle(
          color: Color(0xffAFAFAF),
          fontSize: 18,
          height: 21 / 18,
          fontWeight: FontWeight.w400,
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.white,
            width: 1,
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.white,
            width: 2,
          ),
        ),
      ),
    );
  }
}
