import 'package:flutter/material.dart';
import 'package:tube/ui/common/molecules/main_bottom.dart';
import 'package:tube/ui/main_page/notfication_page.dart';
import 'package:tube/ui/main_page/profile_page.dart';

class MainPage extends StatefulWidget {
  final Widget child;

  const MainPage({
    required this.child,
    Key? key,
  }) : super(key: key);

  @override
  MainPageState createState() => MainPageState();
}

class MainPageState extends State<MainPage>
    with SingleTickerProviderStateMixin {
  late final TabController _tabController = TabController(
    vsync: this,
    length: 3,
  )..addListener(() {
      if (_tabController.indexIsChanging) {
        setState(() {
          _activeIndex = _tabController.index;
        });
      }
    });
  int _activeIndex = 0;

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: TabBarView(
            physics: const NeverScrollableScrollPhysics(),
            controller: _tabController,
            children: [
              widget.child,
              const NotificationPage(),
              const ProfilePage()
            ],
          ),
        ),
        MainBottom(
          tabController: _tabController,
          tabIndex: _activeIndex,
        ),
      ],
    );
  }
}
