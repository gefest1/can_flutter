import 'package:flutter/material.dart';

import 'package:tube/logic/model/workt_time_obj.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/ui/main_page/components/molecules/work_day.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';
import 'package:tube/utils/week_day.dart';

class NotActiveWorkDay extends StatelessWidget {
  final Map<int, List<WorkActiveEntity>> getActiveWeekDay;
  final void Function(BuildContext context, int index)? chooseTime;
  final Map<int, List<WorkTimeObjTrainer>> workTimeObjTrainer;

  const NotActiveWorkDay({
    required this.getActiveWeekDay,
    this.chooseTime,
    this.workTimeObjTrainer = const {},
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<int> availableDays = [];
    for (int i = 0; i < 7; ++i) {
      if (getActiveWeekDay[i]?.isEmpty ?? true) availableDays.add(i);
    }
    if (availableDays.isEmpty) return const SizedBox();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const H5Text(
          'Занятия в другое время',
          color: ColorData.colorElementsSecondary,
        ),
        const SizedBox(height: 10),
        Column(
          children: List.generate(
            availableDays.length * 2 - 1,
            (ind) {
              final index = ind ~/ 2;
              if ((workTimeObjTrainer[availableDays[index]] ?? []).isEmpty) {
                return const SizedBox();
              }
              if (ind.isOdd) return const SizedBox(height: 10);

              return SizeTapAnimation(
                onTap: chooseTime == null
                    ? null
                    : () => chooseTime?.call(context, availableDays[index]),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5),
                    boxShadow: blocksMain,
                  ),
                  padding: const EdgeInsets.all(15),
                  child: Row(
                    children: [
                      Expanded(
                        child: H4Text(
                          weeks[availableDays[index]],
                          color: ColorData.colorTextMain,
                        ),
                      ),
                      const P2Text(
                        'Выбрать время',
                        color: ColorData.colorMainLink,
                      )
                    ],
                  ),
                ),
              );
            },
          ),
        )
      ],
    );
  }
}
