import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/circle_choose.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class LittleDayCard extends StatelessWidget {
  final String? title;
  final String? label;
  final String? subLabel;
  final bool value;
  final VoidCallback? onTap;
  final bool active;

  const LittleDayCard({
    this.title,
    this.label,
    this.value = false,
    this.onTap,
    this.subLabel,
    this.active = true,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: active ? onTap : null,
      child: Container(
        padding: const EdgeInsets.all(15),
        decoration: BoxDecoration(
          border: Border.all(
            width: 0.5,
            color: ColorData.colortTextSecondary,
          ),
          borderRadius: const BorderRadius.all(
            Radius.circular(6),
          ),
        ),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  if (title != null)
                    Text(
                      title!,
                      style: const TextStyle(
                        fontSize: H3TextStyle.fontSize,
                        height: H3TextStyle.height,
                        fontWeight: H3TextStyle.fontWeight,
                        color: ColorData.colorTextMain,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  if (title != null && label != null)
                    const SizedBox(height: 10),
                  if (subLabel != null)
                    Padding(
                      padding: const EdgeInsets.only(bottom: 2),
                      child: P1Text(
                        subLabel!,
                        color: ColorData.colorTextMain,
                        textAlign: TextAlign.left,
                      ),
                    ),
                  if (label != null)
                    Text(
                      label!,
                      style: TextStyle(
                        fontSize: P1TextStyle.fontSize,
                        height: P1TextStyle.height,
                        fontWeight: P1TextStyle.fontWeight,
                        color: active
                            ? ColorData.colorTextMain
                            : ColorData.colorMainNo,
                      ),
                      textAlign: TextAlign.left,
                    ),
                ],
              ),
            ),
            const SizedBox(width: 10),
            if (active)
              CircleChoose(
                littleSize: 24,
                bigSize: 32,
                iconSize: 14,
                active: value,
                color: Colors.white,
              ),
          ],
        ),
      ),
    );
  }
}
