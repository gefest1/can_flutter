import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/bottom_data_card.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';
import 'package:tube/utils/get_adaptive_span.dart';

class TariffCard extends StatelessWidget {
  final String? title;
  final String? priceCost;
  final String? description;
  final String? applyUntil;
  final String? countLesson;
  final String? minDuration;
  final VoidCallback? onTap;
  final List<String> tags;

  const TariffCard({
    required this.title,
    required this.priceCost,
    this.description,
    this.applyUntil,
    this.countLesson,
    this.minDuration,
    this.onTap,
    this.tags = const [],
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      child: Container(
        decoration: const BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              offset: Offset(2, 2),
              blurRadius: 15,
              color: Color(0x14000000),
            ),
          ],
        ),
        padding: const EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                if (title != null)
                  Expanded(
                    child: H4Text(
                      title!,
                      color: ColorData.colorTextMain,
                    ),
                  ),
                const SizedBox(width: 10),
                if (priceCost != null)
                  RichText(
                    text: getAdaptiveTextSpan(
                      priceCost!,
                      const TextStyle(
                        fontSize: H4TextStyle.fontSize,
                        height: H4TextStyle.height,
                        fontWeight: H4TextStyle.fontWeight,
                        color: ColorData.colorTextMain,
                      ),
                      context,
                    ),
                  ),

                // H4Text(
                //   priceCost!,
                //   color: ColorData.colorTextMain,
                // ),
              ],
            ),
            const SizedBox(height: 10),
            if (description != null && description!.isNotEmpty) ...[
              P1Text(
                description!,
                color: ColorData.colorTextMain,
              ),
              const SizedBox(height: 10),
            ],
            if (tags.isNotEmpty)
              Padding(
                padding: const EdgeInsets.only(top: 10, bottom: 10),
                child: Wrap(
                  spacing: 5,
                  runSpacing: 5,
                  children: List.generate(
                      tags.length,
                      (index) => Container(
                            height: 40,
                            decoration: BoxDecoration(
                              color: const Color(0x1a969696),
                              borderRadius: BorderRadius.circular(5),
                            ),
                            padding: const EdgeInsets.symmetric(
                              horizontal: 10,
                              vertical: 10.5,
                            ),
                            child: P1Text(
                              tags[index],
                              color: ColorData.colorButtonMain,
                            ),
                          )),
                ),
              ),
            BottomDataCard(
              applyUntil: applyUntil,
              countLesson: countLesson,
              minDuration: minDuration,
            ),
          ],
        ),
      ),
    );
  }
}
