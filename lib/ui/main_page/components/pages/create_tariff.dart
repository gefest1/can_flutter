import 'dart:async';

import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/logic/blocs/gifts/provider/gift_provider.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/model/booking.model.dart';
import 'package:tube/logic/provider/create_tariff/create_tariff.dart';
import 'package:tube/ui/admin/page/tariff/molecules/trainer_detail_sheet.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/main_page/components/molecules/trainer_card_tariff.dart';
import 'package:tube/ui/main_page/components/pages/choose_start_day_page.dart';
import 'package:tube/ui/main_page/components/pages/set_role_page.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';
import 'dart:math' as math;

import 'package:tube/utils/date_time_parse.dart';

class CreateTariff extends StatefulWidget {
  const CreateTariff({
    Key? key,
  }) : super(key: key);

  @override
  State<CreateTariff> createState() => _CreateTariffState();
}

class _CreateTariffState extends State<CreateTariff> {
  final Map<String, User> trainersMap = {};
  final BehaviorSubject<Map<String, bool>> workTimes =
      BehaviorSubject<Map<String, bool>>.seeded({});
  StreamSubscription? streamSubscription;

  @override
  void initState() {
    super.initState();
    streamSubscription = workTimes.listen((_) {
      setState(() {});
    });
    _initS();
  }

  Booking get booking =>
      context.read<CreateTariffProvider?>()?.booking ??
      context.read<GiftTariffProvider?>()!.booking;

  void _initS() {
    Iterable<MapEntry<String, User>>? entries =
        booking.trainers?.map<MapEntry<String, User>>(
      (e) => MapEntry(e.id!, e),
    );
    trainersMap.addEntries(entries ?? []);
  }

  @override
  void dispose() {
    streamSubscription?.cancel();
    workTimes.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          'Создание расписания',
          style: TextStyle(
            fontSize: H4TextStyle.fontSize,
            color: Colors.white,
            fontWeight: H4TextStyle.fontWeight,
            height: H4TextStyle.height,
          ),
        ),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: Stack(
        children: [
          CustomScrollView(
            slivers: [
              SliverPadding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                sliver: SliverToBoxAdapter(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 20),
                      if (booking.name != null)
                        H2Text(
                          booking.name!,
                          color: ColorData.colorTextMain,
                          textAlign: TextAlign.left,
                        ),
                      const SizedBox(height: 5),
                      const P0Text(
                        'Выберите удобное для вас время, вы можете всегда поменять его в дальнейшем.',
                        color: ColorData.colorTextMain,
                        textAlign: TextAlign.left,
                      ),
                      const SizedBox(height: 40),
                    ],
                  ),
                ),
              ),
              SliverList(
                delegate: SliverChildBuilderDelegate(
                  (ctx, ind) {
                    if (ind.isOdd) return const SizedBox(height: 36);
                    final index = ind ~/ 2;

                    Map<int, List<WorkTimeObj>> dayMap = {};

                    final element = booking.workTimes!.entries.elementAt(index);

                    for (final workTimeObj in element.value) {
                      final DateTime date = workTimeObj.date!;
                      (dayMap[(date.weekday + 6) % 7] ??= []).add(workTimeObj);
                    }

                    return Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: TrainerCardTariff(
                        trainer: trainersMap[element.key]!,
                        trainerBookings: dayMap.entries.toList(),
                        workTimes: workTimes,
                      ),
                    );
                  },
                  childCount: math.max(booking.workTimes!.length * 2 - 1, 0),
                ),
              ),
              const SliverToBoxAdapter(
                child: SizedBox(height: 160),
              )
            ],
          ),
          if (!const DeepCollectionEquality(DefaultEqualityTime())
              .equals({}, workTimes.value))
            Positioned(
              left: 0,
              right: 0,
              bottom: 0,
              child: BottomButton(
                onTap: () async {
                  final Map<String, List<DateTime>> dateTimesMap = {};
                  // ignore: avoid_function_literals_in_foreach_calls
                  workTimes.value.entries.forEach((e) {
                    if (e.value) {
                      final splitted = e.key.split('_');
                      final userId = splitted.first;
                      final DateTime dateTime = dateTimeParse(splitted.last)!;
                      (dateTimesMap[userId] ??= []).add(dateTime);
                    }
                  });

                  final tariffSubject = context.read<CreateTariffProvider?>() ??
                      context.read<GiftTariffProvider?>();
                  if (tariffSubject is CreateTariffProvider) {
                    tariffSubject.update(
                      dateTimesMap: dateTimesMap,
                      mapUsers: trainersMap,
                    );
                    await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) {
                          return const SetRolePage();
                        },
                      ),
                    );
                    tariffSubject.clean(
                      dateTimesMap: true,
                      mapUsers: true,
                    );
                  } else if (tariffSubject is GiftTariffProvider) {
                    tariffSubject.update(
                      dateTimesMap: dateTimesMap,
                      mapUsers: trainersMap,
                    );
                    await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) {
                          return const ChooseStartDayPage();
                        },
                      ),
                    );
                    tariffSubject.clean(
                      dateTimesMap: true,
                      mapUsers: true,
                    );
                  }
                },
              ),
            ),
        ],
      ),
    );
  }
}
