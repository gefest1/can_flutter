// ignore: unused_import
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/logic/provider/create_tariff/create_tariff.dart';
import 'package:tube/ui/common/atoms/animated_switch.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';
import 'package:tube/ui/main_page/components/molecules/buy_ticket_card.dart';
import 'package:tube/ui/main_page/components/organisms/buy_ticket_body.dart';
import 'package:tube/ui/main_page/components/pages/choose_start_day_page.dart';
import 'package:tube/utils/colors.dart';

class SetRolePage extends StatefulWidget {
  const SetRolePage({
    super.key,
  });

  @override
  State<SetRolePage> createState() => _SetRolePageState();
}

class _SetRolePageState extends State<SetRolePage> {
  final BehaviorSubject<TypePart?> typeSubject =
      BehaviorSubject<TypePart?>.seeded(null);
  late final TextEditingController controller = TextEditingController()
    ..addListener(() {
      typeSubject.add(typeSubject.value);
    });

  @override
  void dispose() {
    typeSubject.close();
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final f = FocusScope.of(context);

        if (f.hasFocus || f.hasPrimaryFocus) {
          f.unfocus();
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: true,
          title: const H4Text(
            'Создание расписания',
            color: Colors.white,
          ),
          leading: IconButton(
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
            onPressed: () => Navigator.pop(context),
          ),
        ),
        body: Stack(
          children: [
            ListView(
              padding: const EdgeInsets.only(
                left: 20,
                right: 20,
                top: 20,
                bottom: 200,
              ),
              children: [
                const H2Text(
                  'Чей это будет абонемент?',
                  color: ColorData.colorTextMain,
                ),
                const SizedBox(height: 30),
                StreamBuilder(
                  stream: typeSubject,
                  builder: (ctx, _) {
                    return Column(
                      children: [
                        BuyTicketCard(
                          title: 'Мой',
                          active: typeSubject.value == TypePart.my,
                          onTap: () {
                            typeSubject.value = TypePart.my;
                          },
                        ),
                        const SizedBox(height: 20),
                        // BuyTicketCard(
                        //   title: 'Кого-то другого',
                        //   active: typeSubject.value == TypePart.another,
                        //   onTap: () {
                        //     typeSubject.value = TypePart.another;
                        //   },
                        // ),
                        AnimatedSwitch(
                          active: typeSubject.value == TypePart.another,
                          child: Padding(
                            padding: const EdgeInsets.only(top: 40),
                            child: CustomTextField(
                              controller: controller,
                              scrollPadding: const EdgeInsets.only(bottom: 200),
                              icon: const Icon(
                                Icons.alternate_email,
                                color: ColorData.colorElementsActive,
                              ),
                              hintText: 'mail@mail.kz',
                              title:
                                  'Электронная почта владельца абонемента. Если её нет, то для пользования абонементом нужно будет ваше присутствие ',
                            ),
                          ),
                        )
                      ],
                    );
                  },
                ),
              ],
            ),
            Positioned(
              left: 0,
              right: 0,
              bottom: 0,
              child: StreamBuilder(
                stream: typeSubject,
                builder: (ctx, _) {
                  return AnimatedSwitch(
                    active: () {
                      return (typeSubject.value == TypePart.another &&
                              controller.text.isNotEmpty &&
                              RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                  .hasMatch(controller.text)) ||
                          typeSubject.value == TypePart.my;
                    }(),
                    child: BottomButton(
                      title: 'Продолжить',
                      onTap: () async {
                        final tariffSubject = Provider.of<CreateTariffProvider>(
                            context,
                            listen: false);
                        tariffSubject.update(
                          owner: typeSubject.value == TypePart.another
                              ? controller.text.replaceAll(" ", "")
                              : null,
                        );

                        await Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (_) => const ChooseStartDayPage(),
                          ),
                        );
                        tariffSubject.clean(owner: true);
                        // Navigator.push(
                        //   context,
                        //   MaterialPageRoute(
                        //     builder: (_) => PayTarifPage(
                        //       booking: widget.booking,
                        //       dateTimesMap: widget.dateTimesMap,
                        //       mapUsers: widget.mapUsers,
                        //       owner: typeSubject.value == TypePart.another ? controller.text : null,
                        //     ),
                        //   ),
                        // );
                      },
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
