import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';

class DayCard extends StatelessWidget {
  final String? title;
  final String? label;
  final VoidCallback? onTap;

  const DayCard({
    this.label,
    this.onTap,
    this.title,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.all(15),
        decoration: const BoxDecoration(
          boxShadow: [
            BoxShadow(
              offset: Offset(2, 2),
              blurRadius: 15,
              color: Color(0x14000000),
            )
          ],
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(6)),
        ),
        child: Row(
          children: [
            Expanded(
              child: title == null
                  ? const SizedBox()
                  : H4Text(
                      title!,
                      color: ColorData.colorTextMain,
                    ),
            ),
            const SizedBox(width: 10),
            if (title != null)
              P2Text(
                label!,
                color: ColorData.colorMainLink,
              ),
          ],
        ),
      ),
    );
  }
}
