import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/gifts/gift_bloc.dart';
import 'package:tube/logic/blocs/gifts/model/gift.model.dart';
import 'package:tube/logic/blocs/gifts/provider/gift_provider.dart';

import 'package:tube/logic/general_providers.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/abo_card.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/ui/main_page/components/pages/create_tariff.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/local_page_scroll_physics.dart';
import 'package:tube/utils/nested_logic_route.dart';

class NewClientTariffList extends StatefulWidget {
  const NewClientTariffList({super.key});

  @override
  State<NewClientTariffList> createState() => _NewClientTariffListState();
}

class _NewClientTariffListState extends State<NewClientTariffList> {
  UserBlocMap get userBlocMap =>
      Provider.of<UserBlocMap>(context, listen: false);
  late GiftBloc giftBloc = userBlocMap.getGiftBloc(
    MainDrawerState.clubId!,
  );

  @override
  void initState() {
    giftBloc.add(const GetGiftEvent());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GiftBloc, GiftState>(
      bloc: giftBloc,
      builder: (context, state) {
        final List<GiftModel> gifts = [];
        if (state is DataGiftState) {
          gifts.addAll(state.gifts);
        }
        if (gifts.isEmpty) return const SizedBox();
        return Padding(
          padding: const EdgeInsets.only(bottom: 30),
          child: LayoutBuilder(
            builder: (context, constraints) {
              final widthElement = constraints.maxWidth - 40;
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: H5Text(
                      'Новое',
                      color: ColorData.colorMainNo,
                    ),
                  ),
                  const SizedBox(height: 10),
                  SizedBox(
                    height: 185,
                    child: ListView.separated(
                      clipBehavior: Clip.none,
                      physics: LocalPageScrollPhysics(
                        separateWidth: 10,
                        width: widthElement,
                      ),
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      scrollDirection: Axis.horizontal,
                      separatorBuilder: (_, index) => const SizedBox(width: 10),
                      itemBuilder: (_, index) {
                        final gift = gifts[index];

                        return AbpoCard(
                          onTap: () {
                            final widget = ChangeNotifierProvider(
                              create: (context) => GiftTariffProvider(
                                booking: gift.booking!,
                                gift: gift,
                              ),
                              child: const NestedLogicRoute(
                                child: CreateTariff(),
                              ),
                            );
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => widget,
                              ),
                            );
                          },
                          activeTrains: gift.count,
                          userTitle:
                              gift.client?.fullName ?? gift.client?.email,
                          title: gift.booking?.name,
                          // onTap: _count == 0 ? null : onTap,
                          dayTitle: '',
                          // photoUrl: ,
                          width: widthElement,
                          isNew: true,
                        );
                        // AbpoCardWrapper(
                        //   width: _widthElement,
                        //   photoUrl: _tarrif.user?.photoUrl?.thumbnail ??
                        //       user?.photoUrl?.thumbnail,
                        //   userTitle: _tarrif.user?.fullName ??
                        //       _tarrif.user?.email ??
                        //       user?.fullName ??
                        //       user?.email,
                        //   title: _tarrif.booking?.name,
                        //   trainTimes: _tarrif.trainTimes,
                        //   isNew: true,
                        //   onTap: () {
                        //     Navigator.push(
                        //       context,
                        //       MaterialPageRoute(
                        //         builder: (_) => SubscriptionPage(
                        //           clubId: MainDrawerState
                        //               .currentParticipance!.clubId!,
                        //           bookingId: _tarrif.bookingId!,
                        //           tarrif: _tarrif,
                        //         ),
                        //       ),
                        //     );
                        //     Provider.of<GetSpecialBooking>(
                        //       context,
                        //       listen: false,
                        //     ).superLazyGetBooking(_tarrif.bookingId!);
                        //   },
                        // );
                      },
                      itemCount: gifts.length,
                    ),
                  ),
                ],
              );
            },
          ),
        );
      },
    );
  }
}
