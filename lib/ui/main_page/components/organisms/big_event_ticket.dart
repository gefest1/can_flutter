import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/shadow.dart';

class BigEventTicket extends StatelessWidget {
  final double? width;
  final String? title;
  final String? label;
  final String? subLabel;

  final VoidCallback? onTap;

  const BigEventTicket({
    this.width,
    this.title,
    this.label,
    this.subLabel,
    this.onTap,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      child: Container(
        width: width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          boxShadow: blocksMains,
          color: Colors.white,
        ),
        padding: const EdgeInsets.all(15),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (title != null)
              Expanded(
                child: H3Text(
                  title!,
                  color: ColorData.colorTextMain,
                ),
              ),
            const SizedBox(height: 20),
            Row(
              children: [
                if (label != null)
                  Padding(
                    padding: const EdgeInsets.only(right: 10.0),
                    child: H3Text(
                      label!,
                      color: ColorData.colortTextSecondary,
                    ),
                  ),
                if (subLabel != null)
                  Expanded(
                    child: P1Text(
                      subLabel!,
                      color: ColorData.colortTextSecondary,
                      textAlign: TextAlign.end,
                    ),
                  ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
