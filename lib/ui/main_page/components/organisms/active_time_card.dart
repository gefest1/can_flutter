import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';

class ActiveTimeCard extends StatelessWidget {
  final String? title;
  final String? label;
  final String? subLabel;
  final VoidCallback? onTap;

  const ActiveTimeCard({
    this.title,
    this.label,
    this.subLabel,
    this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.only(
          left: 10,
          right: 10,
          top: 10,
          bottom: 15,
        ),
        decoration: const BoxDecoration(
          color: Color(0xffF3F4F6),
          borderRadius: BorderRadius.all(Radius.circular(6)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Expanded(
                  child: title == null
                      ? const SizedBox()
                      : H4Text(
                          title!,
                          color: ColorData.colorTextMain,
                          textAlign: TextAlign.left,
                        ),
                ),
                const SizedBox(width: 10),
                if (label != null)
                  P2Text(
                    label!,
                    color: ColorData.colorMainLink,
                    textAlign: TextAlign.left,
                  )
              ],
            ),
            if (subLabel != null)
              // const SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: P0Text(
                  subLabel!,
                  color: ColorData.colorMainBlack,
                  textAlign: TextAlign.left,
                ),
              ),
          ],
        ),
      ),
    );
  }
}
