import 'dart:async';

import 'package:flutter/material.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';
import 'package:tube/ui/main_page/components/molecules/buy_ticket_card.dart';
import 'package:tube/utils/colors.dart';

class InputModelBuyTicket {
  final String? fullName;
  final String? email;
  final String? phoneNumber;
  final String? iin;
  final bool? me;

  const InputModelBuyTicket({
    this.fullName,
    this.email,
    this.phoneNumber,
    this.iin,
    this.me,
  });
}

class _InputModel {
  late final TextEditingController fullNameController;
  late final TextEditingController emailController;
  late final TextEditingController phoneNumberController;
  late final TextEditingController iinController;

  _InputModel(VoidCallback callback) {
    fullNameController = TextEditingController()..addListener(callback);
    emailController = TextEditingController()..addListener(callback);
    phoneNumberController = TextEditingController()..addListener(callback);
    iinController = TextEditingController()..addListener(callback);
  }
}

enum TypePart { my, another }

class BuyTicketAdaptiveBody extends StatefulWidget {
  final StreamController<InputModelBuyTicket?> streamController;

  const BuyTicketAdaptiveBody({
    required this.streamController,
    Key? key,
  }) : super(key: key);

  @override
  State<BuyTicketAdaptiveBody> createState() => _BuyTicketAdaptiveBodyState();
}

class _BuyTicketAdaptiveBodyState extends State<BuyTicketAdaptiveBody> {
  late final ValueNotifier<TypePart?> type = ValueNotifier<TypePart?>(null)
    ..addListener(() {
      setState(() {});
    });
  TextEditingController get fullNameController =>
      inpController.fullNameController;
  TextEditingController get emailController => inpController.emailController;
  TextEditingController get phoneNumberController =>
      inpController.phoneNumberController;
  TextEditingController get iinController => inpController.iinController;

  void _update() {
    final valid = fullNameController.text.isNotEmpty &&
        emailController.text.isNotEmpty &&
        phoneNumberController.text.isNotEmpty &&
        iinController.text.isNotEmpty;
    final newValue = valid
        ? InputModelBuyTicket(
            email: emailController.text,
            fullName: fullNameController.text,
            iin: iinController.text,
            phoneNumber: phoneNumberController.text,
          )
        : null;
    widget.streamController.add(newValue);
  }

  late _InputModel inpController = _InputModel(_update);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        BuyTicketCard(
          title: 'Мой',
          active: type.value == TypePart.my,
          onTap: () {
            type.value = TypePart.my;
            widget.streamController.add(
              const InputModelBuyTicket(me: true),
            );
          },
        ),
        const SizedBox(height: 20),
        BuyTicketCard(
          title: 'Кого-то другого',
          active: type.value == TypePart.another,
          onTap: () {
            type.value = TypePart.another;
          },
        ),
        const SizedBox(height: 40),
        AnimatedCrossFade(
          sizeCurve: Curves.easeOut,
          firstChild: const SizedBox(width: double.infinity),
          secondChild: Column(
            children: [
              CustomTextField(
                scrollPadding: const EdgeInsets.only(bottom: 100),
                hintText: 'Иванов Иван',
                title: 'Имя владельца абонемента',
                controller: inpController.fullNameController,
              ),
              const SizedBox(height: 36),
              CustomTextField(
                scrollPadding: const EdgeInsets.only(bottom: 200),
                icon: const Icon(
                  Icons.alternate_email,
                  color: ColorData.colorElementsActive,
                ),
                controller: inpController.emailController,
                hintText: 'mail@mail.kz',
                title:
                    'Электронная почта. Если её нет, то для пользования абонементом нужно будет ваше присутствие',
              ),
              const SizedBox(height: 36),
              CustomTextField(
                scrollPadding: const EdgeInsets.only(bottom: 200),
                icon: const Icon(
                  Icons.beenhere,
                  color: ColorData.colorElementsActive,
                ),
                controller: inpController.iinController,
                hintText: '12 цифр',
                title: 'ИИН',
              ),
              const SizedBox(height: 36),
              CustomTextField(
                scrollPadding: const EdgeInsets.only(bottom: 400),
                icon: const Icon(
                  Icons.call,
                  color: ColorData.colorElementsActive,
                ),
                hintText: '+7',
                title: 'Номер телефона. Нужен для быстрой связи с вами.',
                controller: inpController.phoneNumberController,
              ),
            ],
          ),
          crossFadeState: type.value == TypePart.another
              ? CrossFadeState.showSecond
              : CrossFadeState.showFirst,
          duration: const Duration(milliseconds: 400),
        )
      ],
    );
  }
}
