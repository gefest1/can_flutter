import 'package:flutter/material.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';
import 'package:tube/utils/colors.dart';

class FamilyBody extends StatelessWidget {
  final bool active;

  const FamilyBody({
    required this.active,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedCrossFade(
      firstChild: Container(height: 0.0),
      secondChild: Column(
        children: [
          const SizedBox(height: 40),
          const CustomTextField(
            hintText: 'Иван',
            title: 'Имя владельца',
          ),
          const SizedBox(height: 30),
          const CustomTextField(
            hintText: 'Иванов',
            title: 'Фамилия',
          ),
          const SizedBox(height: 30),
          const CustomTextField(
            hintText: 'Иван',
            title: 'Имя владельца',
            icon: Icon(
              Icons.date_range,
              color: ColorData.colorElementsSecondary,
              size: 24,
            ),
          ),
          const SizedBox(height: 30),
          const CustomTextField(
            scrollPadding: EdgeInsets.only(bottom: 400),
            hintText: 'mail@mail.kz',
            title:
                'Электронная почта. Если её нет, то для пользования абонементом нужно будет ваше присутствие',
            icon: Icon(
              Icons.alternate_email,
              color: ColorData.colorElementsSecondary,
              size: 24,
            ),
          ),
          SizedBox(
            height: MediaQuery.of(context).viewInsets.bottom,
          )
        ],
      ),
      crossFadeState:
          active ? CrossFadeState.showSecond : CrossFadeState.showFirst,
      duration: const Duration(milliseconds: 300),
    );
  }
}
