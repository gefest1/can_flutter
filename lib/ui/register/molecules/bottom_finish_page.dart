import 'package:flutter/material.dart';
import 'package:tube/main.dart';
import 'package:tube/ui/common/molecules/custom_button.dart';
import 'package:tube/ui/register/register_company.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class BottomFinishPage extends StatelessWidget {
  final InputModel inputModel;
  final VoidCallback onTap;

  const BottomFinishPage({
    required this.inputModel,
    required this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    late final media = MediaQuery.of(navigatorKeyGlobal.currentContext!);
    return Positioned(
      bottom: 0,
      left: 0,
      right: 0,
      child: Container(
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(5),
          ),
          color: Color(0xccffffff),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, -4),
              blurRadius: 15,
              color: Color(0x0d000000),
            ),
          ],
        ),
        padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
        child: Column(
          children: [
            CustomButton(
              title: 'Продолжить',
              onTap: onTap,
            ),
            const SizedBox(height: 10),
            RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                style: TextStyle(
                  fontSize: P2TextStyle.fontSize,
                  fontWeight: P2TextStyle.fontWeight,
                  height: P2TextStyle.height,
                  color: ColorData.colorTextMain,
                  fontFamily: 'InterTight',
                ),
                children: [
                  TextSpan(
                    text: 'Нажимая «Продолжить» вы соглашаетесь с\n',
                  ),
                  TextSpan(
                    text: 'Пользовательским соглашением компании.',
                    style: TextStyle(
                      color: ColorData.colorMainLink,
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 20 + media.padding.bottom),
          ],
        ),
      ),
    );
  }
}
