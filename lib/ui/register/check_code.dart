import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:graphql/client.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/blocs/user/user_bloc.dart';
import 'package:tube/main.dart';
import 'package:tube/ui/common/molecules/custom_button.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';
import 'package:tube/ui/common/molecules/fast_app_bar.dart';
import 'package:tube/ui/register/register_company.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';
import 'package:tube/utils/loader.dart';
import 'package:tube/utils/restart_widget.dart';

class CheckCodePage extends StatefulWidget {
  final String email;
  final ScrollController controller;

  const CheckCodePage({
    required this.email,
    required this.controller,
    super.key,
  });

  @override
  State<CheckCodePage> createState() => _CheckCodePageState();
}

class _CheckCodePageState extends State<CheckCodePage> {
  late final _media = MediaQuery.of(navigatorKeyGlobal.currentContext!);
  final TextEditingController controller = TextEditingController();

  void _verify() async {
    loaderActivate(context);
    try {
      final mutationResult = await graphqlClient.mutate$globalLogin(
        Options$Mutation$globalLogin(
          variables: Variables$Mutation$globalLogin(
            email: widget.email,
            code: controller.text.replaceAll(" ", ""),
          ),
          fetchPolicy: FetchPolicy.networkOnly,
        ),
      );
      // final mutationResult = client.mutate(
      //   MutationOptions(
      //     document: gql('''
      //       mutation(
      //         \$email: String!
      //         \$code: String!
      //       ) {
      //         globalLogin(
      //           email: \$email
      //           code: \$code
      //         )
      //         {
      //           token
      //           user {
      //             _id
      //             iin
      //             date
      //             fullName
      //             phoneNumber
      //             photoUrl {
      //               M
      //               XL
      //               thumbnail
      //             }
      //           }
      //         }
      //       }'''),
      //     variables: {
      //       "email": widget.email,
      //       "code": controller.text.replaceAll(" ", ""),
      //     },
      //     fetchPolicy: FetchPolicy.networkOnly,
      //   ),
      // );

      if (mutationResult.hasException) {
        throw mutationResult.exception!;
      }
      final data = mutationResult.parsedData!;
      final String token = data.globalLogin.token;

      final User? user = data.globalLogin.user == null
          ? null
          : User.fromMap(data.globalLogin.user!.toJson());
      // fastToken = token;
      await sharedBox.put('token', token);
      if (!mounted) return;
      if (user == null) {
        showModalBottomSheet(
          backgroundColor: Colors.transparent,
          isScrollControlled: true,
          isDismissible: true,
          context: context,
          builder: (_) {
            return const RegisterCompany();
          },
        );
      } else {
        context.read<UserBloc>().add(InitUserEvent(user));

        RestartWidget.restartApp(context);

        return;
      }
    } catch (e) {
      log(e.toString());
    }

    killLoaderOverlay();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final f = FocusScope.of(context);

        if (f.hasFocus || f.hasPrimaryFocus) {
          f.unfocus();
        }
      },
      child: Material(
        color: Colors.white,
        borderRadius: const BorderRadius.vertical(
          top: Radius.circular(10),
        ),
        child: CustomScrollView(
          controller: widget.controller,

          slivers: [
            SliverToBoxAdapter(
              child: Column(
                children: [
                  FastAppBar(
                    title: 'Подтверждение почты',
                    preIcon: Icons.arrow_back,
                    preOnTap: () => Navigator.pop(context),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(height: 40),
                        const Icon(
                          Icons.mail,
                          color: ColorData.colorElementsActive,
                          size: 45,
                        ),
                        const SizedBox(height: 20),
                        Text(
                          'Проверьте свой почтовый ящик ${widget.email}',
                          style: const TextStyle(
                            fontSize: H3TextStyle.fontSize,
                            fontWeight: H3TextStyle.fontWeight,
                            height: H3TextStyle.height,
                            color: ColorData.colorElementsActive,
                          ),
                        ),
                        CustomTextField(
                          maxLength: 4,
                          controller: controller,
                          keyboardType: TextInputType.phone,
                          scrollPadding: const EdgeInsets.only(bottom: 300),
                          hintText: 'Код из письма',
                          title:
                              'Если вы не можете найти письмо, попробуйте посмотреть в спаме. Или введите другую почту',
                        ),
                        const SizedBox(height: 20),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            SliverFillRemaining(
              hasScrollBody: false,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: EdgeInsets.only(
                    left: 20,
                    right: 20,
                    top: 20,
                    bottom: _media.padding.bottom + 20,
                  ),
                  child: CustomButton(
                    onTap: _verify,
                    title: 'Продолжить',
                  ),
                ),
              ),
            ),
          ],
          // children: [
          //   FastAppBar(
          //     title: 'Подтверждение почты',
          //     preIcon: Icons.arrow_back,
          //     preOnTap: () => Navigator.pop(context),
          //   ),
          //   Expanded(
          //     child: CustomScrollView(
          //       slivers: [
          //         SliverToBoxAdapter(
          //           child: Padding(
          //             padding: const EdgeInsets.symmetric(
          //               horizontal: 20,
          //             ),
          //             child: Column(
          //               crossAxisAlignment: CrossAxisAlignment.start,
          //               children: [
          //                 const SizedBox(height: 40),
          //                 const Icon(
          //                   Icons.mail,
          //                   color: ColorData.colorElementsActive,
          //                   size: 45,
          //                 ),
          //                 const SizedBox(height: 20),
          //                 const Text(
          //                   'Проверьте свой почтовый ящик',
          //                   style: TextStyle(
          //                     fontSize: H3TextStyle.fontSize,
          //                     fontWeight: H3TextStyle.fontWeight,
          //                     height: H3TextStyle.height,
          //                     color: ColorData.colorElementsActive,
          //                   ),
          //                 ),
          //                 CustomTextField(
          //                   maxLength: 4,
          //                   controller: controller,
          //                   keyboardType: TextInputType.phone,
          //                   scrollPadding: const EdgeInsets.only(bottom: 300),
          //                   hintText: 'Код из письма',
          //                   title:
          //                       'Если вы не можете найти письмо, попробуйте посмотреть в спаме. Или введите другую почту',
          //                 ),
          //                 const SizedBox(height: 20),
          //               ],
          //             ),
          //           ),
          //         ),
          //         SliverFillRemaining(
          //           hasScrollBody: false,
          //           child: Align(
          //             alignment: Alignment.bottomCenter,
          //             child: Padding(
          //               padding: EdgeInsets.only(
          //                 left: 20,
          //                 right: 20,
          //                 top: 20,
          //                 bottom: _media.padding.bottom + 20,
          //               ),
          //               child: CustomButton(
          //                 onTap: _verify,
          //                 title: 'Продолжить',
          //               ),
          //             ),
          //           ),
          //         ),
          //       ],
          //     ),
          //   ),
          // ],
        ),
      ),
    );
  }
}
