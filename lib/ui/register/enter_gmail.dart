import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:graphql/client.dart';

import 'package:tube/flexible/lib/src/flexible_bottom_sheet_route.dart';
import 'package:tube/main.dart';
import 'package:tube/ui/common/molecules/custom_button.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';
import 'package:tube/ui/common/molecules/fast_app_bar.dart';
import 'package:tube/ui/register/check_code.dart';
import 'package:tube/utils/loader.dart';

class EnterGmailPage extends StatefulWidget {
  final ScrollController controller;

  const EnterGmailPage({required this.controller, super.key});

  @override
  State<EnterGmailPage> createState() => _EnterGmailPageState();
}

class _EnterGmailPageState extends State<EnterGmailPage> {
  late final _media = MediaQuery.of(navigatorKeyGlobal.currentContext!);
  final TextEditingController controller = TextEditingController();

  void _sendCode() async {
    loaderActivate(context);
    try {
      final mutationResult = await graphqlClient.mutate$sendRandomSms(
        Options$Mutation$sendRandomSms(
          variables: Variables$Mutation$sendRandomSms(
            email: controller.text.replaceAll(" ", ""),
          ),
          fetchPolicy: FetchPolicy.networkOnly,
        ),
      );

      if (mutationResult.hasException) {
        throw mutationResult.exception!;
      }
      if (!mounted) return;
      showFlexibleBottomSheet(
        context: context,
        minHeight: 0,
        initHeight: 0.95,
        maxHeight: 0.95,
        isDismissible: true,
        anchors: [0.95],
        builder: (context, scrollController, bottomSheetOffset) {
          return CheckCodePage(
            email: controller.text.replaceAll(" ", ""),
            controller: scrollController,
          );
        },
      );
      // showModalBottomSheet(
      //   backgroundColor: Colors.transparent,
      //   isScrollControlled: true,
      //   isDismissible: true,
      //   context: context,
      //   builder: (_) {
      //     return CheckCodePage(
      //       email: controller.text.replaceAll(" ", ""),
      //     );
      //   },
      // );
    } catch (e) {
      log(e.toString());
    }
    killLoaderOverlay();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final f = FocusScope.of(context);

        if (f.hasFocus || f.hasPrimaryFocus) {
          f.unfocus();
        }
      },
      child: Material(
        color: Colors.white,
        borderRadius: const BorderRadius.vertical(
          top: Radius.circular(10),
        ),
        child: CustomScrollView(
          controller: widget.controller,
          slivers: [
            SliverToBoxAdapter(
              child: Column(
                children: [
                  FastAppBar(
                    title: 'Ввод почты',
                    preIcon: Icons.arrow_back,
                    preOnTap: () => Navigator.pop(context),
                  ),
                  const SizedBox(height: 40),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: CustomTextField(
                      maxLines: 1,
                      controller: controller,
                      hintText: 'primer@email.com',
                      title:
                          'Мы отправим письмо с кодом, чтобы подтвердить ваш адрес',
                    ),
                  )
                ],
              ),
            ),
            SliverFillRemaining(
              child: Padding(
                padding: EdgeInsets.only(
                  bottom: _media.padding.bottom + 20,
                  top: 20,
                  left: 20,
                  right: 20,
                ),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: CustomButton(
                    title: 'Отправить код',
                    onTap: _sendCode,
                  ),
                ),
              ),
            ),
          ],
        ),
        // Column(
        //   children: [
        //     FastAppBar(
        //       title: 'Ввод почты',
        //       preIcon: Icons.arrow_back,
        //       preOnTap: () => Navigator.pop(context),
        //     ),
        //     Expanded(
        //       child: CustomScrollView(
        //         slivers: [
        //           const SliverToBoxAdapter(child: SizedBox(height: 40)),
        //           SliverToBoxAdapter(
        //             child: Padding(
        //               padding: const EdgeInsets.symmetric(horizontal: 20),
        //               child: CustomTextField(
        //                 controller: controller,
        //                 hintText: 'primer@email.com',
        //                 title: 'Мы отправим письмо с кодом, чтобы подтвердить ваш адрес',
        //               ),
        //             ),
        //           ),
        //           SliverFillRemaining(
        //             child: Padding(
        //               padding: EdgeInsets.only(
        //                 bottom: _media.padding.bottom + 20,
        //                 top: 20,
        //                 left: 20,
        //                 right: 20,
        //               ),
        //               child: Align(
        //                 alignment: Alignment.bottomCenter,
        //                 child: CustomButton(
        //                   title: 'Отправить код',
        //                   onTap: _sendCode,
        //                 ),
        //               ),
        //             ),
        //           ),
        //         ],
        //       ),
        //     ),
        //   ],
        // ),
      ),
    );
  }
}
