import 'dart:async';

import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/tariff/model/tariff.model.dart';
import 'package:tube/logic/blocs/tariff/tariff_bloc.dart';
import 'package:tube/logic/general_providers.dart';

import 'package:tube/logic/provider/booking/get_special_booking.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/page/train_page.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/ui/subscription/shedule_page.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class SubscriptionPage extends StatefulWidget {
  final String clubId;
  final String bookingId;
  final Tariff tariff;

  const SubscriptionPage({
    required this.clubId,
    required this.bookingId,
    required this.tariff,
    Key? key,
  }) : super(key: key);

  @override
  State<SubscriptionPage> createState() => _SubscriptionPageState();
}

class _SubscriptionPageState extends State<SubscriptionPage>
    with SingleTickerProviderStateMixin {
  late final TabController tabController =
      TabController(vsync: this, length: 2);
  late GetSpecialBooking specBooking = Provider.of<GetSpecialBooking>(context);

  late TariffBloc tariffBloc =
      Provider.of<UserBlocMap>(context, listen: false).getTariffBloc(
    MainDrawerState.clubId!,
  );
  StreamSubscription? str;

  late Tariff tariff = widget.tariff;

  @override
  void initState() {
    super.initState();

    Future(() {
      str = tariffBloc.stream.listen((state) {
        if (state is DataTariffState) {
          final newTariff =
              state.tariffs.firstWhereOrNull((el) => el.id == tariff.id);
          if (newTariff != null) {
            setState(() {
              tariff = newTariff;
            });
          }
        }
      });
      specBooking.getBooking(widget.bookingId);
    });
  }

  @override
  void dispose() {
    tabController.dispose();
    str?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: ColorData.colorMainElements,
        leading: IconButton(
          iconSize: 24,
          color: Colors.white,
          icon: const Icon(
            Icons.arrow_back,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        centerTitle: true,
        title: H4Text(
          tariff.booking!.name!,
          color: Colors.white,
        ),
        // actions: [

        // ],
        bottom: TabBar(
          controller: tabController,
          labelStyle: const TextStyle(
            fontWeight: P1TextStyle.fontWeight,
            fontSize: P1TextStyle.fontSize,
            height: P1TextStyle.height,
          ),
          indicatorColor: Colors.white,
          indicatorWeight: 5,
          labelPadding: const EdgeInsets.only(
            bottom: 10,
            top: 15,
          ),
          tabs: const [
            Text('Тренировка'),
            // Text('Информация'),
            Text('Расписание'),
          ],
        ),
      ),
      body: TabBarView(
        controller: tabController,
        children: [
          TrainPage(
            clubId: MainDrawerState.currentParticipance!.clubId!,
            tariff: tariff,
            deadlineDate: tariff.deadlineDate!,
          ),
          // const InfoPage(),
          specBooking.bookings[widget.bookingId] == null
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : ShedulePage(
                  clubId: MainDrawerState.currentParticipance!.clubId!,
                  tariffId: tariff.id!,
                ),
        ],
      ),
    );
  }
}
