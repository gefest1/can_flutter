import 'package:flutter/material.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';

class SpaceControllers extends StatelessWidget {
  final List<String> values;
  final void Function(int)? removeAt;

  const SpaceControllers({
    required this.values,
    this.removeAt,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (values.isEmpty) {
      return const SizedBox();
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: List.generate(values.length * 2 - 1, (index) {
        if (index % 2 == 1) return const SizedBox(height: 5);
        final realIndex = index ~/ 2;
        return SizeTapAnimation(
          child: GestureDetector(
            onTap: () => removeAt?.call(realIndex),
            child: Container(
              decoration: const BoxDecoration(
                color: Color(0x1a969696),
                borderRadius: BorderRadius.all(
                  Radius.circular(5),
                ),
              ),
              padding: const EdgeInsets.all(10),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    values[realIndex],
                    style: const TextStyle(
                      color: ColorData.colorButtonMain,
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                      height: 21 / 18,
                    ),
                  ),
                  const SizedBox(width: 5),
                  const Icon(
                    Icons.clear,
                    size: 21,
                    color: ColorData.colorButtonMain,
                  )
                ],
              ),
            ),
          ),
        );
      }),
    );
  }
}
