import 'package:flutter/material.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/ui/main_page/notfication_page.dart';
import 'package:tube/ui/main_page/profile_page.dart';
import 'package:tube/ui/trainer_page/components/molecules/bottom_tab_bar.dart';
import 'package:tube/ui/trainer_page/components/pages/finance_page.dart';
import 'package:tube/ui/trainer_page/components/pages/train_boy_page.dart';

const sssColor = [
  Colors.red,
  Colors.orange,
  Colors.cyan,
  Colors.brown,
  Colors.lime,
];

class TrainerMainPage extends StatefulWidget {
  const TrainerMainPage({Key? key}) : super(key: key);

  @override
  State<TrainerMainPage> createState() => _TrainerMainPageState();
}

class _TrainerMainPageState extends State<TrainerMainPage> with SingleTickerProviderStateMixin {
  late TabController tabController = TabController(
    vsync: this,
    length: 4,
  );

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const MainDrawer(),
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          TabBarView(
            controller: tabController,
            physics: const NeverScrollableScrollPhysics(),
            children: const [
              TrainBodyPage(),
              FinancePage(),
              NotificationPage(),
              ProfilePage(),
            ],
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: MainTrainerBottom(
              tabController: tabController,
            ),
          ),
        ],
      ),
    );
  }
}
