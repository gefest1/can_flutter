import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:tube/utils/colors.dart';

class LittleChip extends StatelessWidget {
  final String text;

  const LittleChip({required this.text, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 76,
      height: 40,
      child: DecoratedBox(
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(6)),
          boxShadow: [
            BoxShadow(
              offset: Offset(2, 2),
              blurRadius: 15,
              color: Color(0x14000000),
            ),
          ],
        ),
        child: Center(
          child: AutoSizeText(
            text,
            minFontSize: 12,
            maxFontSize: 18,
            style: const TextStyle(
              color: ColorData.colorElementsActive,
              height: 21 / 18,
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
      ),
    );
  }
}
