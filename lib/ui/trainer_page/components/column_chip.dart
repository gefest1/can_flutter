import 'package:flutter/material.dart';
import 'package:tube/ui/trainer_page/components/little_chip.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class ColumnChip extends StatelessWidget {
  final IconData iconData;
  final String title1;
  final String title2;
  final String label;

  const ColumnChip({
    required this.iconData,
    required this.title1,
    required this.title2,
    required this.label,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Icon(
          iconData,
          size: 24,
          color: ColorData.colorElementsSecondary,
        ),
        Text(
          label,
          style: const TextStyle(
            color: ColorData.colorElementsSecondary,
            fontSize: H5TextStyle.fontSize,
            fontWeight: H5TextStyle.fontWeight,
            height: H5TextStyle.height,
          ),
        ),
        const SizedBox(height: 10),
        LittleChip(text: title1),
        const Padding(
          padding: EdgeInsets.only(top: 5, bottom: 7),
          child: Text(
            'из',
            style: TextStyle(
              color: ColorData.colortTextSecondary,
              fontSize: 14,
              height: 17 / 14,
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
        LittleChip(text: title2),
      ],
    );
  }
}
