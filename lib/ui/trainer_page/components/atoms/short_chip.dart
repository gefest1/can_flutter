import 'package:flutter/material.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class ShortChip extends StatelessWidget {
  final bool active;
  final String title;

  const ShortChip({
    required this.title,
    this.active = false,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          width: 1,
          color: ColorData.colorElementsActive,
        ),
        color: active ? ColorData.colorElementsActive : Colors.transparent,
        borderRadius: BorderRadius.circular(5),
      ),
      padding: const EdgeInsets.all(10),
      child: Text(
        title,
        style: TextStyle(
          fontSize: P1TextStyle.fontSize,
          fontWeight: P1TextStyle.fontWeight,
          height: P1TextStyle.height,
          color: active ? Colors.white : ColorData.colorElementsActive,
        ),
      ),
    );
  }
}
