import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';

class BalanceCard extends StatelessWidget {
  final VoidCallback? onTap;

  const BalanceCard({
    this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Padding(
                        padding: EdgeInsets.only(bottom: 8.0),
                        child: H5Text(
                          'Баланс',
                          color: Color(0xff959595),
                        ),
                      ),
                      Wrap(
                        crossAxisAlignment: WrapCrossAlignment.center,
                        children: [
                          RichText(
                            text: const TextSpan(
                              text: '0',
                              style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'InterTight',
                              ),
                              children: [
                                TextSpan(
                                  text: '₸',
                                  style: TextStyle(
                                    fontFamily: '',
                                  ),
                                )
                              ],
                            ),
                          ),
                          const SizedBox(width: 5),
                          const P2Text(
                            'На 02.12.2021',
                            color: ColorData.colortTextSecondary,
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                const Icon(
                  Icons.chevron_right,
                  size: 24,
                  color: ColorData.colorElementsSecondary,
                )
              ],
            ),
          ),
          const Divider(
            height: 0,
            thickness: 0.5,
            color: Color(0xffA5A5A5),
          ),
        ],
      ),
    );
  }
}
