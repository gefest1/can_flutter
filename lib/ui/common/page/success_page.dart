import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/tariff/model/tariff.model.dart';
import 'package:tube/logic/blocs/tariff/model/train_time.model.dart';
import 'package:tube/logic/blocs/tariff/tariff_bloc.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/custom_button.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class SuccessPage extends StatelessWidget {
  final String tariffId;

  const SuccessPage({
    required this.tariffId,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: ColorData.colorMainElements,
      child: Column(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 60),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Icon(
                    Icons.check,
                    size: 188,
                    color: Colors.white,
                  ),
                  const SizedBox(height: 20),
                  BlocBuilder(
                    bloc: Provider.of<UserBlocMap>(context, listen: false)
                        .getTariffBloc(
                      MainDrawerState.currentParticipance!.clubId!,
                    ),
                    builder: (ctx, state) {
                      Tariff? tariff;
                      if (state is DataTariffState) {
                        tariff = state.tariffs.firstWhere((element) {
                          return element.id == tariffId;
                        });
                      }
                      if (tariff == null) return const SizedBox();
                      final count = tariff.trainTimes?.fold(
                            0,
                            (num previousValue, TrainTime element) {
                              if (element.come == false &&
                                  element.endTime!.compareTo(DateTime.now()) ==
                                      1) {
                                return previousValue + 1;
                              }
                              return previousValue;
                            },
                          ) ??
                          0;
                      return Column(
                        children: [
                          P0Text(
                            'Посещение занятия по абонементу «${tariff.booking?.name}» учтено!',
                            color: Colors.white,
                            textAlign: TextAlign.center,
                          ),
                          const SizedBox(height: 20),
                          Text(
                            'Осталось $count',
                            style: const TextStyle(
                              color: Colors.white,
                              fontSize: P0TextStyle.fontSize,
                              fontWeight: FontWeight.w700,
                              height: P0TextStyle.height,
                            ),
                            // color: Colors.white,
                            textAlign: TextAlign.center,
                          ),
                        ],
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(
              bottom: MediaQuery.of(context).padding.bottom + 10,
              left: 20,
              right: 20,
              top: 10,
            ),
            color: const Color(0xccffffff),
            child: CustomButton(
              title: 'Понятно!',
              onTap: () {
                Navigator.popUntil(context, (route) => route.isFirst);
              },
              color: ColorData.colorMainLink,
            ),
          ),
        ],
      ),
    );
  }
}
