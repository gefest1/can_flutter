import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tube/flexible/lib/src/flexible_bottom_sheet_route.dart';

import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/blocs/user/user_bloc.dart';
import 'package:tube/ui/add_compnay/pages/new_company.dart';
import 'package:tube/ui/common/atoms/menu_drawer.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';

import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class EmptyPage extends StatefulWidget {
  const EmptyPage({Key? key}) : super(key: key);

  @override
  State<EmptyPage> createState() => _EmptyPageState();
}

class _EmptyPageState extends State<EmptyPage> {
  late final userBloc = BlocProvider.of<UserBloc>(context, listen: true);
  Widget? leadinW;
  @override
  void dispose() {
    leadinW = null;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final state = userBloc.state;
    User? user;
    if (state is DataUserState) {
      user = state.user;
    }
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Align(
          alignment: Alignment.centerRight,
          child: H4Text('Check All Now'),
        ),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(5),
          ),
        ),
        leading: leadinW ??= MenuDrawer(
          parentContext: context,
        ),
      ),
      body: ListView(
        padding: const EdgeInsets.all(20),
        children: [
          GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              showFlexibleBottomSheet(
                minHeight: 0,
                initHeight: 0.95,
                maxHeight: 0.95,
                context: context,
                anchors: [0.95],
                builder: (ctx, scroll, _) {
                  return NewCompany(
                    controller: scroll,
                  );
                },
              );
              // showFlexibleBottomSheet(
              //   context: context,
              //   minHeight: 0,
              //   initHeight: 0.95,
              //   maxHeight: 0.95,
              //   builder: (ctx, scoll, _) => Material(
              //     color: Colors.transparent,
              //     child: BottomBar(
              //       scrollController: scoll,
              //     ),
              //   ),
              //   anchors: [0.95],
              // );
            },
            child: Container(
              padding: const EdgeInsets.all(20),
              decoration: BoxDecoration(
                border: Border.all(
                  color: const Color(0x1a000000),
                  width: 0.5,
                ),
                borderRadius: BorderRadius.circular(5),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Icon(
                    Icons.account_balance,
                    size: 36,
                    color: ColorData.colorElementsActive,
                  ),
                  const SizedBox(height: 10),
                  const Text(
                    'Владельцам бизнеса',
                    style: TextStyle(
                      fontSize: H3TextStyle.fontSize,
                      fontWeight: H3TextStyle.fontWeight,
                      height: H3TextStyle.height,
                      color: ColorData.colorTextMain,
                    ),
                  ),
                  const SizedBox(height: 5),
                  const Text(
                    'Избавиться от бумажной волокиты, видеть аналитику по данным компании, принимать платежи безналично',
                    style: TextStyle(
                      fontSize: P1TextStyle.fontSize,
                      fontWeight: P1TextStyle.fontWeight,
                      height: P1TextStyle.height,
                      color: ColorData.colortTextSecondary,
                    ),
                  ),
                  if (user?.isAdmin ?? false)
                    Container(
                      margin: const EdgeInsets.only(top: 20),
                      height: 40,
                      decoration: BoxDecoration(
                        color: const Color(0xffF5F5F5),
                        borderRadius: BorderRadius.circular(5),
                      ),
                      alignment: Alignment.center,
                      child: const Text(
                        'Создать компанию',
                        style: TextStyle(
                            fontSize: P1TextStyle.fontSize,
                            fontWeight: P1TextStyle.fontWeight,
                            height: P1TextStyle.height,
                            color: ColorData.colorMainLink),
                      ),
                    ),
                ],
              ),
            ),
          ),
          const SizedBox(height: 20),
          Container(
            padding: const EdgeInsets.all(20),
            decoration: BoxDecoration(
              border: Border.all(
                color: const Color(0x1a000000),
                width: 0.5,
              ),
              borderRadius: BorderRadius.circular(5),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const [
                P1Text(
                  'Для добавления в существующую компанию перейдите по ссылке-приглашению, или попросите администратора добавить вашу почту',
                  color: ColorData.colorTextMain,
                ),
                SizedBox(height: 30),
                Icon(
                  Icons.face,
                  color: ColorData.colorElementsActive,
                  size: 36,
                ),
                SizedBox(height: 10),
                H3Text(
                  'Клиентам',
                  color: ColorData.colorTextMain,
                ),
                SizedBox(height: 5),
                P1Text(
                  'Записываться и оплачивать услуги удаленно, быть в курсе посещаемости',
                  color: ColorData.colortTextSecondary,
                ),
                SizedBox(height: 30),
                Icon(
                  Icons.perm_contact_calendar,
                  color: ColorData.colorElementsActive,
                  size: 36,
                ),
                SizedBox(height: 10),
                H3Text(
                  'Сотрудникам',
                  color: ColorData.colorTextMain,
                ),
                SizedBox(height: 5),
                P1Text(
                  'Видеть свое расписание, заработок, не тратить время на заполнение лишних бумаг',
                  color: ColorData.colortTextSecondary,
                ),
              ],
            ),
          ),
          const SizedBox(height: 40),
        ],
      ),
    );
  }
}
