import 'dart:io';

import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class PaymentPage extends StatefulWidget {
  final String initialUrl;
  const PaymentPage({required this.initialUrl, super.key});

  @override
  State<PaymentPage> createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  @override
  void initState() {
    super.initState();

    if (Platform.isAndroid) WebView.platform = AndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: SafeArea(
        child: WebView(
          allowsInlineMediaPlayback: true,
          initialUrl: widget.initialUrl,
          zoomEnabled: true,
          onWebViewCreated: (controller) {
            // controller.
          },
          navigationDelegate: (navigation) async {
            return Future(() {
              if (navigation.url.contains('https://www.checkallnow.net/')) {
                Navigator.pop(context, true);
                return NavigationDecision.prevent;
              }
              return NavigationDecision.navigate;
            });
          },
          javascriptMode: JavascriptMode.unrestricted,
        ),
      ),
    );
  }
}
