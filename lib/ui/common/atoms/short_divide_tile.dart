import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';

class ShortDivideTile extends StatelessWidget {
  final String? title;
  final String? label;

  const ShortDivideTile({
    this.title,
    this.label,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 15),
        if (title != null)
          H4Text(
            title!,
            color: ColorData.colorTextMain,
          ),
        if (label != null)
          Padding(
            padding: const EdgeInsets.only(top: 5.0),
            child: P1Text(
              label!,
              color: ColorData.colorTextMain,
            ),
          ),
        const SizedBox(height: 15),
        const Divider(
          height: 0,
          thickness: 0.2,
          color: Color(0xff969696),
        )
      ],
    );
  }
}
