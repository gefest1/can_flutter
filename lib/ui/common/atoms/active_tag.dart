import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';

class ActiveTag extends StatelessWidget {
  final VoidCallback? onTap;
  final bool active;
  final String title;

  const ActiveTag({
    this.onTap,
    required this.title,
    this.active = false,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        decoration: ShapeDecoration(
          color: active ? ColorData.colorElementsActive : Colors.transparent,
          shape: const StadiumBorder(
            side: BorderSide(
              color: ColorData.colorElementsActive,
              width: 0.5,
            ),
          ),
        ),
        child: P0Text(
          title,
          color: active ? Colors.white : ColorData.colorTextMain,
        ),
      ),
    );
  }
}
