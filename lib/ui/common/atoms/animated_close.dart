import 'package:flutter/material.dart';

class AnimatedFastClose extends StatefulWidget {
  final Widget child;
  final bool active;

  const AnimatedFastClose({
    required this.child,
    this.active = false,
    Key? key,
  }) : super(key: key);

  @override
  State<AnimatedFastClose> createState() => _AnimatedFastCloseState();
}

class _AnimatedFastCloseState extends State<AnimatedFastClose>
    with SingleTickerProviderStateMixin {
  late final AnimationController _opacity = AnimationController(
    vsync: this,
    value: widget.active ? 1.0 : 0.0,
    duration: const Duration(
      milliseconds: 300,
    ),
  );

  @override
  void didUpdateWidget(covariant AnimatedFastClose oldWidget) {
    if (widget.active) {
      _opacity.forward();
    } else {
      _opacity.reverse();
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return SizeTransition(
      sizeFactor: _opacity,
      child: FadeTransition(
        opacity: _opacity,
        child: widget.child,
      ),
    );
  }
}
