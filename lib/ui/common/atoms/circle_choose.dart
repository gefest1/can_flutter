import 'package:flutter/material.dart';
import 'package:tube/utils/colors.dart';

class CircleChoose extends StatefulWidget {
  final bool active;
  final double bigSize;
  final double littleSize;
  final double iconSize;
  final Color? color;

  const CircleChoose({
    required this.active,
    this.bigSize = 42,
    this.littleSize = 32,
    this.iconSize = 18,
    this.color,
    Key? key,
  }) : super(key: key);

  @override
  State<CircleChoose> createState() => _CircleChooseState();
}

class _CircleChooseState extends State<CircleChoose>
    with SingleTickerProviderStateMixin {
  late final AnimationController animationController = AnimationController(
    vsync: this,
    value: widget.active ? 1 : 0,
    duration: const Duration(milliseconds: 300),
  );
  @override
  void didUpdateWidget(covariant CircleChoose oldWidget) {
    if (widget.active) animationController.forward();
    if (!widget.active) animationController.reverse();
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.bigSize,
      width: widget.bigSize,
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.all(Radius.circular(5)),
        border: Border.all(
          width: 1,
          color: ColorData.colorMainLink,
        ),
      ),
      alignment: Alignment.center,
      child: FadeTransition(
        opacity: animationController,
        child: SizedBox(
          height: widget.littleSize,
          width: widget.littleSize,
          child: DecoratedBox(
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(4)),
              color: ColorData.colorMainLink,
            ),
            child: Center(
              child: Icon(
                Icons.done,
                size: widget.iconSize,
                color: widget.color,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
