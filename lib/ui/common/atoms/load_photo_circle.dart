import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class LoadPhotoCircle extends StatelessWidget {
  final double size;
  final XFile? file;
  final VoidCallback onTap;

  const LoadPhotoCircle({
    this.size = 162,
    required this.file,
    required this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          height: size,
          width: size,
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
            color: Color(0xffF3F4F6),
          ),
          child: file != null
              ? ClipOval(
                  child: Image.file(
                    File(file!.path),
                    fit: BoxFit.cover,
                  ),
                )
              : Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Icon(
                      Icons.add_a_photo,
                      color: ColorData.colorTextMain,
                    ),
                    Text(
                      'Загрузить\nлоготип',
                      style: TextStyle(
                        color: ColorData.colorTextMain,
                        fontSize: P2TextStyle.fontSize,
                        height: P2TextStyle.height,
                        fontWeight: P2TextStyle.fontWeight,
                      ),
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
        ),
      ),
    );
  }
}
