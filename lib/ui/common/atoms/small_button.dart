import 'package:flutter/material.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';

class SmallButton extends StatelessWidget {
  final Widget child;
  final VoidCallback? onTap;

  const SmallButton({
    required this.child,
    this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 10),
        decoration: BoxDecoration(
          color: const Color(0xffF5F5F5),
          borderRadius: BorderRadius.circular(5),
        ),
        alignment: Alignment.center,
        child: child,
      ),
    );
  }
}
