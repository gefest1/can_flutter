import 'package:flutter/material.dart';

class AnimatedSwitch extends StatefulWidget {
  final Widget child;
  final bool active;

  const AnimatedSwitch({
    required this.child,
    this.active = false,
    Key? key,
  }) : super(key: key);

  @override
  State<AnimatedSwitch> createState() => _AnimatedSwitchState();
}

class _AnimatedSwitchState extends State<AnimatedSwitch>
    with SingleTickerProviderStateMixin {
  late final AnimationController _opacity = AnimationController(
    vsync: this,
    value: widget.active ? 1.0 : 0.0,
    duration: const Duration(
      milliseconds: 300,
    ),
  );

  @override
  void didUpdateWidget(covariant AnimatedSwitch oldWidget) {
    if (widget.active) {
      _opacity.forward();
    } else {
      _opacity.reverse();
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return FadeTransition(
      opacity: _opacity,
      child: widget.child,
    );
  }
}
