import 'package:flutter/material.dart';

class TwoGisCustomPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.4267275, size.height * 0.5054391);
    path_0.cubicTo(
        size.width * 0.4267275,
        size.height * 0.4466043,
        size.width * 0.4217529,
        size.height * 0.3904870,
        size.width * 0.4130843,
        size.height * 0.3384357);
    path_0.cubicTo(
        size.width * 0.4107902,
        size.height * 0.5097652,
        size.width * 0.3331098,
        size.height * 0.6424087,
        size.width * 0.2875020,
        size.height * 0.7301913);
    path_0.lineTo(size.width * 0.4016588, size.height * 0.7301913);
    path_0.cubicTo(
        size.width * 0.4176176,
        size.height * 0.6631652,
        size.width * 0.4267275,
        size.height * 0.5867043,
        size.width * 0.4267275,
        size.height * 0.5054391);
    path_0.close();
    path_0.moveTo(size.width * 0.3442000, size.height * 0.1281917);
    path_0.cubicTo(
        size.width * 0.3080549,
        size.height * 0.06510870,
        size.width * 0.2627373,
        size.height * 0.02717391,
        size.width * 0.2133765,
        size.height * 0.02717391);
    path_0.cubicTo(
        size.width * 0.09554373,
        size.height * 0.02717391,
        size.width * 0.00001914827,
        size.height * 0.2413113,
        size.width * 0.00001914827,
        size.height * 0.5054391);
    path_0.cubicTo(
        size.width * 0.00001914827,
        size.height * 0.7695652,
        size.width * 0.09554373,
        size.height * 0.9836957,
        size.width * 0.2133765,
        size.height * 0.9836957);
    path_0.cubicTo(
        size.width * 0.2673353,
        size.height * 0.9836957,
        size.width * 0.3164667,
        size.height * 0.9384391,
        size.width * 0.3540392,
        size.height * 0.8643783);
    path_0.lineTo(size.width * 0.1995745, size.height * 0.8643783);
    path_0.lineTo(size.width * 0.1991039, size.height * 0.7626957);
    path_0.cubicTo(
        size.width * 0.2921745,
        size.height * 0.5813217,
        size.width * 0.3492275,
        size.height * 0.4586652,
        size.width * 0.3492275,
        size.height * 0.3401861);
    path_0.cubicTo(
        size.width * 0.3492275,
        size.height * 0.2961543,
        size.width * 0.3417451,
        size.height * 0.2405900,
        size.width * 0.3080765,
        size.height * 0.2405900);
    path_0.cubicTo(
        size.width * 0.2814137,
        size.height * 0.2405900,
        size.width * 0.2556941,
        size.height * 0.2752100,
        size.width * 0.2603686,
        size.height * 0.4198713);
    path_0.lineTo(size.width * 0.2061235, size.height * 0.4198713);
    path_0.cubicTo(
        size.width * 0.1925578,
        size.height * 0.2521448,
        size.width * 0.2332471,
        size.height * 0.1189865,
        size.width * 0.3108745,
        size.height * 0.1189865);
    path_0.cubicTo(
        size.width * 0.3229098,
        size.height * 0.1189865,
        size.width * 0.3340412,
        size.height * 0.1222074,
        size.width * 0.3442000,
        size.height * 0.1281917);
    path_0.close();

    // ignore: non_constant_identifier_names
    Paint paint_0_fill = Paint()..style = PaintingStyle.fill;
    paint_0_fill.color = Colors.white.withOpacity(1.0);
    canvas.drawPath(path_0, paint_0_fill);

    Path path_1 = Path();
    path_1.moveTo(size.width * 0.6246059, size.height * 0.3577574);
    path_1.lineTo(size.width * 0.6750706, size.height * 0.3577574);
    path_1.cubicTo(
        size.width * 0.6767961,
        size.height * 0.2717130,
        size.width * 0.6638608,
        size.height * 0.1353600,
        size.width * 0.5806176,
        size.height * 0.1353600);
    path_1.cubicTo(
        size.width * 0.5193627,
        size.height * 0.1353600,
        size.width * 0.4749353,
        size.height * 0.2175648,
        size.width * 0.4749353,
        size.height * 0.3887000);
    path_1.cubicTo(
        size.width * 0.4749353,
        size.height * 0.4119074,
        size.width * 0.4753627,
        size.height * 0.5318000,
        size.width * 0.4753627,
        size.height * 0.6062522);
    path_1.cubicTo(
        size.width * 0.4753627,
        size.height * 0.7919174,
        size.width * 0.5327373,
        size.height * 0.8325174,
        size.width * 0.5810471,
        size.height * 0.8325174);
    path_1.cubicTo(
        size.width * 0.6099471,
        size.height * 0.8325174,
        size.width * 0.6517843,
        size.height * 0.8180043,
        size.width * 0.6711922,
        size.height * 0.7706304);
    path_1.lineTo(size.width * 0.6711922, size.height * 0.4747522);
    path_1.lineTo(size.width * 0.5762980, size.height * 0.4747522);
    path_1.lineTo(size.width * 0.5762980, size.height * 0.5820826);
    path_1.lineTo(size.width * 0.6185725, size.height * 0.5820826);
    path_1.lineTo(size.width * 0.6185725, size.height * 0.7126217);
    path_1.cubicTo(
        size.width * 0.5866510,
        size.height * 0.7367870,
        size.width * 0.5284196,
        size.height * 0.7464609,
        size.width * 0.5284196,
        size.height * 0.5820826);
    path_1.lineTo(size.width * 0.5284196, size.height * 0.3887000);
    path_1.cubicTo(
        size.width * 0.5284196,
        size.height * 0.2736339,
        size.width * 0.5521451,
        size.height * 0.2339943,
        size.width * 0.5784510,
        size.height * 0.2339943);
    path_1.cubicTo(
        size.width * 0.6056294,
        size.height * 0.2339943,
        size.width * 0.6246059,
        size.height * 0.2649374,
        size.width * 0.6246059,
        size.height * 0.3577574);
    path_1.close();
    path_1.moveTo(size.width * 0.9999824, size.height * 0.6449217);
    path_1.cubicTo(
        size.width * 1.002137,
        size.height * 0.3800035,
        size.width * 0.8667059,
        size.height * 0.4679783,
        size.width * 0.8679902,
        size.height * 0.3200470);
    path_1.cubicTo(
        size.width * 0.8684196,
        size.height * 0.2678457,
        size.width * 0.8835196,
        size.height * 0.2339943,
        size.width * 0.9081118,
        size.height * 0.2339943);
    path_1.cubicTo(
        size.width * 0.9344216,
        size.height * 0.2339943,
        size.width * 0.9499373,
        size.height * 0.2688048,
        size.width * 0.9495098,
        size.height * 0.3597043);
    path_1.lineTo(size.width * 0.9991137, size.height * 0.3597043);
    path_1.cubicTo(
        size.width * 1.000412,
        size.height * 0.2775013,
        size.width * 0.9917843,
        size.height * 0.1373070,
        size.width * 0.9098373,
        size.height * 0.1373070);
    path_1.cubicTo(
        size.width * 0.8589373,
        size.height * 0.1373070,
        size.width * 0.8171020,
        size.height * 0.1933978,
        size.width * 0.8162333,
        size.height * 0.3181200);
    path_1.cubicTo(
        size.width * 0.8140784,
        size.height * 0.5724000,
        size.width * 0.9503784,
        size.height * 0.4892565,
        size.width * 0.9482255,
        size.height * 0.6430000);
    path_1.cubicTo(
        size.width * 0.9473569,
        size.height * 0.7058435,
        size.width * 0.9270804,
        size.height * 0.7338783,
        size.width * 0.9042255,
        size.height * 0.7338783);
    path_1.cubicTo(
        size.width * 0.8774863,
        size.height * 0.7338783,
        size.width * 0.8559157,
        size.height * 0.6990696,
        size.width * 0.8602333,
        size.height * 0.5946435);
    path_1.lineTo(size.width * 0.8101882, size.height * 0.5946435);
    path_1.cubicTo(
        size.width * 0.8045843,
        size.height * 0.7329174,
        size.width * 0.8317608,
        size.height * 0.8315565,
        size.width * 0.9037961,
        size.height * 0.8315565);
    path_1.cubicTo(
        size.width * 0.9646098,
        size.height * 0.8315565,
        size.width * 0.9991137,
        size.height * 0.7561174,
        size.width * 0.9999824,
        size.height * 0.6449217);
    path_1.close();
    path_1.moveTo(size.width * 0.7721137, size.height * 0.8228304);
    path_1.lineTo(size.width * 0.7212255, size.height * 0.8228304);
    path_1.lineTo(size.width * 0.7212255, size.height * 0.1460035);
    path_1.lineTo(size.width * 0.7721137, size.height * 0.1460035);
    path_1.lineTo(size.width * 0.7721137, size.height * 0.8228304);
    path_1.close();
    path_1.moveTo(size.width * 0.7721137, size.height * 0.8228304);
    path_1.lineTo(size.width * 0.7212255, size.height * 0.8228304);
    path_1.lineTo(size.width * 0.7212255, size.height * 0.1460035);
    path_1.lineTo(size.width * 0.7721137, size.height * 0.1460035);
    path_1.lineTo(size.width * 0.7721137, size.height * 0.8228304);
    path_1.close();

    canvas.drawPath(
      path_1,
      Paint()
        ..style = PaintingStyle.fill
        ..color = Colors.white,
    );
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
