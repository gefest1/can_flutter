import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:tube/ui/admin/page/tariff/molecules/duration_wheel.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';

class DurationExpire extends StatelessWidget {
  // Duration
  final BehaviorSubject<int>? hourSubject;
  final BehaviorSubject<int>? minSubject;

  const DurationExpire({
    required this.hourSubject,
    required this.minSubject,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 146,
      decoration: BoxDecoration(
        color: const Color(0xffF7F7F7),
        borderRadius: BorderRadius.circular(10),
      ),
      child: ClipRect(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.all(10),
              child: H5Text(
                'Длительность одного занятия',
                color: ColorData.colorTextMain,
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Stack(
                  children: [
                    Center(
                      child: Container(
                        height: 41,
                        margin: const EdgeInsets.symmetric(horizontal: 10),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    DurationDirtWheel(
                      hourSubject: hourSubject,
                      minSubject: minSubject,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
