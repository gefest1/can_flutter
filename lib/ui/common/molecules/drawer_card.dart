import 'package:flutter/material.dart';
import 'package:paulonia_cache_image/paulonia_cache_image.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';

class DrawerCard extends StatelessWidget {
  final String? title;
  final String? subTitle;
  final String? networkImage;
  final bool active;
  final VoidCallback? onTap;

  const DrawerCard({
    this.title,
    this.subTitle,
    this.active = false,
    this.onTap,
    this.networkImage,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          constraints: const BoxConstraints(minHeight: 74),
          padding: const EdgeInsets.symmetric(horizontal: 15),
          decoration: BoxDecoration(
            color: active ? const Color(0xffF5F5F5) : Colors.white,
            borderRadius: BorderRadius.circular(6),
            boxShadow: [
              if (!active)
                BoxShadow(
                  offset: const Offset(2, 2),
                  color: Colors.black.withOpacity(0.08),
                  blurRadius: 15,
                ),
            ],
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (networkImage != null) ...[
                Container(
                  height: 48,
                  width: 48,
                  margin: const EdgeInsets.only(top: 15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6),
                    border: active
                        ? Border.all(
                            width: 2,
                            color: ColorData.colorTextMain,
                          )
                        : null,
                  ),
                  alignment: Alignment.center,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(6),
                    child: Image(
                      image: PCacheImage(
                        networkImage!,
                        enableInMemory: true,
                      ),
                      height: 42,
                      width: 42,
                    ),
                  ),
                ),
                const SizedBox(width: 10),
              ],
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: 14),
                    if (title != null)
                      H3Text(
                        title!,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        color: ColorData.colorElementsActive,
                      ),
                    if (subTitle != null)
                      P2Text(
                        subTitle!,
                        color: const Color(0xff9C9C9C),
                      ),
                    // const SizedBox(height: 4),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
