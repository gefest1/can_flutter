import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';

class FastAppBar extends StatefulWidget {
  final String title;
  final IconData? preIcon;
  final IconData? suffIcon;
  final VoidCallback? suffOnTap;
  final VoidCallback? preOnTap;
  final String? suffText;
  final VoidCallback? suffTextOnTap;
  final String? subTitle;

  const FastAppBar({
    required this.title,
    this.subTitle,
    this.preIcon,
    this.suffIcon,
    this.suffOnTap,
    this.preOnTap,
    this.suffText,
    this.suffTextOnTap,
    Key? key,
  }) : super(key: key);

  @override
  State<FastAppBar> createState() => _FastAppBarState();
}

class _FastAppBarState extends State<FastAppBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 52,
      decoration: const BoxDecoration(
        color: Color(0xffF8F8F8),
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(5),
        ),
      ),
      child: Stack(
        children: [
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                H4Text(
                  widget.title,
                  color: ColorData.colorTextMain,
                ),
                if (widget.subTitle != null)
                  Padding(
                    padding: const EdgeInsets.only(top: 2),
                    child: P2Text(
                      widget.subTitle!,
                      color: ColorData.colorTextMain,
                    ),
                  )
              ],
            ),
          ),
          if (widget.preIcon != null)
            Positioned(
              left: 0,
              bottom: 0,
              top: 0,
              child: GestureDetector(
                onTap: widget.preOnTap,
                behavior: HitTestBehavior.opaque,
                child: Padding(
                  padding: const EdgeInsets.all(14),
                  child: Icon(
                    widget.preIcon!,
                    color: const Color(0xff14142B),
                  ),
                ),
              ),
            ),
          if (widget.suffIcon != null)
            Positioned(
              right: 0,
              top: 0,
              bottom: 0,
              child: GestureDetector(
                onTap: widget.suffOnTap,
                child: Padding(
                  padding: const EdgeInsets.all(14),
                  child: Icon(
                    widget.suffIcon!,
                    color: ColorData.colorMainElements,
                    size: 24,
                  ),
                ),
              ),
            ),
          if (widget.suffText != null)
            Positioned(
              bottom: 0,
              top: 0,
              right: 0,
              child: SizeTapAnimation(
                child: GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onTap: widget.suffTextOnTap,
                  child: Padding(
                    padding: const EdgeInsets.all(17.0),
                    child: Text(
                      widget.suffText!,
                      style: const TextStyle(
                        fontSize: H5TextStyle.fontSize,
                        fontWeight: H5TextStyle.fontWeight,
                        height: H5TextStyle.height,
                        color: ColorData.colorMainLink,
                      ),
                    ),
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }
}
