import 'package:flutter/material.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/ui/admin/page/tariff/molecules/duration_wheel.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';

class DurationTitleWheel extends StatefulWidget {
  final BehaviorSubject<Duration>? durationSubject;

  const DurationTitleWheel({
    this.durationSubject,
    super.key,
  });

  @override
  State<DurationTitleWheel> createState() => _DurationTitleWheelState();
}

class _DurationTitleWheelState extends State<DurationTitleWheel> {
  final hourSubject = BehaviorSubject<int>.seeded(0);
  final minSubject = BehaviorSubject<int>.seeded(0);

  late final durationSubject = widget.durationSubject ??
      BehaviorSubject<Duration>.seeded(const Duration());

  @override
  void initState() {
    hourSubject.listen((value) => update());
    minSubject.listen((value) => update());
    super.initState();
  }

  void update() {
    durationSubject.value = Duration(
      hours: hourSubject.value,
      minutes: minSubject.value,
    );
  }

  @override
  void dispose() {
    hourSubject.close();
    minSubject.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 146,
      decoration: BoxDecoration(
        color: const Color(0xffF7F7F7),
        borderRadius: BorderRadius.circular(10),
      ),
      child: ClipRect(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.all(10),
              child: H5Text(
                'Длительность услуги',
                color: ColorData.colorTextMain,
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Stack(
                  children: [
                    Center(
                      child: Container(
                        height: 41,
                        margin: const EdgeInsets.symmetric(horizontal: 10),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    DurationDirtWheel(
                      hourSubject: hourSubject,
                      minSubject: minSubject,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
