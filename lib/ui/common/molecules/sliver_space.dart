import 'package:flutter/material.dart';

class SliverSpace extends StatelessWidget {
  final double? height;
  final double? width;

  const SliverSpace({this.height, this.width, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: SizedBox(
        height: height,
        width: width,
      ),
    );
  }
}
