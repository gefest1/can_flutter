import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';

class CustomButton extends StatelessWidget {
  final VoidCallback? onTap;
  final String? title;
  final Color? color;
  final Widget? child;
  final Color? titleColor;
  final List<BoxShadow>? boxShadow;

  const CustomButton({
    this.onTap,
    this.title,
    this.child,
    this.titleColor = Colors.white,
    this.color = ColorData.colorElementsActive,
    this.boxShadow = const [
      BoxShadow(
        offset: Offset(2, 2),
        blurRadius: 5,
        color: Color(0x26000000),
      ),
    ],
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget? child;
    if (this.child != null) child = this.child;
    if (title != null) {
      child = P0Text(
        title!,
        color: titleColor,
      );
    }
    return SizeTapAnimation(
      onTap: onTap,
      child: Container(
        height: 61,
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(5),
          boxShadow: boxShadow,
        ),
        alignment: Alignment.center,
        child: child,
      ),
    );
  }
}
