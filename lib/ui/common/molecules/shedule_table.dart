import 'package:flutter/material.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/infinite_listview.dart';
import 'package:tube/utils/is_same_day.dart';

class SheduleTableController {
  VoidCallback? prevPage;
  VoidCallback? nextPage;
}

const monthName = [
  "Январь",
  "Февраль",
  "Март",
  "Апрель",
  "Май",
  "Июнь",
  "Июль",
  "Август",
  "Сентябрь",
  "Октябрь",
  "Ноябрь",
  "Декабрь",
];
const weeksShort = [
  "Пон",
  "Вт",
  "Ср",
  "Чт",
  "Пт",
  "Сб",
  "Вс",
];

class SheduleTable extends StatefulWidget {
  final InfiniteScrollController controller;
  final BehaviorSubject<int> lastIndexSubject;
  final SheduleTableController sheduleTableController;
  final EdgeInsets horizontalPadding;
  final void Function(DateTime date)? onTap;
  final Map<String, Set<String>> dateTimeMapWidget;
  final BehaviorSubject<DateTime> dateTimeSubject;

  const SheduleTable({
    required this.controller,
    required this.lastIndexSubject,
    required this.sheduleTableController,
    this.onTap,
    this.horizontalPadding = EdgeInsets.zero,
    this.dateTimeMapWidget = const {},
    required this.dateTimeSubject,
    Key? key,
  }) : super(key: key);

  @override
  State<SheduleTable> createState() => _SheduleTableState();
}

class _SheduleTableState extends State<SheduleTable> {
  final currentYear = DateTime.now().year;
  final currentMonth = DateTime.now().month;
  double maxWidth = 1;

  void _listen() {
    final currentInd = (widget.controller.position.pixels / maxWidth).round();
    if (widget.lastIndexSubject.value != currentInd) {
      widget.lastIndexSubject.value = currentInd;
    }
  }

  void prevPage() {
    widget.dateTimeSubject.value = DateTime(
      widget.dateTimeSubject.value.year,
      widget.dateTimeSubject.value.month - 1,
    );
    widget.controller.animateTo(
      widget.controller.position.pixels - maxWidth,
      duration: const Duration(milliseconds: 300),
      curve: Curves.easeIn,
    );
  }

  void nextPage() {
    widget.dateTimeSubject.value = DateTime(
      widget.dateTimeSubject.value.year,
      widget.dateTimeSubject.value.month + 1,
    );
    widget.controller.animateTo(
      widget.controller.position.pixels + maxWidth,
      duration: const Duration(milliseconds: 300),
      curve: Curves.easeIn,
    );
  }

  @override
  void initState() {
    super.initState();
    widget.controller.addListener(_listen);
    widget.sheduleTableController.nextPage = nextPage;
    widget.sheduleTableController.prevPage = prevPage;
  }

  @override
  void dispose() {
    widget.controller.removeListener(_listen);
    super.dispose();
  }

  DateTime get parentDateTime => widget.dateTimeSubject.value;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: widget.horizontalPadding,
          child: Row(
            children: List.generate(
              weeksShort.length,
              (index) => Expanded(
                child: Center(
                  child: H6Text(
                    weeksShort[index],
                    color: ColorData.colortTextSecondary,
                  ),
                ),
              ),
            ),
          ),
        ),
        const SizedBox(height: 20),
        StreamBuilder(
          stream: widget.dateTimeSubject,
          builder: (context, _) {
            return SizedBox(
              height: 252,
              child: LayoutBuilder(
                builder: (context, box) {
                  maxWidth = box.maxWidth;
                  return InfiniteListView.builder(
                    controller: widget.controller,
                    physics: const PageScrollPhysics(),
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (ctx, index) {
                      final currentStartTime =
                          DateTime(currentYear, currentMonth + index);
                      final currentEndTime =
                          DateTime(currentYear, currentMonth + index + 1);
                      return SizedBox(
                        height: 252,
                        width: box.maxWidth,
                        child: Padding(
                          padding: widget.horizontalPadding,
                          child: Column(
                            children: List.generate(
                              6,
                              (index) {
                                return Row(
                                  children: List.generate(
                                    7,
                                    (subIndex) {
                                      final currentDay = 1 +
                                          index * 7 +
                                          subIndex -
                                          ((currentStartTime.weekday + 6) % 7);
                                      final lastDay = currentEndTime
                                          .difference(currentStartTime)
                                          .inDays;

                                      if (!(currentDay > 0 &&
                                          currentDay <= lastDay)) {
                                        return const Expanded(
                                          child: SizedBox(height: 42),
                                        );
                                      }
                                      final subDay = DateTime(
                                        currentStartTime.year,
                                        currentStartTime.month,
                                        currentDay,
                                      );
                                      final subWidget =
                                          widget.dateTimeMapWidget[
                                              subDay.toIso8601String()];
                                      final bool choosen =
                                          isSameDay(parentDateTime, subDay);
                                      parentDateTime.year == subDay.year &&
                                          parentDateTime.month ==
                                              subDay.month &&
                                          parentDateTime.day == subDay.day;
                                      return Expanded(
                                        child: GestureDetector(
                                          behavior: HitTestBehavior.opaque,
                                          onTap: () => widget.onTap?.call(
                                            DateTime(
                                              currentStartTime.year,
                                              currentStartTime.month,
                                              currentDay,
                                            ),
                                          ),
                                          child: SizedBox(
                                            height: 42,
                                            child: Center(
                                              child: Container(
                                                height: 42,
                                                width: 32,
                                                constraints:
                                                    const BoxConstraints(
                                                  maxWidth: 32,
                                                ),
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  color: choosen
                                                      ? ColorData.colorMainLink
                                                      : null,
                                                ),
                                                child: Column(
                                                  children: [
                                                    const SizedBox(height: 5),
                                                    H4Text(
                                                      currentDay.toString(),
                                                      color: choosen
                                                          ? Colors.white
                                                          : ColorData
                                                              .colorMainBlack,
                                                    ),
                                                    if (subWidget != null)
                                                      Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .only(top: 2),
                                                        child: Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          children: [
                                                            // CustomPaint(
                                                            //   painter:
                                                            //       ServiceIconCustomPainter(),
                                                            //   size: Size(8, 8),
                                                            // ),
                                                            // SizedBox(width: 2),
                                                            if (subWidget
                                                                .contains(
                                                                    'booking'))
                                                              Icon(
                                                                Icons
                                                                    .fiber_smart_record,
                                                                size: 8,
                                                                color: choosen
                                                                    ? Colors
                                                                        .white
                                                                    : ColorData
                                                                        .abonement,
                                                              ),
                                                            // SizedBox(width: 2),
                                                            // Icon(
                                                            //   Icons.today,
                                                            //   size: 8,
                                                            //   color:
                                                            //       ColorData.event,
                                                            // ),
                                                          ],
                                                        ),
                                                      ),
                                                    //  const [
                                                    //   CustomPaint(
                                                    //     painter:
                                                    //         ServiceIconCustomPainter(),
                                                    //     size: Size(8, 8),
                                                    //   ),
                                                    //   SizedBox(width: 2),
                                                    //   Icon(
                                                    //     Icons.fiber_smart_record,
                                                    //     size: 8,
                                                    //     color: ColorData.abonement,
                                                    //   ),
                                                    //   SizedBox(width: 2),
                                                    //   Icon(
                                                    //     Icons.today,
                                                    //     size: 8,
                                                    //     color: ColorData.event,
                                                    //   ),
                                                    // ],
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                );
                              },
                            ),
                          ),
                        ),
                      );
                    },
                  );
                },
              ),
            );
          },
        ),
      ],
    );
  }
}
