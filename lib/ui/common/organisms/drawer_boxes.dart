import 'package:flutter/material.dart';
import 'package:tube/logic/blocs/participance/model/participance.model.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/drawer_card.dart';
import 'dart:math' as math;

import 'package:tube/ui/common/molecules/invite_card.dart';
import 'package:tube/utils/colors.dart';

class DrawerBoxes extends StatelessWidget {
  final List<Participance> participances;
  final void Function(int)? onTap;
  final void Function(Participance)? acceptTap;
  final void Function(Participance)? declineTap;
  final int active;

  const DrawerBoxes({
    required this.participances,
    required this.active,
    this.onTap,
    this.acceptTap,
    this.declineTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final invited =
        participances.where((element) => !(element.accepted ?? false)).toList();
    final participancesEnt =
        participances.where((element) => element.accepted ?? false).toList();
    return CustomScrollView(
      physics: const BouncingScrollPhysics(),
      slivers: [
        if (invited.isNotEmpty)
          const SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.only(left: 20, right: 20, bottom: 5, top: 20),
              child: H5Text(
                'Приглашения',
                color: ColorData.colortTextSecondary,
              ),
            ),
          ),
        SliverPadding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          sliver: SliverList(
            delegate: SliverChildBuilderDelegate(
              (ctx, index) {
                final Widget widget;
                final int itemIndex = index ~/ 2;
                if (index.isEven) {
                  widget = InviteCard(
                    acceptTap: () => acceptTap?.call(invited[itemIndex]),
                    declineTap: () => declineTap?.call(invited[itemIndex]),
                    title: invited[itemIndex].club?.fullName,
                    subTitle: invited[itemIndex].status?.toStrValue(),
                    networkImage: invited[itemIndex].club?.photoUrl?.m,
                  );
                } else {
                  widget = const SizedBox(height: 10);
                }
                return widget;
              },
              childCount: math.max(0, invited.length * 2 - 1),
            ),
          ),
        ),
        if (participancesEnt.isNotEmpty)
          const SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.only(left: 20, right: 20, bottom: 5, top: 30),
              child: H5Text(
                'Уже состоите',
                color: ColorData.colortTextSecondary,
              ),
            ),
          ),
        SliverPadding(
          padding: const EdgeInsets.only(
            right: 20,
            left: 20,
            top: 10,
            bottom: 40,
          ),
          sliver: SliverList(
            delegate: SliverChildBuilderDelegate(
              (ctx, index) {
                final Widget widget;
                final int itemIndex = index ~/ 2;

                if (index.isEven) {
                  widget = DrawerCard(
                    title: participancesEnt[itemIndex].club?.fullName,
                    subTitle: participancesEnt[itemIndex].status?.toStrValue(),
                    networkImage: participancesEnt[itemIndex].club?.photoUrl?.m,
                    onTap: () => onTap?.call(itemIndex),
                    active: active == itemIndex,
                  );
                } else {
                  widget = const SizedBox(height: 10);
                }
                return widget;
              },
              childCount: math.max(0, participancesEnt.length * 2 - 1),
            ),
          ),
        ),
        const SliverToBoxAdapter(child: SizedBox(height: 40)),
      ],
    );
    // return ListView.separated(
    //   padding: const EdgeInsets.only(
    //     left: 20,
    //     right: 20,
    //     top: 10,
    //     bottom: 40,
    //   ),
    //   itemBuilder: (_, __) {
    //     if (!(participances[__].accepted ?? false)) {
    //       return const SizedBox();
    //     }
    //     return DrawerCard(
    //       title: participances[__].club?.fullName,
    //       subTitle: participances[__].status,
    //       networkImage: participances[__].club?.photoUrl?.m,
    //       onTap: () => onTap?.call(__),
    //       active: active == __,
    //     );
    //   },
    //   separatorBuilder: (_, __) {
    //     if (!(participances[__].accepted ?? false)) {
    //       return const SizedBox();
    //     }
    //     return const SizedBox(height: 10);
    //   },
    //   itemCount: participances.length,
    // );
  }
}
