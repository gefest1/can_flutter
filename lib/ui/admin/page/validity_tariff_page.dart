import 'package:flutter/material.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/ui/admin/page/tariff/molecules/duration_wheel.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/expire_days.dart';
import 'package:tube/utils/colors.dart';

class ValidityTariffPage extends StatefulWidget {
  // Expire
  final BehaviorSubject<int>? yearSubject;
  final BehaviorSubject<int>? monthSubject;
  final BehaviorSubject<int>? daySubject;

  // Duration
  final BehaviorSubject<int>? hourSubject;
  final BehaviorSubject<int>? minSubject;

  const ValidityTariffPage({
    this.yearSubject,
    this.monthSubject,
    this.daySubject,
    //
    this.hourSubject,
    this.minSubject,
    Key? key,
  }) : super(key: key);

  @override
  State<ValidityTariffPage> createState() => _ValidityTariffPageState();
}

String getDoubleStr(String str) {
  final toReturn = "00$str";
  return toReturn.substring(toReturn.length - 2, toReturn.length);
}

class _ValidityTariffPageState extends State<ValidityTariffPage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        ExpireDays(
          daySubject: widget.daySubject,
          monthSubject: widget.monthSubject,
          yearSubject: widget.yearSubject,
        ),
        const SizedBox(height: 10),
        Container(
          height: 146,
          decoration: BoxDecoration(
            color: const Color(0xffF7F7F7),
            borderRadius: BorderRadius.circular(10),
          ),
          child: ClipRect(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.all(10),
                  child: H5Text(
                    'Длительность одного занятия',
                    color: ColorData.colorTextMain,
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 10),
                    child: Stack(
                      children: [
                        Center(
                          child: Container(
                            height: 41,
                            margin: const EdgeInsets.symmetric(horizontal: 10),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                        ),
                        DurationDirtWheel(
                          hourSubject: widget.hourSubject,
                          minSubject: widget.minSubject,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
