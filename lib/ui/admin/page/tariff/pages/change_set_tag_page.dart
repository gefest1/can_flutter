import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tube/logic/blocs/participance/participance_bloc.dart';
import 'package:tube/ui/admin/page/tariff/add_trainer_page.dart';
import 'package:tube/ui/admin/page/tariff/pages/edit_tag_page.dart';
import 'package:tube/ui/common/atoms/tag_container.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/load_popup_route.dart';

class ChangeSetTagPage extends StatefulWidget {
  const ChangeSetTagPage({super.key});

  @override
  State<ChangeSetTagPage> createState() => _ChangeSetTagPageState();
}

class _ChangeSetTagPageState extends State<ChangeSetTagPage> {
  List<String> tags = [];
  late final partBloc = BlocProvider.of<ParticipanceBloc>(
    context,
    listen: false,
  );

  final tagController = TextEditingController();
  void openNext() async {
    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (_) => const AddTrainerPage(),
      ),
    );
  }

  void submit([String? p0]) async {
    if (tagController.text.isEmpty ||
        tags.contains(tagController.text.trim())) {
      return;
    }

    final completer = Completer();
    Navigator.push(context, LoadPopupRoute(completer: completer));
    partBloc.add(
      AddTagParticipanceEvent(
        clubId: MainDrawerState.clubId!,
        tagName: tagController.text,
        completer: completer,
      ),
    );
    await completer.future;

    tagController.clear();
  }

  void editOrRemove(int index) async {
    final res = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => EditTagPage(
          title: tags[index],
        ),
      ),
    );
    if (res == null) return;
    final completer = Completer();
    if (!mounted) return;
    Navigator.push(context, LoadPopupRoute(completer: completer));
    try {
      if (res == '') {
        partBloc.add(
          RemoveTagParticipanceEvent(
            clubId: MainDrawerState.clubId!,
            tagName: tags[index],
            completer: completer,
          ),
        );
        return;
      }
      if (res is String) {
        partBloc.add(
          EditTagParticipanceEvent(
            oldTagName: tags[index],
            clubId: MainDrawerState.clubId!,
            tagName: res,
            completer: completer,
          ),
        );
        return;
      }
      completer.complete();
    } finally {
      await completer.future;
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final f = FocusScope.of(context);

        if (f.hasFocus || f.hasPrimaryFocus) {
          f.unfocus();
        }
      },
      child: Scaffold(
        appBar: AppBar(
          actions: [
            ValueListenableBuilder(
              valueListenable: tagController,
              builder: (context, value, child) {
                if (tagController.text.isEmpty) return const SizedBox();

                return SizeTapAnimation(
                  onTap: submit,
                  child: const Padding(
                    padding: EdgeInsets.all(18),
                    child: P1Text(
                      'Готово',
                      color: ColorData.colorMainLink,
                    ),
                  ),
                );
              },
            ),
          ],
          centerTitle: true,
          title: const H4Text(
            'Настройка атрибутов',
            color: Colors.white,
          ),
        ),
        backgroundColor: Colors.white,
        body: CustomScrollView(
          slivers: [
            StreamBuilder(
              stream: MainDrawerState.currentParticipanceValue,
              builder: (context, snapshot) {
                final tags = MainDrawerState
                        .currentParticipanceValue.value?.club?.tags ??
                    <String>[];
                this.tags = tags;

                return SliverToBoxAdapter(
                  child: Column(
                    children: [
                      if (tags.isEmpty)
                        const SizedBox(
                          height: 40,
                        ),
                      if (tags.isNotEmpty)
                        Padding(
                          padding: const EdgeInsets.all(20),
                          child: Column(
                            children: List.generate(
                              tags.length * 2 - 1,
                              (index) => index.isOdd
                                  ? const SizedBox(height: 10)
                                  : TagContainer(
                                      title: tags[index ~/ 2],
                                      onTap: () => editOrRemove(index ~/ 2),
                                    ),
                            ),
                          ),
                        ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: CustomTextField(
                          maxLines: 1,
                          keyboardType: TextInputType.text,
                          onSubmitted: submit,
                          controller: tagController,
                          hintText: 'Новый атрибут',
                          title: 'Введите название нового атрибута',
                        ),
                      ),
                      const SizedBox(height: 100),
                    ],
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
