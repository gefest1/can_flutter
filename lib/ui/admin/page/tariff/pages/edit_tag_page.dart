import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/custom_button.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';

class EditTagPage extends StatefulWidget {
  final String title;

  const EditTagPage({
    required this.title,
    super.key,
  });

  @override
  State<EditTagPage> createState() => _EditTagPageState();
}

class _EditTagPageState extends State<EditTagPage> {
  late final controller = TextEditingController(text: widget.title);

  void pop() => Navigator.pop(context, controller.text.trim());

  void remove() => Navigator.pop(context, '');

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        final f = FocusScope.of(context);

        if (f.hasFocus || f.hasPrimaryFocus) {
          f.unfocus();
        }
      },
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          actions: [
            Center(
              child: ValueListenableBuilder(
                valueListenable: controller,
                builder: (context, value, child) {
                  if (controller.text.isEmpty) return const SizedBox();
                  return SizeTapAnimation(
                    onTap: pop,
                    child: const Padding(
                      padding: EdgeInsets.all(18),
                      child: P1Text(
                        'Готово',
                        color: ColorData.colorMainLink,
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
          title: H4Text(
            widget.title,
            color: Colors.white,
          ),
        ),
        backgroundColor: Colors.white,
        body: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  children: [
                    const SizedBox(height: 20),
                    CustomTextField(
                      hintText: '',
                      maxLines: 1,
                      onSubmitted: (p0) =>
                          Navigator.pop(context, controller.text.trim()),
                      title: 'Название атрибута',
                      controller: controller,
                    ),
                    const SizedBox(height: 40),
                    CustomButton(
                      color: const Color(0xffF5F5F5),
                      titleColor: const Color(0xffFF2F2F),
                      title: 'Удалить атрибут',
                      boxShadow: const [],
                      onTap: remove,
                    ),
                    const SizedBox(height: 100),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
