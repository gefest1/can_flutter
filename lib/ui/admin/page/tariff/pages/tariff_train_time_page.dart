import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:paulonia_cache_image/paulonia_cache_image.dart';
import 'package:tube/logic/blocs/tariff/model/tariff.model.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';
import 'package:tube/utils/get_date_time_object_id.dart';
import 'package:url_launcher/url_launcher.dart';

class TariffTrainTimePage extends StatelessWidget {
  final String title;
  final DateTime dateTime;
  final List<TariffSingle> tariffList;

  const TariffTrainTimePage({
    required this.title,
    required this.dateTime,
    required this.tariffList,
    Key? key,
  }) : super(key: key);

  String countStr(TariffSingle tariff) {
    // final _list = (_tariff.trainTimes ?? []);

    return (tariff.trainTimes ?? [])
        .where((element) =>
            element.come == false &&
            DateTime.now().compareTo(element.date!) == -1)
        .length
        .toString();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          iconSize: 33,
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        centerTitle: true,
        title: Column(
          children: [
            H4Text(title, color: Colors.white),
            const SizedBox(height: 2),
            P1Text(
              DateFormat("HH:mm dd.MM.yyyy").format(dateTime),
              color: Colors.white,
            ),
          ],
        ),
      ),
      body: ListView.builder(
        padding:
            const EdgeInsets.only(left: 20, right: 20, bottom: 40, top: 25),
        itemCount: tariffList.length,
        itemBuilder: (ctx, index) {
          final tarif = tariffList[index];
          final photoUrl = tarif.user!.photoUrl;
          return Column(
            children: [
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 10.0),
                    child: CircleAvatar(
                      radius: 20,
                      backgroundColor: ColorData.color3Percent,
                      child: Stack(
                        children: [
                          const Center(
                            child: Icon(
                              Icons.face,
                              color: ColorData.colorElementsActive,
                            ),
                          ),
                          if (photoUrl?.m != null)
                            ClipOval(
                              child: Image(
                                image: PCacheImage(
                                  photoUrl!.m!,
                                  enableInMemory: true,
                                ),
                                height: 40,
                                width: 40,
                              ),
                            )
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(height: 15),
                        H4Text(
                          tarif.user!.fullName ?? tarif.user!.email ?? '',
                          color: ColorData.colorTextMain,
                        ),
                        // getDateTimefromObjectId(objectId)
                        RichText(
                          text: TextSpan(
                            style: const TextStyle(fontFamily: 'InterTight'),
                            children: [
                              const TextSpan(
                                // Абонемент куплен: 15.10.2022
                                text: 'Абонемент куплен: ',
                                style: TextStyle(
                                  fontSize: P2TextStyle.fontSize,
                                  fontWeight: P2TextStyle.fontWeight,
                                  height: P2TextStyle.height,
                                  color: ColorData.colorTextMain,
                                ),
                              ),
                              TextSpan(
                                text: DateFormat("dd.MM.yyyy").format(
                                  getDateTimefromObjectId(tarif.id!),
                                  // _tarif.trainTimes!.last.endTime!,
                                ),
                                style: const TextStyle(
                                  fontSize: P1TextStyle.fontSize,
                                  fontWeight: FontWeight.w700,
                                  height: P1TextStyle.height,
                                  color: ColorData.colorTextMain,
                                ),
                              ),
                            ],
                          ),
                        ),
                        RichText(
                          text: TextSpan(
                            style: const TextStyle(fontFamily: 'InterTight'),
                            children: [
                              const TextSpan(
                                text: 'Действует до: ',
                                style: TextStyle(
                                  fontSize: P2TextStyle.fontSize,
                                  fontWeight: P2TextStyle.fontWeight,
                                  height: P2TextStyle.height,
                                  color: ColorData.colorTextMain,
                                ),
                              ),
                              TextSpan(
                                text: DateFormat("dd.MM.yyyy")
                                    .format(tarif.trainTimes!.last.endTime!),
                                style: const TextStyle(
                                  fontSize: P1TextStyle.fontSize,
                                  fontWeight: FontWeight.w700,
                                  height: P1TextStyle.height,
                                  color: ColorData.colorTextMain,
                                ),
                              ),
                            ],
                          ),
                        ),
                        RichText(
                          text: TextSpan(
                            style: const TextStyle(fontFamily: 'InterTight'),
                            children: [
                              const TextSpan(
                                text: 'Осталось занятий: ',
                                style: TextStyle(
                                  fontSize: P2TextStyle.fontSize,
                                  fontWeight: P2TextStyle.fontWeight,
                                  height: P2TextStyle.height,
                                  color: ColorData.colorTextMain,
                                ),
                              ),
                              TextSpan(
                                text:
                                    "${countStr(tarif)}/${tarif.trainTimes!.length}",
                                style: const TextStyle(
                                  fontSize: P1TextStyle.fontSize,
                                  fontWeight: FontWeight.w700,
                                  height: P1TextStyle.height,
                                  color: ColorData.colorTextMain,
                                ),
                              ),
                            ],
                          ),
                        ),
                        if (tarif.user?.phoneNumber != null)
                          GestureDetector(
                            onTap: () {
                              launchUrl(
                                Uri.parse('tel:${tarif.user!.phoneNumber!}'),
                              );
                            },
                            behavior: HitTestBehavior.opaque,
                            child: IgnorePointer(
                              child: RichText(
                                text: TextSpan(
                                  style:
                                      const TextStyle(fontFamily: 'InterTight'),
                                  children: [
                                    const TextSpan(
                                      text: 'Телефон: ',
                                      style: TextStyle(
                                        fontSize: P2TextStyle.fontSize,
                                        fontWeight: P2TextStyle.fontWeight,
                                        height: P2TextStyle.height,
                                        color: ColorData.colorTextMain,
                                      ),
                                    ),
                                    TextSpan(
                                      text: tarif.user!.phoneNumber!,
                                      style: const TextStyle(
                                        fontSize: P1TextStyle.fontSize,
                                        fontWeight: FontWeight.w700,
                                        height: P1TextStyle.height,
                                        color: ColorData.colorMainLink,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                      ],
                    ),
                  )
                ],
              ),
              const SizedBox(height: 15),
              const Divider(
                height: 0,
                color: Color(0xffEBEBEB),
                thickness: 0.5,
              )
            ],
          );
        },
        // children: ,
      ),
    );
  }
}
