import 'package:flutter/material.dart';
import 'package:tube/ui/admin/page/tariff/molecules/shedule_sccroll.dart';

class SheduleExpandPage extends StatefulWidget {
  const SheduleExpandPage({Key? key}) : super(key: key);

  @override
  State<SheduleExpandPage> createState() => _SheduleExpandPageState();
}

class _SheduleExpandPageState extends State<SheduleExpandPage> {
  @override
  Widget build(BuildContext context) {
    return const Material(
      color: Colors.white,
      child: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Center(
              child: Padding(
                padding: EdgeInsets.only(top: 100),
                child: SizedBox(
                  height: 330,
                  width: 335,
                  child: SheduleScroll(),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
