import 'package:flutter/material.dart';
import 'package:paulonia_cache_image/paulonia_cache_image.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/shadow.dart';

class StackImageCard extends StatelessWidget {
  final VoidCallback? onTap;
  final String? title;
  final List<String>? images;
  final int? numberTitle;
  final bool active;

  const StackImageCard({
    this.onTap,
    this.title,
    this.images,
    this.numberTitle,
    this.active = false,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.all(15),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: blocksMains,
        ),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (title != null)
                    H4Text(
                      title!,
                      color: ColorData.colorTextMain,
                    ),
                  const SizedBox(height: 20),
                  Row(
                    children: [
                      Stack(
                        children: [
                          if (images != null && images!.length >= 2)
                            Padding(
                              padding: const EdgeInsets.only(left: 27.2 * 2),
                              child: Container(
                                decoration: const BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.white,
                                ),
                                height: 50,
                                width: 50,
                                alignment: Alignment.center,
                                child: CircleAvatar(
                                  radius: 23,
                                  backgroundColor: ColorData.color3Percent,
                                  foregroundColor: Colors.transparent,
                                  foregroundImage: PCacheImage(
                                    images![1],
                                    enableInMemory: true,
                                  ),
                                  child: const Icon(
                                    Icons.perm_contact_cal,
                                    color: ColorData.colorElementsActive,
                                  ),
                                ),
                              ),
                            ),
                          if (images != null && images!.isNotEmpty)
                            Padding(
                              padding: const EdgeInsets.only(left: 27.2),
                              child: Container(
                                decoration: const BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.white,
                                ),
                                height: 50,
                                width: 50,
                                alignment: Alignment.center,
                                child: CircleAvatar(
                                  radius: 23,
                                  backgroundColor: ColorData.color3Percent,
                                  foregroundColor: Colors.transparent,
                                  foregroundImage: PCacheImage(
                                    images![0],
                                    enableInMemory: true,
                                  ),
                                  child: const Icon(
                                    Icons.perm_contact_cal,
                                    color: ColorData.colorElementsActive,
                                  ),
                                ),
                              ),
                            ),
                          Container(
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.white,
                              border: active
                                  ? Border.all(
                                      width: 2,
                                      color: ColorData.colorMainLink,
                                    )
                                  : null,
                            ),
                            height: 50,
                            width: 50,
                            alignment: Alignment.center,
                            child: const CircleAvatar(
                              radius: 23,
                              backgroundColor: ColorData.color3Percent,
                              child: Icon(
                                Icons.perm_contact_cal,
                                color: ColorData.colorElementsActive,
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(width: 10),
                      H4Text(
                        (numberTitle ?? 0).toString(),
                        color: ColorData.colortTextSecondary,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const Padding(
              padding: EdgeInsets.only(left: 10),
              child: Icon(
                Icons.navigate_next,
                size: 24,
                color: ColorData.colorTextMain,
              ),
            )
          ],
        ),
      ),
    );
  }
}
