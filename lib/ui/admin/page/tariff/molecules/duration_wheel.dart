import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/ui/admin/atoms/wheel_label.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';

enum _SubjectType { hour, min }

class DurationDirtWheel extends StatefulWidget {
  final BehaviorSubject<int>? hourSubject;
  final BehaviorSubject<int>? minSubject;

  const DurationDirtWheel({
    this.hourSubject,
    this.minSubject,
    Key? key,
  }) : super(key: key);

  @override
  State<DurationDirtWheel> createState() => _DurationDirtWheelState();
}

class _DurationDirtWheelState extends State<DurationDirtWheel> {
  int hourLastHapticIndex = 0;
  int minLastHapticIndex = 0;

  late FixedExtentScrollController hourController;
  late FixedExtentScrollController minController;

  @override
  void initState() {
    if (widget.hourSubject != null) {
      hourLastHapticIndex = widget.hourSubject!.value;
      hourController =
          FixedExtentScrollController(initialItem: hourLastHapticIndex);
    }
    if (widget.minSubject != null) {
      minLastHapticIndex = widget.minSubject!.value;
      minController =
          FixedExtentScrollController(initialItem: minLastHapticIndex);
    }
    super.initState();
  }

  void _handleSelectedItemChanged(int index, _SubjectType subject, int inter) {
    if (hourLastHapticIndex != index && subject == _SubjectType.hour) {
      hourLastHapticIndex = index;
      widget.hourSubject!.value = index % inter;
      HapticFeedback.selectionClick();
    }
    if (minLastHapticIndex != index && subject == _SubjectType.min) {
      minLastHapticIndex = index;
      widget.minSubject!.value = index % inter;
      HapticFeedback.selectionClick();
    }
  }

  Widget _wheel(
      int inter, _SubjectType subject, FixedExtentScrollController controller) {
    return ListWheelScrollView.useDelegate(
      itemExtent: 41,
      clipBehavior: Clip.none,
      overAndUnderCenterOpacity: 0.3,
      diameterRatio: 1.2,
      controller: controller,
      onSelectedItemChanged: (index) =>
          _handleSelectedItemChanged(index, subject, inter),
      squeeze: 1.4,
      physics: const FixedExtentScrollPhysics(),
      childDelegate: ListWheelChildBuilderDelegate(builder: (ctx, index) {
        final cur = index % inter;
        return SizedBox(
          height: 41,
          child: Center(
            child: H3Text(
              cur.toString(),
              color: ColorData.colorElementsActive,
            ),
          ),
        );
      }),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Stack(
            children: [
              _wheel(
                24,
                _SubjectType.hour,
                hourController,
              ),
              const WheelLabel(data: 'ч'),
            ],
          ),
        ),
        Expanded(
          child: Stack(
            children: [
              _wheel(60, _SubjectType.min, minController),
              const WheelLabel(data: 'мин')
            ],
          ),
        ),
      ],
    );
  }
}
