import 'package:flutter/material.dart';
import 'package:paulonia_cache_image/paulonia_cache_image.dart';
import 'package:tube/logic/model/photourl.dart';
import 'package:tube/utils/colors.dart';

class CustomAvatar extends StatelessWidget {
  final PhotoUrl? photoUrl;

  const CustomAvatar({required this.photoUrl, super.key});

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      radius: 24,
      backgroundColor: ColorData.color3Percent,
      backgroundImage: photoUrl?.thumbnail == null
          ? null
          : PCacheImage(
              photoUrl!.thumbnail!,
              enableInMemory: true,
            ),
      child: Stack(
        children: [
          const Center(
            child: Icon(
              Icons.perm_contact_calendar,
              size: 32,
              color: ColorData.colorElementsActive,
            ),
          ),
          if (photoUrl?.m != null)
            ClipOval(
              child: Image(
                image: PCacheImage(
                  photoUrl!.m!,
                  enableInMemory: true,
                ),
              ),
            ),
        ],
      ),
    );
  }
}
