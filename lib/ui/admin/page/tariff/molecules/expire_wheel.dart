import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/ui/admin/atoms/wheel_label.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';

enum _SubjectType { year, month, day }

class ExpireWheel extends StatefulWidget {
  final BehaviorSubject<int>? yearSubject;
  final BehaviorSubject<int>? monthSubject;
  final BehaviorSubject<int>? daySubject;

  const ExpireWheel({
    this.yearSubject,
    this.monthSubject,
    this.daySubject,
    Key? key,
  }) : super(key: key);

  @override
  State<ExpireWheel> createState() => _ExpireWheelState();
}

class _ExpireWheelState extends State<ExpireWheel> {
  int yearLastHapticIndex = 0;
  int monthLastHapticIndex = 0;
  int dayLastHapticIndex = 0;

  late FixedExtentScrollController yearController;
  late FixedExtentScrollController monthController;
  late FixedExtentScrollController dayController;

  @override
  void initState() {
    if (widget.yearSubject != null) {
      yearLastHapticIndex = widget.yearSubject!.value;
      yearController =
          FixedExtentScrollController(initialItem: yearLastHapticIndex);
    }
    if (widget.monthSubject != null) {
      monthLastHapticIndex = widget.monthSubject!.value;
      monthController =
          FixedExtentScrollController(initialItem: monthLastHapticIndex);
    }
    if (widget.daySubject != null) {
      dayLastHapticIndex = widget.daySubject!.value;
      dayController =
          FixedExtentScrollController(initialItem: dayLastHapticIndex);
    }

    super.initState();
  }

  void _handleSelectedItemChanged(int index, _SubjectType subject, int inter) {
    if (dayLastHapticIndex != index && subject == _SubjectType.day) {
      dayLastHapticIndex = index;
      widget.daySubject!.value = index % inter;
      HapticFeedback.selectionClick();
    }
    if (yearLastHapticIndex != index && subject == _SubjectType.month) {
      yearLastHapticIndex = index;
      widget.monthSubject!.value = index % inter;
      HapticFeedback.selectionClick();
    }
    if (monthLastHapticIndex != index && subject == _SubjectType.year) {
      monthLastHapticIndex = index;
      widget.yearSubject!.value = index % inter;
      HapticFeedback.selectionClick();
    }
  }

  Widget _wheel(
      int inter, _SubjectType subject, FixedExtentScrollController controller) {
    return ListWheelScrollView.useDelegate(
      itemExtent: 41,
      clipBehavior: Clip.none,
      overAndUnderCenterOpacity: 0.3,
      diameterRatio: 1.2,
      onSelectedItemChanged: (index) =>
          _handleSelectedItemChanged(index, subject, inter),
      squeeze: 1.4,
      controller: controller,
      physics: const FixedExtentScrollPhysics(),
      childDelegate: ListWheelChildBuilderDelegate(builder: (ctx, index) {
        final cur = index % inter;
        return SizedBox(
          height: 41,
          child: Center(
            child: H3Text(
              cur.toString(),
              color: ColorData.colorElementsActive,
            ),
          ),
        );
      }),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Stack(
            children: [
              _wheel(10, _SubjectType.year, yearController),
              const WheelLabel(data: 'лет'),
            ],
          ),
        ),
        Expanded(
          child: Stack(
            children: [
              _wheel(13, _SubjectType.month, monthController),
              const WheelLabel(data: 'мес')
            ],
          ),
        ),
        Expanded(
          child: Stack(
            children: [
              _wheel(32, _SubjectType.day, dayController),
              const WheelLabel(data: 'дн'),
            ],
          ),
        ),
      ],
    );
  }
}
