import 'package:flutter/material.dart';
import 'package:paulonia_cache_image/paulonia_cache_image.dart';
import 'package:tube/ui/common/atoms/animated_switch.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';

class TrainerShortTile extends StatelessWidget {
  final VoidCallback? onTap;
  final String? title;
  final String? label;
  final String? photoUrl;
  final bool active;

  const TrainerShortTile({
    this.title,
    this.onTap,
    this.label,
    this.photoUrl,
    this.active = false,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 15),
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: CircleAvatar(
                    radius: 24,
                    backgroundColor: ColorData.color3Percent,
                    backgroundImage: photoUrl == null
                        ? null
                        : PCacheImage(
                            photoUrl!,
                            enableInMemory: true,
                          ),
                    child: Stack(
                      children: [
                        const Center(
                          child: Icon(
                            Icons.perm_contact_calendar,
                            size: 32,
                            color: ColorData.colorElementsActive,
                          ),
                        ),
                        if (photoUrl != null)
                          ClipOval(
                            child: Image(
                              image: PCacheImage(
                                photoUrl!,
                                enableInMemory: true,
                              ),
                            ),
                          ),
                        AnimatedSwitch(
                          active: active,
                          child: Container(
                            decoration: const BoxDecoration(
                              color: Color(0xcc00284e),
                              shape: BoxShape.circle,
                            ),
                            alignment: Alignment.center,
                            child: const Icon(
                              Icons.check,
                              size: 38,
                              color: ColorData.colorMainLink,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      if (title != null)
                        H4Text(
                          title!,
                          color: ColorData.colorTextMain,
                        ),
                      if (label != null)
                        Padding(
                          padding: const EdgeInsets.only(top: 2),
                          child: P2Text(
                            label!,
                            color: ColorData.colortTextSecondary,
                          ),
                        )
                    ],
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.only(left: 10),
                  child: Icon(
                    Icons.chevron_right,
                    color: ColorData.colorElementsSecondary,
                    size: 24,
                  ),
                )
              ],
            ),
          ),
          const Divider(
            color: Color(0xffA5A5A5),
            height: 0,
            thickness: 0.5,
          )
        ],
      ),
    );
  }
}
