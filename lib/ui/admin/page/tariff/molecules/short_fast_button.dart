import 'package:flutter/material.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';

class ShortFastButton extends StatelessWidget {
  final VoidCallback? onTap;
  final Widget? child;

  const ShortFastButton({
    this.child,
    this.onTap,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      child: Container(
        decoration: const BoxDecoration(
          color: Color(0xffF5F5F5),
          borderRadius: BorderRadius.all(Radius.circular(5)),
        ),
        alignment: Alignment.center,
        height: 45,
        child: child,
      ),
    );
  }
}
