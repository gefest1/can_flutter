import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';

class DurationLabel extends StatelessWidget {
  final Duration duration;

  const DurationLabel({
    required this.duration,
    Key? key,
  }) : super(key: key);

  String _getStr() {
    List<String> str = [];
    if (duration.inHours != 0) {
      str.add("${duration.inHours} ч");
    }
    if (duration.inMinutes.remainder(60) != 0) {
      str.add("${duration.inMinutes.remainder(60)} мин");
    }
    str = List.generate(str.length * 2 - 1, (index) {
      if (index.isOdd) return " ";
      return str[index ~/ 2];
    });
    if (str.isEmpty) return "0 мин";
    return str.fold("", (previousValue, element) => previousValue + element);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const H5Text(
          'О занятии',
          color: ColorData.colorElementsSecondary,
        ),
        const SizedBox(height: 5),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 15),
          child: Row(
            children: [
              const Expanded(
                child: P1Text(
                  'Длительность',
                  color: ColorData.colorTextMain,
                ),
              ),
              P1Text(
                _getStr(),
                color: ColorData.colorTextMain,
              )
            ],
          ),
        ),
        const Divider(
          color: ColorData.colorElementsSecondary,
          thickness: 0.2,
          height: 0,
        ),
      ],
    );
  }
}
