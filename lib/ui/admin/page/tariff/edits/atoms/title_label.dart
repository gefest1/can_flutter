import 'package:flutter/material.dart';

import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';

class TitleLabel extends StatelessWidget {
  final String title;
  final String label;
  final EdgeInsets padding;

  const TitleLabel({
    required this.title,
    required this.label,
    this.padding = const EdgeInsets.symmetric(vertical: 15),
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          H4Text(
            title,
            color: ColorData.colorElementsActive,
          ),
          const SizedBox(height: 5),
          P1Text(
            label,
            color: ColorData.colorElementsActive,
          ),
        ],
      ),
    );
  }
}
