import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tube/logic/blocs/admin/booking_admin/booking_admin_bloc.dart';
import 'package:tube/logic/blocs/admin/booking_admin/models/edit_duration.model.dart';
import 'package:tube/logic/blocs/admin/booking_admin/models/edit_schedule.model.dart';
import 'package:tube/logic/model/admin/edit_booking.model.dart';
import 'package:tube/ui/admin/page/tariff/edits/edit_info_duration_page.dart';
import 'package:tube/ui/admin/page/tariff/observe_tariff_page.dart';
import 'package:tube/utils/load_popup_route.dart';
import 'package:tube/utils/nested_logic_route.dart';

mixin EditDurationMixin on ObserveTariffPageStateAbstract {
  @override
  void editInfoDuration() async {
    final editModel = EditBookingModel(
      timePerMinute: booking.timePerMinute?.toInt() ?? 0,
    );

    editModel.routes.add(
      MaterialPageRoute(
        builder: (context) {
          return MultiProvider(
            providers: [
              ChangeNotifierProvider(
                create: (context) => editModel,
              )
            ],
            child: NestedLogicRoute(
              child: EditInfoDurationPage(
                count: widget.booking.count?.toInt() ?? 0,
                deadlineDays: widget.booking.deadlineDays ?? 0,
                name: widget.booking.name ?? '',
                timePerMinute: widget.booking.timePerMinute?.toInt() ?? 0,
                studentNumber: booking.studentNumber ?? 10,
              ),
            ),
          );
        },
      ),
    );
    await Navigator.push(context, editModel.routes.first);
    if (editModel.count == null || editModel.deadlineDays == null) return;
    final c = Completer();
    if (!mounted) return;
    Navigator.push(context, LoadPopupRoute(completer: c));
    bookingAdminBloc.add(EditBookingEvent(
      bookingId: widget.booking.id!,
      completer: c,
      editDuration: EditDuration(
        count: editModel.count!,
        deadlineDays: editModel.deadlineDays!,
        timePerMinute: editModel.timePerMinute,
        studentNumber: editModel.studentNumber!,
      ),
      applyToOld: editModel.concernsOld,
      editSchedule: editModel.workTime == null
          ? null
          : EditSchedule(workTime: editModel.workTime!),
    ));
    await c.future;
  }
}
