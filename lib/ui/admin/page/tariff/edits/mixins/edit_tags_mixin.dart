import 'dart:async';

import 'package:flutter/material.dart';
import 'package:tube/logic/blocs/admin/booking_admin/booking_admin_bloc.dart';
import 'package:tube/logic/blocs/admin/booking_admin/models/edit_tags.model.dart';
import 'package:tube/ui/admin/page/tariff/edits/edit_tags_page.dart';
import 'package:tube/ui/admin/page/tariff/observe_tariff_page.dart';
import 'package:tube/utils/load_popup_route.dart';

mixin EditTagsMixin on ObserveTariffPageStateAbstract {
  @override
  void editTags() async {
    final val = await Navigator.push<EditTags>(
      context,
      MaterialPageRoute(
        builder: (context) => EditTagsPage(
          tags: booking.tags,
        ),
      ),
    );
    if (val == null) return;
    final c = Completer();
    if (!mounted) return;
    Navigator.push(
      context,
      LoadPopupRoute(completer: c),
    );
    bookingAdminBloc.add(
      EditBookingEvent(
        bookingId: widget.booking.id!,
        completer: c,
        editTags: val,
      ),
    );
    await c.future;
    return;
  }
}
