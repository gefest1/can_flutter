import 'package:flutter/material.dart';

import 'package:tube/ui/common/atoms/read_more_widget.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';

class EditNameCard extends StatelessWidget {
  final String description;
  final VoidCallback? onTap;
  const EditNameCard({
    required this.description,
    this.onTap,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(
        left: 15,
        right: 15,
        top: 15,
        bottom: 20,
      ),
      decoration: BoxDecoration(
        color: ColorData.color3Percent,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              const Expanded(
                child: H4Text(
                  'Об абонементе',
                  color: ColorData.colorTextMain,
                ),
              ),
              SizeTapAnimation(
                onTap: onTap,
                child: const P1Text(
                  'Изменить',
                  color: ColorData.colorMainLink,
                ),
              )
            ],
          ),
          const SizedBox(height: 15),
          if (description.isNotEmpty)
            ReadMoreWidget(
              description,
              trimLines: 3,
            )
        ],
      ),
    );
  }
}
