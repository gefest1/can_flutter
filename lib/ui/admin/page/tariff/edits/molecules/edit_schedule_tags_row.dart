import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tube/logic/blocs/admin/booking_detail_data_bloc/booking_detail_data_bloc.dart';
import 'package:tube/ui/admin/page/tariff/edits/atoms/title_card.dart';

class EditScheduleTagsRow extends StatelessWidget {
  final BookingDetailDataBloc bloc;
  final VoidCallback? scheduleTap;
  final VoidCallback? tagsTap;
  final int tagsLength;

  const EditScheduleTagsRow({
    required this.bloc,
    required this.tagsLength,
    this.scheduleTap,
    this.tagsTap,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: BlocBuilder<BookingDetailDataBloc, BookingDetailDataState>(
            bloc: bloc,
            builder: (context, state) {
              if (state is! DataBookingDetailDataState) {
                return const SizedBox();
              }
              return TitleCard(
                onTap: scheduleTap,
                label: 'Сотрудники',
                title: "${state.trainers.length} чел",
              );
            },
          ),
        ),
        const SizedBox(width: 10),
        Expanded(
          child: TitleCard(
            title: "$tagsLength шт.",
            label: 'Атрибуты',
            onTap: tagsTap,
          ),
        ),
      ],
    );
  }
}
