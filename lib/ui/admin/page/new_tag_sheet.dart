// ignore: unused_import
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:tube/ui/admin/molecules/add_mail_body.dart';
import 'package:tube/ui/common/molecules/fast_app_bar.dart';

class NewTagSheet extends StatefulWidget {
  final ScrollController controller;

  const NewTagSheet({
    required this.controller,
    Key? key,
  }) : super(key: key);

  @override
  State<NewTagSheet> createState() => _NewTagSheetState();
}

class _NewTagSheetState extends State<NewTagSheet> {
  final List<String> _list = [];
  late final TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);
    return GestureDetector(
      onTap: () {
        FocusScopeNode f = FocusScope.of(context);

        if (f.hasFocus || f.hasPrimaryFocus) {
          f.unfocus();
        }
      },
      child: Container(
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(10),
          ),
        ),
        child: CustomScrollView(
          controller: widget.controller,
          slivers: [
            SliverToBoxAdapter(
              child: FastAppBar(
                title: 'Новая должность',
                suffIcon: Icons.close,
                suffTextOnTap: () => Navigator.pop(context),
              ),
            ),
            SliverPadding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              sliver: SliverToBoxAdapter(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: 20),
                    AddMailBody(
                      keyboardType: TextInputType.text,
                      hintText: 'Новая должность',
                      title: 'Введите название новой должности',
                      controller: controller,
                      list: _list,
                      callBack: () {},
                    ),
                    const SizedBox(height: 20),
                  ],
                ),
              ),
            ),
            SliverToBoxAdapter(
              child: SizedBox(
                height: mediaQuery.viewInsets.bottom,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
