import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tube/logic/blocs/admin/big_service/entities/big_service_entity.dart';
import 'package:tube/ui/common/atoms/read_more_widget.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';
import 'package:tube/utils/get_adaptive_span.dart';

class ObserveBigServicePage extends StatelessWidget {
  final BigServiceEntity bigService;

  const ObserveBigServicePage({
    required this.bigService,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const H4Text(
          'Услуги',
          color: Colors.white,
        ),
        centerTitle: true,
      ),
      backgroundColor: Colors.white,
      body: ListView(
        padding: const EdgeInsets.all(20),
        children: [
          H2Text(
            bigService.name,
            color: ColorData.colorElementsActive,
          ),
          const SizedBox(height: 20),
          Container(
            decoration: BoxDecoration(
              color: ColorData.color3Percent,
              borderRadius: BorderRadius.circular(10),
            ),
            padding: const EdgeInsets.only(
              left: 15,
              right: 15,
              top: 15,
              bottom: 20,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const H4Text(
                  'Об услуге',
                  color: ColorData.colorTextMain,
                ),
                const SizedBox(height: 15),
                const Divider(
                  thickness: 0.5,
                  height: 0,
                  color: Color(0xffCBCBCB),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: ReadMoreWidget(
                    bigService.description,
                    trimLines: 4,
                  ),
                ),
              ],
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 20),
              const H4Text(
                'Длительность услуги',
                color: ColorData.colorTextMain,
              ),
              const SizedBox(height: 5),
              P1Text(
                "${bigService.durationMin} минут",
                color: ColorData.colorTextMain,
              ),
              const SizedBox(height: 15),
              const Divider(
                thickness: 0.5,
                height: 0,
                color: Color(0xffCBCBCB),
              ),

              //

              const SizedBox(height: 20),
              const H4Text(
                'Цена',
                color: ColorData.colorTextMain,
              ),
              const SizedBox(height: 5),
              Builder(builder: (context) {
                return RichText(
                  text: getAdaptiveTextSpan(
                    NumberFormat('###,###,000₸').format(bigService.price),
                    const TextStyle(
                      fontSize: P1TextStyle.fontSize,
                      fontWeight: P1TextStyle.fontWeight,
                      height: P1TextStyle.height,
                      color: ColorData.colorTextMain,
                    ),
                    context,
                  ),
                );
              }),
              const SizedBox(height: 15),
              const Divider(
                thickness: 0.5,
                height: 0,
                color: Color(0xffCBCBCB),
              ),

              //

              const SizedBox(height: 20),
              const H4Text(
                'Сотрудник получает',
                color: ColorData.colorTextMain,
              ),
              const SizedBox(height: 5),
              P1Text(
                '${bigService.percent.toDouble()}% от стоимости услуги',
                color: ColorData.colorTextMain,
              ),
              // Builder(builder: (context) {
              //   return RichText(
              //     text: getAdaptiveTextSpan(
              //       NumberFormat('###,###,000₸').format(bigService.price),
              //       const TextStyle(
              //         fontSize: P1TextStyle.fontSize,
              //         fontWeight: P1TextStyle.fontWeight,
              //         height: P1TextStyle.height,
              //         color: ColorData.colorTextMain,
              //       ),
              //       context,
              //     ),
              //   );
              // }),
            ],
          )
        ],
      ),
    );
  }
}
