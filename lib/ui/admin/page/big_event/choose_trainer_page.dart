import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:tube/logic/blocs/user/user.model.dart';

import 'package:tube/ui/admin/atoms/user_active_tile.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';

import 'package:tube/ui/common/molecules/size_tap_animation.dart';

class ChooseTrainerPage extends StatefulWidget {
  final List<User> users;
  final Map<String, User>? activeUsers;

  const ChooseTrainerPage({
    this.users = const [],
    this.activeUsers,
    super.key,
  });

  @override
  State<ChooseTrainerPage> createState() => _ChooseTrainerPageState();
}

class _ChooseTrainerPageState extends State<ChooseTrainerPage> {
  late Map<String, User> activeUsers = {...(widget.activeUsers ?? {})};

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
      ),
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          ListView.separated(
            padding: const EdgeInsets.all(20),
            itemBuilder: (context, index) {
              return SizeTapAnimation(
                onTap: () {
                  if (activeUsers.containsKey(widget.users[index].id!)) {
                    setState(() {
                      activeUsers.remove(widget.users[index].id!);
                    });
                  } else {
                    setState(() {
                      activeUsers[widget.users[index].id!] =
                          widget.users[index];
                    });
                  }
                },
                child: UserActiveTile(
                  title:
                      widget.users[index].fullName ?? widget.users[index].email,
                  photoUrl: widget.users[index].photoUrl,
                  active: activeUsers.containsKey(widget.users[index].id!),
                ),
              );
            },
            separatorBuilder: (context, index) {
              return const SizedBox(height: 10);
            },
            itemCount: widget.users.length,
          ),
          if (!mapEquals(widget.activeUsers, activeUsers))
            Positioned(
              left: 0,
              right: 0,
              bottom: 0,
              child: BottomButton(
                title: 'Сохранить',
                onTap: () {
                  Navigator.pop(context, activeUsers);
                },
              ),
            )
        ],
      ),
    );
  }
}
