import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:tube/ui/admin/molecules/add_mail_body.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';

class AddGuestPage extends StatefulWidget {
  final List<String> list;

  const AddGuestPage({
    required this.list,
    super.key,
  });

  @override
  State<AddGuestPage> createState() => _AddGuestPageState();
}

class _AddGuestPageState extends State<AddGuestPage> {
  late final List<String> list = [...widget.list];

  void update() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: const H4Text(
          'Добавление по почте',
          color: Colors.white,
        ),
      ),
      body: Stack(
        children: [
          ListView(
            padding: const EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 40,
            ),
            children: [
              AddMailBody(
                list: list,
                callBack: update,
              ),
              const SizedBox(height: 200),
            ],
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: BottomButton(
              title: 'Добавить',
              active: !listEquals(widget.list, list),
              onTap: () {
                Navigator.pop(context, list);
              },
            ),
          ),
        ],
      ),
    );
  }
}
