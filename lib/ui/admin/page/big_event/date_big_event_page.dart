import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/logic/provider/create_big_event/create_big_event.dart';
import 'package:tube/ui/admin/finances/mol/shedule_select.dart';
import 'package:tube/ui/admin/page/big_event/big_event_duration_wheel.dart';
import 'package:tube/ui/admin/page/big_event/client_big_event_page.dart';
import 'package:tube/ui/admin/page/time_set_page.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/utils/parse_time.dart';

class DateBigEventPage extends StatefulWidget {
  const DateBigEventPage({super.key});

  @override
  State<DateBigEventPage> createState() => _DateBigEventPageState();
}

class _DateBigEventPageState extends State<DateBigEventPage> {
  final dateTime = BehaviorSubject<DateTime?>.seeded(null);
  final startTime = BehaviorSubject<DateTime>.seeded(getStratTime());

  final durationSub = BehaviorSubject<Duration>.seeded(const Duration());

  List<StreamSubscription> streamSubs = [];

  @override
  void initState() {
    streamSubs.addAll([
      dateTime.listen(update),
      startTime.listen(update),
      durationSub.listen(update)
    ]);

    super.initState();
  }

  void update(dynamic val) {
    activeSubject.value =
        dateTime.value != null && durationSub.value > const Duration();
  }

  final activeSubject = BehaviorSubject.seeded(false);

  @override
  void dispose() {
    for (final cancel in streamSubs) {
      cancel.cancel();
    }
    streamSubs.clear();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
        centerTitle: true,
        title: const H4Text(
          'Время проведения',
          color: Colors.white,
        ),
      ),
      body: Stack(
        children: [
          ListView(
            padding: const EdgeInsets.all(20),
            children: [
              SheduleSelect(
                dateTime: dateTime,
              ),
              const SizedBox(height: 20),
              TimeSet(
                title: 'Начало мероприятия',
                value: startTime,
                padding: EdgeInsets.zero,
              ),
              const SizedBox(height: 20),
              BigEventDurationWheel(
                title: 'Длительность мероприятия',
                durationSubject: durationSub,
              ),
              const SizedBox(height: 200),
            ],
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: StreamBuilder(
              stream: activeSubject,
              builder: (context, _) {
                return BottomButton(
                  active: activeSubject.value,
                  onTap: () async {
                    final createProvider = Provider.of<CreateBigEventProvider>(
                        context,
                        listen: false);
                    final value = createProvider.createBigEvent;

                    createProvider.setNewInstance(
                      createProvider.createBigEvent.copyWith(
                        startDate: DateTime(
                          dateTime.value!.year,
                          dateTime.value!.month,
                          dateTime.value!.day,
                          startTime.value.hour,
                          startTime.value.minute,
                        ),
                        duration: durationSub.value,
                      ),
                    );
                    await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => const ClientBigEventPage(),
                      ),
                    );

                    createProvider.setNewInstance(value);
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
