import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/logic/blocs/admin/workers/worker_bloc.dart';
import 'package:tube/logic/blocs/big_event/big_event_bloc.dart';
import 'package:tube/logic/blocs/big_event/entities/create_big_entity.dart';
import 'package:tube/logic/blocs/club_client/club_client_bloc.dart';
import 'package:tube/logic/blocs/user/user.model.dart';
import 'package:tube/logic/general_providers.dart';
import 'package:tube/logic/provider/create_big_event/create_big_event.dart';
import 'package:tube/ui/admin/molecules/trainer_list.dart';
import 'package:tube/ui/admin/page/big_event/add_guest_page.dart';
import 'package:tube/ui/admin/page/big_event/choose_trainer_page.dart';
import 'package:tube/ui/admin/page/tariff/molecules/stack_image_card.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';
import 'package:tube/utils/load_popup_route.dart';

class ClientBigEventPage extends StatefulWidget {
  const ClientBigEventPage({super.key});

  @override
  State<ClientBigEventPage> createState() => _ClientBigEventPageState();
}

class _ClientBigEventPageState extends State<ClientBigEventPage> {
  AdminBlocMap get getAdminBloc =>
      Provider.of<AdminBlocMap>(context, listen: false);
  WorkerState get workerState => getAdminBloc
      .getWorkerBloc(MainDrawerState.currentParticipance!.clubId!)
      .state;
  ClubClientBloc get clubClientBloc =>
      getAdminBloc.getClubClient(MainDrawerState.currentParticipance!.clubId!);

  final activeTrainerSubject = BehaviorSubject<Map<String, User>>.seeded({});
  final activeClientSubject = BehaviorSubject<Map<String, User>>.seeded({});
  final activeGuestSubject = BehaviorSubject<List<String>>.seeded([]);

  AdminBigEventBloc get adminBigEventBloc =>
      getAdminBloc.getAdminBigEventBloc(MainDrawerState.clubId!);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context),
        ),
        centerTitle: true,
        title: const H4Text(
          'Участники',
          color: Colors.white,
        ),
      ),
      body: Stack(
        children: [
          ListView(
            padding: const EdgeInsets.symmetric(vertical: 40, horizontal: 20),
            children: [
              TrainerList(
                padding: EdgeInsets.zero,
                active: activeTrainerSubject.value.isNotEmpty,
                onTap: () async {
                  final workerState = this.workerState;
                  if (workerState is ClubWorkerState &&
                      workerState.mapWorkers.isNotEmpty) {
                    final List<User> users = workerState.mapWorkers;
                    final activeUserMap = await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => ChooseTrainerPage(
                          activeUsers: {...activeTrainerSubject.value},
                          users: users,
                        ),
                      ),
                    );
                    if (activeUserMap == null) return;
                    setState(() {
                      activeTrainerSubject.value = activeUserMap;
                    });
                  }
                },
              ),
              const SizedBox(height: 20),
              BlocBuilder(
                bloc: clubClientBloc,
                builder: (context, state) {
                  if (state is! DataClubClientState) return const SizedBox();

                  return StackImageCard(
                    numberTitle: state.users.length,
                    onTap: () async {
                      final activeUserMap = await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (_) => ChooseTrainerPage(
                            activeUsers: {...activeClientSubject.value},
                            users: state.users,
                          ),
                        ),
                      );
                      if (activeUserMap == null) return;
                      setState(() {
                        activeClientSubject.value = activeUserMap;
                      });
                    },
                    active: activeClientSubject.value.isNotEmpty,
                    title: "Клиенты",
                    images: state.users
                        .map((e) => e.photoUrl?.m)
                        .whereType<String>()
                        .toList(),
                  );
                },
              ),
              const SizedBox(height: 20),
              StackImageCard(
                numberTitle: activeGuestSubject.value.length,
                onTap: () async {
                  final list = await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => AddGuestPage(
                        list: activeGuestSubject.value,
                      ),
                    ),
                  );
                  if (list == null) return;

                  setState(() {
                    activeGuestSubject.value = list;
                  });
                },
                title: "Гости",
              ),
              const SizedBox(height: 100),
            ],
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: BottomButton(
              title: 'Создать мероприятие',
              subTitle:
                  'Участникам будет отправлено уведомление и отметка в календаре.',
              onTap: () async {
                final bigEvent =
                    Provider.of<CreateBigEventProvider>(context, listen: false)
                        .createBigEvent;

                final completer = Completer();

                adminBigEventBloc.add(
                  AddBigEventEvent(
                    entity: CreateBigEventEntity(
                      durationMin: bigEvent.duration!.inMinutes,
                      name: bigEvent.name!,
                      description: bigEvent.description!,
                      addressData: bigEvent.addressData!.toAddressDataEntity(),
                      date: bigEvent.startDate!,
                      clientIds: activeClientSubject.value.keys.toList(),
                      trainerIds: activeTrainerSubject.value.keys.toList(),
                      clubId: MainDrawerState.clubId!,
                      guestsEmail: activeGuestSubject.value,
                    ),
                    completer: completer,
                  ),
                );
                Navigator.push(context, LoadPopupRoute(completer: completer));
                await completer.future;
                if (!mounted) return;
                Navigator.of(context, rootNavigator: true)
                    .popUntil((route) => route.isFirst);
              },
            ),
          ),
        ],
      ),
    );
  }
}
