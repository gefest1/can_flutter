import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:tube/logic/provider/big_user_map.dart';
import 'package:tube/ui/admin/molecules/trainer_tile.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';

class UserListPage extends StatefulWidget {
  final String title;
  final List<String> ids;

  const UserListPage({
    required this.title,
    required this.ids,
    super.key,
  });

  @override
  State<UserListPage> createState() => _UserListPageState();
}

class _UserListPageState extends State<UserListPage> {
  late BigUserMap bigUserMap = Provider.of<BigUserMap>(context);

  @override
  void initState() {
    super.initState();
    Future(() {
      bigUserMap.getUsers(widget.ids);
    });
  }

  @override
  Widget build(BuildContext context) {
    final filteredUsers = widget.ids
        .where((element) => bigUserMap.userMap[element] != null)
        .map((e) => bigUserMap.userMap[e])
        .toList();
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: H4Text(widget.title),
      ),
      backgroundColor: Colors.white,
      body: ListView.builder(
        itemCount: filteredUsers.length,
        itemBuilder: (context, index) => TrainerTile(
          innerPadding: const EdgeInsets.symmetric(horizontal: 20),
          title: filteredUsers[index]?.fullName ??
              filteredUsers[index]?.email ??
              '',
          photoUrl: filteredUsers[index]?.photoUrl?.m,
        ),
      ),
    );
  }
}
