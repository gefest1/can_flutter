import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rxdart/subjects.dart';
import 'package:tube/ui/admin/atoms/wheel_label.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/utils/colors.dart';

class BigEventDurationWheel extends StatefulWidget {
  final String title;
  final BehaviorSubject<Duration>? durationSubject;

  const BigEventDurationWheel({
    this.title = 'Срок действия абонемента',
    this.durationSubject,
    Key? key,
  }) : super(key: key);

  @override
  State<BigEventDurationWheel> createState() => _BigEventDurationWheelState();
}

String getDoubleStr(String str) {
  final toReturn = "00$str";
  return toReturn.substring(toReturn.length - 2, toReturn.length);
}

class _BigEventDurationWheelState extends State<BigEventDurationWheel> {
  int _lastHapticIndex = 0;
  late final dayController = FixedExtentScrollController()..addListener(update);
  late final hourController = FixedExtentScrollController()
    ..addListener(update);
  late final minController = FixedExtentScrollController()..addListener(update);
  late BehaviorSubject<Duration> durationSubject = widget.durationSubject ??
      BehaviorSubject<Duration>.seeded(const Duration());

  void update() {
    final newDuration = Duration(
      days: dayController.selectedItem % 100,
      hours: hourController.selectedItem % 24,
      minutes: minController.selectedItem % 60,
    );
    if (newDuration == durationSubject.value) return;

    durationSubject.value = newDuration;
  }

  void _handleSelectedItemChanged(int indexDirt) {
    final index = indexDirt % 10;
    if (index != _lastHapticIndex) {
      _lastHapticIndex = index;
      HapticFeedback.selectionClick();
    }
  }

  Widget _wheel(int inter, FixedExtentScrollController controller) {
    return ListWheelScrollView.useDelegate(
      itemExtent: 41,
      clipBehavior: Clip.none,
      overAndUnderCenterOpacity: 0.3,
      diameterRatio: 1.2,
      controller: controller,
      onSelectedItemChanged: _handleSelectedItemChanged,
      squeeze: 1.4,
      physics: const FixedExtentScrollPhysics(),
      childDelegate: ListWheelChildBuilderDelegate(
        builder: (ctx, index) {
          return SizedBox(
            height: 41,
            child: Center(
              child: H3Text(
                "${index % inter}",
                color: ColorData.colorElementsActive,
              ),
            ),
          );
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 146,
      decoration: BoxDecoration(
        color: const Color(0xffF7F7F7),
        borderRadius: BorderRadius.circular(10),
      ),
      child: ClipRect(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(10),
              child: H5Text(
                widget.title,
                color: ColorData.colorTextMain,
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Stack(
                  children: [
                    Center(
                      child: Container(
                        height: 41,
                        margin: const EdgeInsets.symmetric(horizontal: 10),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: Stack(
                            children: [
                              _wheel(100, dayController),
                              const WheelLabel(data: 'дн'),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Stack(
                            children: [
                              _wheel(24, hourController),
                              const WheelLabel(data: 'ч'),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Stack(
                            children: [
                              _wheel(60, minController),
                              const WheelLabel(data: 'мин'),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
