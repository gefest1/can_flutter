import 'package:flutter/material.dart';
import 'package:tube/ui/admin/molecules/add_mail_body.dart';
import 'package:tube/ui/admin/page/trainer_detail_page.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/custom_button.dart';

class AddFieldByMail extends StatefulWidget {
  const AddFieldByMail({Key? key}) : super(key: key);

  @override
  State<AddFieldByMail> createState() => _AddFieldByMailState();
}

class _AddFieldByMailState extends State<AddFieldByMail> {
  final List<String> _list = [];
  late final TextEditingController controller = TextEditingController();
  final ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    // log(scrollController.toString());
    final media = MediaQuery.of(context);
    return PrimaryScrollController(
      controller: scrollController,
      child: GestureDetector(
        onTap: () {
          final focus = FocusScope.of(context);

          if (focus.hasFocus || focus.hasPrimaryFocus) {
            focus.unfocus();
          }
        },
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: const H4Text(
              'Добавление сотрудников',
              color: Colors.white,
            ),
            centerTitle: true,
            leading: IconButton(
              icon: const Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
              onPressed: () => Navigator.pop(context),
            ),
          ),
          body: Stack(
            children: [
              ListView(
                controller: scrollController,
                padding: EdgeInsets.only(
                  top: 40,
                  left: 20,
                  right: 20,
                  bottom: 81 + media.padding.bottom,
                ),
                children: [
                  AddMailBody(
                    controller: controller,
                    list: _list,
                    callBack: () {
                      setState(() {});
                    },
                  ),
                  SizedBox(height: 200 + media.viewInsets.bottom),
                ],
              ),
              if (_list.isNotEmpty)
                Positioned(
                  left: 0,
                  right: 0,
                  bottom: 0,
                  child: Container(
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          offset: Offset(0, -4),
                          color: Color(0x0d000000),
                          blurRadius: 15,
                        ),
                      ],
                    ),
                    padding: EdgeInsets.only(
                      left: 20,
                      right: 20,
                      top: 10,
                      bottom: 10 + MediaQuery.of(context).padding.bottom,
                    ),
                    child: CustomButton(
                      title: 'Добавить e-mail',
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (_) => const TrainerDetailPage(),
                          ),
                        );
                      },
                    ),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
