import 'package:flutter/material.dart';
import 'package:tube/ui/admin/page/trainer_contact_page.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/custom_button.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';
import 'package:tube/utils/colors.dart';

class RegisterTrainerPage extends StatelessWidget {
  const RegisterTrainerPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: const H4Text(
          'jeanwwd@gmail.com',
          color: Colors.white,
        ),
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: const Icon(
            Icons.arrow_back,
            size: 33,
          ),
          color: Colors.white,
        ),
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView(
              padding: const EdgeInsets.only(top: 40, left: 20, right: 20),
              children: [
                CircleAvatar(
                  radius: 162 / 2,
                  backgroundColor: const Color(0xffF3F4F6),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      Icon(
                        Icons.add_a_photo,
                        color: ColorData.colorTextMain,
                        size: 24,
                      ),
                      SizedBox(height: 5),
                      P2Text(
                        'Загрузить\nфото',
                        color: ColorData.colorTextMain,
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 10),
                const Center(
                  child: P1Text(
                    'Фото можно\nзагрузить позже',
                    color: ColorData.colorElementsSecondary,
                    textAlign: TextAlign.center,
                  ),
                ),
                const SizedBox(height: 10),
                const CustomTextField(
                  hintText: 'Иван Иванов',
                  title: 'Полное Имя',
                ),
                const SizedBox(height: 30),
                const CustomTextField(
                  hintText: 'Расскажите о себе',
                  title:
                      'Описание. Расскажите о подходе, опишите как помогаете клиентам. Можно добавить позже',
                ),
                const SizedBox(height: 30),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            decoration: const BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Color(0x1a000000),
                  offset: Offset(0, -0.5),
                ),
              ],
            ),
            child: CustomButton(
              title: 'Далее',
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (_) => const TrainerContactPage(),
                  ),
                );
              },
              color: ColorData.colorMainLink,
            ),
          ),
        ],
      ),
    );
  }
}
