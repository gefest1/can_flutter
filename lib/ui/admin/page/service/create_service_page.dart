import 'package:flutter/material.dart';
import 'package:tube/ui/admin/molecules/service_wheel.dart';
import 'package:tube/ui/admin/page/service/set_trainer_service_page.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/bottom_button.dart';
import 'package:tube/ui/common/molecules/custom_text_field.dart';

class CreateServicePage extends StatefulWidget {
  const CreateServicePage({Key? key}) : super(key: key);

  @override
  State<CreateServicePage> createState() => _CreateServicePageState();
}

class _CreateServicePageState extends State<CreateServicePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: const H4Text(
          'Добавление услуги',
          color: Colors.white,
        ),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            size: 33,
          ),
          onPressed: () => Navigator.pop(context),
        ),
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 40),
              children: const [
                CustomTextField(
                  hintText: 'Например: «Детский массаж»',
                  title: 'Название услуги',
                ),
                SizedBox(height: 30),
                CustomTextField(
                  title:
                      'Описание. Поможет вашим клиентам выбрать именно то, что им нужно',
                  hintText: 'Опишите услугу',
                ),
                SizedBox(height: 30),
                ServiceWheel(),
              ],
            ),
          ),
          BottomButton(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => const SetTrainerServicePage(),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
