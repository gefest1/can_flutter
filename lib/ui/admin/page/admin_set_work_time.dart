// import 'dart:async';

// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:intl/intl.dart';
// import 'package:provider/provider.dart';
// import 'package:tube/flexible/lib/src/flexible_bottom_sheet_route.dart';
// import 'package:tube/logic/bloc_wrapper.dart';
// import 'package:tube/logic/blocs/trainer/work_time/work_time_bloc.dart';
// import 'package:tube/logic/model/work_time_graph.dart';
// import 'package:tube/ui/admin/page/set_worktime_sheet.dart';
// import 'package:tube/ui/common/atoms/texts_widgets.dart';
// import 'package:tube/ui/common/molecules/bottom_button.dart';
// import 'package:tube/ui/common/templates/main_drawer.dart';
// import 'package:tube/ui/main_page/components/atoms/day_card.dart';
// import 'package:tube/ui/main_page/components/organisms/active_time_card.dart';
// import 'package:tube/utils/week_day.dart';

// class AdminSetWorkTime extends StatefulWidget {
//   final String? clubId;

//   const AdminSetWorkTime({
//     this.clubId,
//     Key? key,
//   }) : super(key: key);

//   @override
//   State<AdminSetWorkTime> createState() => _AdminSetWorkTimeState();
// }

// class _AdminSetWorkTimeState extends State<AdminSetWorkTime> {
//   late ControllerWorkTimeBloc workTimeBloc =
//       Provider.of<TrainerBlocMap>(context, listen: false).getWorkTimeBloc(
//     widget.clubId ?? MainDrawerState.currentParticipance!.clubId!,
//   );
//   Map<int, WorkTimeGraph?>? _workTimes;
//   bool show = false;

//   @override
//   void initState() {
//     workTimeBloc.state;
//     super.initState();
//   }

//   void _openSheet(int index) async {
//     WorkTimeGraph? _workTime = _workTimes![index] == null
//         ? null
//         : WorkTimeGraph(
//             startTime:
//                 _workTimes![index]?.startTime!.subtract(Duration(days: index)),
//             endTime:
//                 _workTimes![index]?.endTime!.subtract(Duration(days: index)),
//           );

//     final _result = await showFlexibleBottomSheet<List<DateTime>>(
//       minHeight: 0,
//       initHeight: 0.95,
//       maxHeight: 0.95,
//       context: context,
//       anchors: [0.95],
//       builder: (ctx, controller, _) {
//         return Material(
//           color: Colors.transparent,
//           child: SetWorktimeSheet(
//             controller: controller,
//             startTime: _workTime?.startTime,
//             endTime: _workTime?.endTime,
//             title: weeks[index],
//           ),
//         );
//       },
//     );
//     if (_result == null) return;
//     if (_result.last.difference(_result.first).inDays >= 1) {
//       _result.last = _result.last.subtract(const Duration(days: 1));
//     }
//     setState(() {
//       show = true;
//       _workTimes![index] = WorkTimeGraph(
//         startTime: _result.first.add(Duration(days: index)),
//         endTime: _result.last.add(Duration(days: index)),
//       );
//     });
//   }

//   void _sendData() async {
//     if (_workTimes == null) return;
//     Completer completer = Completer();
//     workTimeBloc.add(
//       SetDataWorkTimeEvent(
//         clubId: MainDrawerState.currentParticipance!.clubId!,
//         workTimes: _workTimes!.values.where((element) {
//           return element != null;
//         }).toList() as List<WorkTimeGraph>,
//         completer: completer,
//       ),
//     );
//     await completer.future;
//     Navigator.pop(context);
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.white,
//       appBar: AppBar(
//         centerTitle: true,
//         title: Column(
//           children: const [
//             H4Text(
//               'Рабочее время',
//               color: Colors.white,
//             ),
//             // P1Text(
//             //   'Иван И.И.',
//             //   color: Colors.white,
//             // )
//           ],
//         ),
//       ),
//       body: BlocBuilder<ControllerWorkTimeBloc, WorkTimeState>(
//           bloc: workTimeBloc,
//           builder: (context, _state) {
//             if (_state is LoadingWorkTimeState) {
//               return const Center(
//                 child: CircularProgressIndicator.adaptive(),
//               );
//             }
//             final state = _state as SuccessWorkTimeState;
//             _workTimes ??= state.workTime;
//             return Column(
//               children: [
//                 Expanded(
//                   child: ListView.separated(
//                     padding: const EdgeInsets.symmetric(
//                         horizontal: 20, vertical: 40),
//                     itemBuilder: (context, index) {
//                       if (_workTimes![index] == null) {
//                         return DayCard(
//                           title: weeks[index],
//                           label: 'Выбрать время',
//                           onTap: () => _openSheet(index),
//                         );
//                       }

//                       final _frstText = DateFormat('HH:mm')
//                           .format(_workTimes![index]!.startTime!);
//                       final _scndText = DateFormat('HH:mm')
//                           .format(_workTimes![index]!.endTime!);
//                       return ActiveTimeCard(
//                         onTap: () => _openSheet(index),
//                         label: 'Изменить',
//                         title: weeks[index],
//                         subLabel: '$_frstText — $_scndText',
//                       );
//                     },
//                     itemCount: 7,
//                     separatorBuilder: (context, index) =>
//                         const SizedBox(height: 10),
//                   ),
//                 ),
//                 if (show)
//                   BottomButton(
//                     title: 'Завершить',
//                     onTap: _sendData,
//                   ),
//               ],
//             );
//           }),
//     );
//   }
// }
