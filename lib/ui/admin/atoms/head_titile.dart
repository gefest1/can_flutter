import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/templates/main_drawer.dart';

class HeadTitle extends StatelessWidget {
  const HeadTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: MainDrawerState.currentParticipanceValue,
      builder: (ctx, snap) {
        if (MainDrawerState.currentParticipanceValue.value?.club?.fullName ==
            null) {
          return const SizedBox();
        }

        final part = MainDrawerState.currentParticipanceValue.value!;

        return H4Text(
          part.club!.fullName!,
        );
      },
    );
  }
}
