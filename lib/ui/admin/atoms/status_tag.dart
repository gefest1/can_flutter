import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';

class StatusTag extends StatelessWidget {
  final bool active;
  const StatusTag({this.active = false, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      child: GestureDetector(
        onTap: () {},
        child: Container(
          padding: const EdgeInsets.symmetric(
            vertical: 10,
            horizontal: 20,
          ),
          decoration: ShapeDecoration(
            color: active ? ColorData.colorElementsActive : Colors.transparent,
            shape: const StadiumBorder(
              side: BorderSide(
                width: 0.5,
                color: ColorData.colorElementsActive,
              ),
            ),
          ),
          child: P0Text(
            'Тренер',
            color: active ? Colors.white : ColorData.colorTextMain,
          ),
        ),
      ),
    );
  }
}
