import 'package:flutter/material.dart';
import 'package:paulonia_cache_image/paulonia_cache_image.dart';
import 'package:tube/logic/model/photourl.dart';
import 'package:tube/ui/admin/atoms/custom_check_box.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/shadow.dart';

class UserActiveTile extends StatelessWidget {
  final VoidCallback? onTap;
  final String? title;
  final bool active;
  final PhotoUrl? photoUrl;

  const UserActiveTile({
    this.title,
    this.onTap,
    this.photoUrl,
    this.active = false,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.all(15),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: blocksMains,
          borderRadius: BorderRadius.circular(15),
        ),
        child: Row(
          children: [
            if (photoUrl != null)
              CircleAvatar(
                radius: 18,
                backgroundColor: Colors.grey,
                backgroundImage: photoUrl?.m == null
                    ? null
                    : PCacheImage(
                        photoUrl!.m!,
                        enableInMemory: true,
                      ),
                foregroundImage: photoUrl?.xl == null
                    ? null
                    : PCacheImage(
                        photoUrl!.xl!,
                        enableInMemory: true,
                      ),
              ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: title == null
                    ? const SizedBox()
                    : H4Text(
                        title!,
                        color: ColorData.colorTextMain,
                      ),
              ),
            ),
            CustomCheckBox(
              active: active,
            ),
          ],
        ),
      ),
    );
  }
}
