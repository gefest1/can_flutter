import 'package:flutter/material.dart';

import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/const.dart';
import 'package:tube/utils/get_adaptive_span.dart';

class FinanceBigCard extends StatelessWidget {
  final VoidCallback? onTap;
  final String? title;
  final String? subTitle;
  final String? trainerTitle;
  final String? userTitle;

  final String? dateTitle;

  const FinanceBigCard({
    this.onTap,
    this.title,
    this.subTitle,
    this.trainerTitle,
    this.dateTitle,
    this.userTitle,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.only(right: 10.0),
              child: CircleAvatar(
                radius: 33 / 2,
                backgroundColor: ColorData.abonement,
                child: Icon(
                  Icons.fiber_smart_record,
                  size: 17,
                  color: Colors.white,
                ),
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      if (title != null)
                        Expanded(
                          child: H4Text(
                            title!,
                            color: ColorData.colorTextMain,
                          ),
                        ),
                      if (subTitle != null)
                        RichText(
                          text: getAdaptiveTextSpan(
                              subTitle!,
                              const TextStyle(
                                fontSize: H4TextStyle.fontSize,
                                height: H4TextStyle.height,
                                fontWeight: H4TextStyle.fontWeight,
                                color: ColorData.colorTextMain,
                              ),
                              context),
                        )
                      // H4Text(
                      //   subTitle!,
                      //   color: ColorData.colorTextMain,
                      // ),
                    ],
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const P2Text(
                              'Запись по абонементу',
                              color: ColorData.colorTextMain,
                            ),
                            if (trainerTitle != null) ...[
                              const SizedBox(height: 2),
                              RichText(
                                text: TextSpan(
                                  style: TextStyle(
                                    color: ColorData.colorElementsActive,
                                    fontSize: P2TextStyle.fontSize,
                                    height: P2TextStyle.height,
                                    fontWeight: P2TextStyle.fontWeight,
                                    fontFamily: DefaultTextStyle.of(context)
                                        .style
                                        .fontFamily,
                                  ),
                                  children: [
                                    const TextSpan(text: 'Сотрудник: '),
                                    TextSpan(
                                      text: trainerTitle!,
                                      style: const TextStyle(
                                        color: ColorData.colorElementsActive,
                                        fontWeight: H5TextStyle.fontWeight,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                            if (userTitle != null) ...[
                              const SizedBox(height: 2),
                              RichText(
                                text: TextSpan(
                                  style: TextStyle(
                                    color: ColorData.colorElementsActive,
                                    fontSize: P2TextStyle.fontSize,
                                    height: P2TextStyle.height,
                                    fontWeight: P2TextStyle.fontWeight,
                                    fontFamily: DefaultTextStyle.of(context)
                                        .style
                                        .fontFamily,
                                  ),
                                  children: [
                                    const TextSpan(text: 'Клиент: '),
                                    TextSpan(
                                      text: userTitle!,
                                      style: const TextStyle(
                                        color: ColorData.colorElementsActive,
                                        fontWeight: H5TextStyle.fontWeight,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                            const SizedBox(height: 15),
                            if (dateTitle != null)
                              P2Text(
                                dateTitle!,
                                // '22 ноября 2022, 11:22',
                                color: ColorData.colorTextMain,
                              )
                          ],
                        ),
                      ),
                      // TODO temporary deleted
                      // const Icon(
                      //   Icons.chevron_right,
                      //   color: Colors.black54,
                      // )
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
