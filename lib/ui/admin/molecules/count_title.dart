import 'package:flutter/material.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/shadow.dart';

class CountTitle extends StatelessWidget {
  final String title;
  final String label;
  final VoidCallback? onTap;

  const CountTitle({
    required this.title,
    required this.label,
    this.onTap,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.all(15),
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10)),
          boxShadow: blocksMains,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            P0Text(
              title,
              color: ColorData.colorButtonMain,
            ),
            const SizedBox(height: 15),
            Row(
              children: [
                Expanded(
                  child: H3Text(
                    label,
                    color: ColorData.colorTextMain,
                  ),
                ),
                const Icon(
                  Icons.navigate_next,
                  color: ColorData.colorTextMain,
                  size: 32,
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
