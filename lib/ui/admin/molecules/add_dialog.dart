import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:tube/flexible/lib/src/flexible_bottom_sheet_route.dart';
import 'package:tube/logic/model/status_enum.dart';
import 'package:tube/logic/provider/create_big_event/create_big_event.dart';
import 'package:tube/logic/provider/create_big_event/create_big_event.model.dart';
import 'package:tube/logic/provider/create_big_service/create_big_service.dart';
import 'package:tube/logic/provider/create_big_service/create_big_service.model.dart';
import 'package:tube/ui/add_compnay/pages/finish_page.dart';
import 'package:tube/ui/admin/atoms/card_container.dart';
import 'package:tube/ui/admin/page/add_admin_page.dart';
import 'package:tube/ui/admin/page/big_event/name_big_event_page.dart';
import 'package:tube/ui/admin/page/big_service/create_big_service_page.dart';
import 'package:tube/ui/admin/page/tariff/create_tariff_page.dart';
import 'package:tube/utils/colors.dart';
import 'package:tube/utils/nested_logic_route.dart';
import 'package:tube/utils/uri_parse.dart';

class AddDialog extends StatelessWidget {
  final String clubId;

  const AddDialog({
    required this.clubId,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        color: Colors.transparent,
        child: Container(
          padding: const EdgeInsets.symmetric(
            vertical: 15,
            horizontal: 20,
          ),
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              const Text(
                'Абонементы',
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  height: 17 / 14,
                  color: ColorData.colortTextSecondary,
                ),
              ),
              const SizedBox(height: 5),
              CardContainer(
                title: 'Создать абонемент',
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => const CreateTariffPage(),
                    ),
                  );
                },
              ),
              const SizedBox(height: 10),
              // const Text(
              //   'Услуги',
              //   style: TextStyle(
              //     fontSize: 14,
              //     fontWeight: FontWeight.w500,
              //     height: 17 / 14,
              //     color: ColorData.colortTextSecondary,
              //   ),
              // ),
              // const SizedBox(height: 5),
              // CardContainer(
              //   title: 'Добавить услугу',
              //   onTap: () async {
              //     Navigator.push(
              //       context,
              //       MaterialPageRoute(
              //         builder: (_) => const CreateServicePage(),
              //       ),
              //     );
              //   },
              // ),
              // const SizedBox(height: 10),
              const Text(
                'Сотрудники',
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  height: 17 / 14,
                  color: ColorData.colortTextSecondary,
                ),
              ),
              const SizedBox(height: 5),
              CardContainer(
                title: 'Добавить сотрудника',
                onTap: () {
                  Navigator.pop(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => const AddAdminPage(),
                    ),
                  );

                  // showFlexibleBottomSheet(
                  //   context: context,
                  //   minHeight: 0,
                  //   initHeight: 0.95,
                  //   maxHeight: 0.95,
                  //   builder: (ctx, _scroll, _) {
                  //     return Material(
                  //       color: Colors.transparent,
                  //       child: FinishPage(
                  //         statusEnum: StatusEnum.trainer,
                  //         clubId: clubId,
                  //         controller: _scroll,
                  //       ),
                  //     );
                  //   },
                  //   anchors: [0.95],
                  // );
                },
              ),
              const SizedBox(height: 10),
              const Text(
                'Клиенты',
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  height: 17 / 14,
                  color: ColorData.colortTextSecondary,
                ),
              ),
              const SizedBox(height: 5),
              CardContainer(
                title: 'Добавить клиента',
                onTap: () {
                  showFlexibleBottomSheet(
                    context: context,
                    minHeight: 0,
                    initHeight: 0.95,
                    maxHeight: 0.95,
                    isDismissible: true,
                    builder: (ctx, scroll, _) {
                      return Material(
                        color: Colors.transparent,
                        child: FinishPage(
                          title: "Добавление клиента",
                          enumType: TypeInviteEnum.InviteClient,
                          statusEnum: StatusEnum.participance,
                          clubId: clubId,
                          controller: scroll,
                          next: false,
                        ),
                      );
                    },
                    anchors: [0.95],
                  );
                },
              ),
              const SizedBox(height: 10),
              const Text(
                'Мероприятия',
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  height: 17 / 14,
                  color: ColorData.colortTextSecondary,
                ),
              ),
              const SizedBox(height: 5),
              CardContainer(
                title: 'Добавить мероприятие',
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => MultiProvider(
                        providers: [
                          ChangeNotifierProvider<CreateBigEventProvider>(
                            create: (_) => CreateBigEventProvider(
                              createBigEvent: const CreateBigEventModel(),
                            ),
                          )
                        ],
                        child: const NestedLogicRoute(
                          child: NameBigEventPage(),
                        ),
                      ),
                    ),
                  );
                },
              ),
              const SizedBox(height: 10),
              const Text(
                'Услуги',
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  height: 17 / 14,
                  color: ColorData.colortTextSecondary,
                ),
              ),
              const SizedBox(height: 5),
              CardContainer(
                title: 'Добавить услугу',
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => MultiProvider(
                        providers: [
                          ChangeNotifierProvider<CreateBigServiceProvider>(
                            create: (_) => CreateBigServiceProvider(
                              createBigService: const CreateBigServiceModel(),
                            ),
                          )
                        ],
                        child: const NestedLogicRoute(
                          child: CreateBigServicePage(),
                        ),
                      ),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
