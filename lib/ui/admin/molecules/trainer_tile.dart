import 'package:flutter/material.dart';

import 'package:paulonia_cache_image/paulonia_cache_image.dart';
import 'package:tube/ui/common/atoms/texts_widgets.dart';
import 'package:tube/ui/common/molecules/size_tap_animation.dart';
import 'package:tube/utils/colors.dart';

class TrainerTile extends StatelessWidget {
  final String? title;
  final String? subTitle;
  final String? photoUrl;
  final Color subTitleColor;
  final VoidCallback? onTap;
  final EdgeInsets innerPadding;

  const TrainerTile({
    this.title,
    this.subTitle,
    this.photoUrl,
    this.onTap,
    this.subTitleColor = ColorData.colorMainLink,
    this.innerPadding = EdgeInsets.zero,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizeTapAnimation(
      onTap: onTap,
      behavior: HitTestBehavior.opaque,
      child: Padding(
        padding: innerPadding,
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15),
              child: Row(
                children: [
                  CircleAvatar(
                    radius: 24,
                    backgroundColor: ColorData.colorElementsLightMenu,
                    foregroundImage: photoUrl == null
                        ? null
                        : PCacheImage(
                            photoUrl!,
                            enableInMemory: true,
                          ),
                    child: const Icon(
                      Icons.perm_contact_calendar,
                      size: 30,
                      color: ColorData.colorElementsSecondary,
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          if (title != null)
                            H4Text(
                              title!,
                              color: Colors.black,
                            ),
                          if (subTitle != null)
                            Padding(
                              padding: const EdgeInsets.only(top: 2),
                              child: P2Text(
                                subTitle!,
                                color: subTitleColor,
                              ),
                            ),
                        ],
                      ),
                    ),
                  ),
                  if (onTap != null)
                    const Padding(
                      padding: EdgeInsets.only(left: 10),
                      child: Icon(
                        Icons.chevron_right,
                        size: 24,
                        color: ColorData.colorElementsSecondary,
                      ),
                    ),
                ],
              ),
            ),
            const Divider(
              color: ColorData.color5Percent,
              height: 0,
              thickness: 0.5,
            )
          ],
        ),
      ),
    );
  }
}
